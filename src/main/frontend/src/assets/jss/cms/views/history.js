import {grayColor, whiteColor} from "../../material-dashboard-react";


const historyStyle = {
    cardTitleWhite: {
        color: whiteColor,
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: grayColor[1],
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    searchHeader: {
        display: 'flex',
        flexDirection: 'row'
    },
    searchInput: {
        width: 200,
        flex: 1,
        border: 'none !important'
    }
}

export default historyStyle;
