import {grayColor, whiteColor} from "../../material-dashboard-react";


const teamStyle = {
    cardTitleWhite: {
        color: whiteColor,
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: grayColor[1],
            fontWeight: "400",
            lineHeight: "1"
        }
    },
    searchHeader: {
        display: 'flex',
        flexDirection: 'row'
    },
    searchInput: {
        width: 200,
        flex: 1,
        border: 'none !important'
    },
    thumbnail: {
        border: '1px solid #ddd',
        borderRadius: 4,
        padding: 5,
        width: 40,
        '&:hover': {
            boxShadow: '0 0 2px 1px rgba(0, 140, 186, 0.5)',
        }
    },
    preview: {
        objectFit: 'contain',
        height: 200,
        width: 200,
        marginTop: 20,
    },

}

export default teamStyle;
