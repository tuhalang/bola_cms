

const modalStyles = {
    imageDiv: {
        display: 'flex',
        alignItems: 'center',
    },
    formItem: {
        margin: '8px 0 8px 0'

    },
    inputForm: {
        width: '450px !important',
    }
}

export default modalStyles;
