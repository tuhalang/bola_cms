import {connect} from 'react-redux';
import Admin from "../layouts/Admin";
import actions from "redux/actions/auth"

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({
    logout() {
        dispatch(actions.logout());
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(Admin);
