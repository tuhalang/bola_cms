import {connect} from 'react-redux';
import History from "../views/History";
import actions from "../redux/actions/history";

const mapStateToProps = (state) => ({
    histories: state.history.histories,
    totalElements: state.history.totalElements,
    totalPages: state.history.totalPages,
});

const mapDispatchToProps = (dispatch) => ({
    getListHistory: (params, callback) => {
        dispatch(actions.getListHistory(params, callback))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(History);
