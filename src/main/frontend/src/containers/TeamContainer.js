import {connect} from 'react-redux';
import Team from "../views/Team";
import actions from "../redux/actions/team";

const mapStateToProps = (state) => ({
    teams: state.team.teams,
    totalElements: state.team.totalElements,
    totalPages: state.team.totalPages,
});

const mapDispatchToProps = (dispatch) => ({
    getListTeam: (params, callback) => {
        dispatch(actions.getListTeam(params, callback))
    },
    createTeam: (params, callback) => {
        dispatch(actions.createTeam(params, callback))
    },
    updateTeam: (params, callback) => {
        dispatch(actions.updateTeam(params, callback))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Team);
