import {connect} from 'react-redux';
import Player from "../views/Player";
import actions from "redux/actions/player";
import actionsTeam from "redux/actions/team";

const mapStateToProps = (state) => ({
    player: state.player.player,
    totalElements: state.player.totalElements,
    totalPages: state.player.totalPages,
    teams: state.team.teams
});

const mapDispatchToProps = (dispatch) => ({
    getListPlayer: (params, callback) => {
        dispatch(actions.getListPlayer(params, callback))
    },
    createPlayer: (params, callback) => {
        dispatch(actions.createPlayer(params, callback))
    },
    updatePlayer: (params, callback) => {
        dispatch(actions.updatePlayer(params, callback))
    },
    getListTeam: (params, callback) => {
        dispatch(actionsTeam.getListTeam(params, callback))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Player);
