import {connect} from 'react-redux';
import PrizePackage from "../views/PrizePackage";
import actions from "../redux/actions/prize";

const mapStateToProps = (state) => ({
    prizePackage: state.prize.prizePackage,
    totalElementsPackage: state.prize.totalElementsPackage,
    totalPagesPackage: state.prize.totalPagesPackage,
    packages: state.prize.packages,
    totalPackageElements: state.prize.totalPackageElements,
    totalPackagePages: state.prize.totalPackagePages,
    prizes: state.prize.prize
});

const mapDispatchToProps = (dispatch) => ({
    getListPackage: (params, callback) => {
        dispatch(actions.getListPackage(params, callback))
    },
    getListPrizePackage: (params, callback) => {
        dispatch(actions.getListPrizePackage(params, callback))
    },
    createPrizePackage: (params, callback) => {
        dispatch(actions.createPrizePackage(params, callback))
    },
    updatePrizePackage: (params, callback) => {
        dispatch(actions.updatePrizePackage(params, callback))
    },
    getListPrize: (params, callback) => {
        dispatch(actions.getListPrize(params, callback))
    },
});


export default connect(mapStateToProps, mapDispatchToProps)(PrizePackage);
