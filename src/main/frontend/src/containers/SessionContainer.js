import {connect} from 'react-redux';
import Session from "../views/Session";
import actions from "../redux/actions/session";
import actionsMatch from "../redux/actions/match";
import actionWinnerPackage from '../redux/actions/package';
import actionTeam from '../redux/actions/team';
import actionPlayer from '../redux/actions/player';

const mapStateToProps = (state) => ({
    session: state.session.session,
    totalElements: state.session.totalElements,
    totalPages: state.session.totalPages,
    match: state.match.match,
    winnerPackage: state.winnerPackage.packages,
    teams: state.team.teams,
    player: state.player.player
});

const mapDispatchToProps = (dispatch) => ({
    getListSession: (params, callback) => {
        dispatch(actions.getListSession(params, callback))
    },
    createSession: (params, callback) => {
        dispatch(actions.createSession(params, callback))
    },
    updateSession: (params, callback) => {
        dispatch(actions.updateSession(params, callback))
    },
    getListMatch: (params, callback) => {
        dispatch(actionsMatch.getListMatch(params, callback))
    },
    getListWinnerPackage: (params, callback) => {
        dispatch(actionWinnerPackage.getListPackage(params, callback))
    },
    getListTeam: (params, callback) => {
        dispatch(actionTeam.getListTeam(params, callback))
    },
    getListPlayer: (params, callback) => {
        dispatch(actionPlayer.getListPlayer(params, callback))
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(Session);
