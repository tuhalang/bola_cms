import {connect} from 'react-redux';
import News from "../views/News";
import actions from "redux/actions/news"

const mapStateToProps = (state) => ({
    news: state.news.news,
    totalElements: state.news.totalElements,
    totalPages: state.news.totalPages,
});

const mapDispatchToProps = (dispatch) => ({
    getListNews: (params, callback) => {
        dispatch(actions.getListNews(params, callback))
    },
    createNews: (params, callback) => {
        dispatch(actions.createNews(params, callback))
    },
    updateNews: (params, callback) => {
        dispatch(actions.updateNews(params, callback))
    },
    testSmsNews: (params, callback) => {
        dispatch(actions.testSmsNews(params, callback))
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(News);
