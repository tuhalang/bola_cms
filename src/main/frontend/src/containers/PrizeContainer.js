import {connect} from 'react-redux';
import Prize from "../views/Prize";
import actions from "redux/actions/prize"

const mapStateToProps = (state) => ({
    prize: state.prize.prize,
    totalElements: state.prize.totalElements,
    totalPages: state.prize.totalPages,
});

const mapDispatchToProps = (dispatch) => ({
    getListPrize: (params, callback) => {
        dispatch(actions.getListPrize(params, callback))
    },
    createPrize: (params, callback) => {
        dispatch(actions.createPrize(params, callback))
    },
    updatePrize: (params, callback) => {
        dispatch(actions.updatePrize(params, callback))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Prize);
