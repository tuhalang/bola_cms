import {connect} from 'react-redux';
import Package from "../views/Package";
import actions from "redux/actions/package"

const mapStateToProps = (state) => ({
    winnerPackage: state.winnerPackage.packages,
    totalElements: state.winnerPackage.totalElements,
    totalPages: state.winnerPackage.totalPages,
});

const mapDispatchToProps = (dispatch) => ({
    getListPackage: (params, callback) => {
        dispatch(actions.getListPackage(params, callback))
    },
    createPackage: (params, callback) => {
        dispatch(actions.createPackage(params, callback))
    },
    updatePackage: (params, callback) => {
        dispatch(actions.updatePackage(params, callback))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Package);
