import {connect} from 'react-redux';
import Login from "../layouts/Login";
import actions from '../redux/actions/auth'
import {withRouter} from "react-router";

const mapStateToProps = (state) => ({
    authenticated: state.auth.authenticated
});

const mapDispatchToProps = (dispatch) => ({
    onSignInAction: (params, callback) => {
        dispatch(actions.signIn(params, callback))
    },
    checkSignIn: () => {
        dispatch(actions.checkSignIn())
    }
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));
