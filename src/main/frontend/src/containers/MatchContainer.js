import {connect} from 'react-redux';
import Match from "../views/Match";
import actions from "../redux/actions/match";
import actionsTeam from "../redux/actions/team";

const mapStateToProps = (state) => ({
    match: state.match.match,
    totalElements: state.match.totalElements,
    totalPages: state.match.totalPages,
    teams: state.team.teams
});

const mapDispatchToProps = (dispatch) => ({
    getListMatch: (params, callback) => {
        dispatch(actions.getListMatch(params, callback))
    },
    createMatch: (params, callback) => {
        dispatch(actions.createMatch(params, callback))
    },
    updateMatch: (params, callback) => {
        dispatch(actions.updateMatch(params, callback))
    },
    getListTeam: (params, callback) => {
        dispatch(actionsTeam.getListTeam(params, callback))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Match);
