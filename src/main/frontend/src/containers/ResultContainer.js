import {connect} from 'react-redux';
import Result from "../views/Result";
import actions from "redux/actions/result";
import actionsSession from "../redux/actions/session";

const mapStateToProps = (state) => ({
    results: state.result.results,
    totalElements: state.result.totalElements,
    totalPages: state.result.totalPages,
    sessions: state.session.session,
});

const mapDispatchToProps = (dispatch) => ({
    getListResult: (params, callback) => {
        dispatch(actions.getListResult(params, callback))
    },
    getListSession: (params, callback) => {
        dispatch(actionsSession.getListSession(params, callback))
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(Result);
