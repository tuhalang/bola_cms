import AuthRequest from './AuthRequest';
import ResultRequest from "./ResultRequest";
import PrizeRequest from "./PrizeRequest";
import SessionRequest from './SessionRequest'
import PlayerRequest from './PlayerRequest'
import MatchRequest from './MatchRequest'
import HistoryRequest from "./HistoryRequest";
import TeamRequest from './TeamRequest';
import PackageRequest from './PackageRequest';
import NewsRequest from './NewsRequest';

const requestMap = {
    AuthRequest,
    ResultRequest,
    PrizeRequest,
    PlayerRequest,
    MatchRequest,
    SessionRequest,
    HistoryRequest,
    TeamRequest,
    PackageRequest,
    NewsRequest
};

const instances = {};

export default class RequestFactory {
    static getRequest(classname) {
        const RequestClass = requestMap[classname];
        if (!RequestClass) {
            throw new Error(`Invalid request class name: ${classname}`);
        }

        let requestInstance = instances[classname];
        if (!requestInstance) {
            requestInstance = new RequestClass();
            instances[classname] = requestInstance;
        }
        return requestInstance;
    }
}
