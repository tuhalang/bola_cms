import BaseRequest from './BaseRequest';

const schema = 'secure';

export default class PrizeRequest extends BaseRequest {

    /**
     * @param {string} params.key
     * @param {string} params.page
     * @param {string} params.size
     * @param {string} params.status
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    getListPackage(params) {
        let url = "packages";
        return this.get(`${schema}/${url}`, params);
    }

    /**
     * @param {string} params.descEn
     * @param {string} params.descLc
     * @param {string} params.id
     * @param {string} params.nameEn
     * @param {string} params.nameLc
     * @param {string} params.status
     * @param {string} params.type
     * @param {string} params.value
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    createPackage(params) {
        let url = "packages";
        return this.post(`${schema}/${url}`, params);
    }


    /**
     * @param {string} params.descEn
     * @param {string} params.descLc
     * @param {string} params.id
     * @param {string} params.nameEn
     * @param {string} params.nameLc
     * @param {string} params.status
     * @param {string} params.type
     * @param {string} params.value
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    updatePackage(params) {
        let url = "packages";
        return this.put(`${schema}/${url}`, params);
    }
}
