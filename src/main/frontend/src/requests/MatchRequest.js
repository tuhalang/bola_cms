import BaseRequest from './BaseRequest';

const schema = 'secure';

export default class MatchRequest extends BaseRequest {

    /**
     * @param {string} params.key
     * @param {string} params.page
     * @param {string} params.size
     * @param {string} params.status
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    getListMatch(params) {
        let url = "matches";
        return this.get(`${schema}/${url}`, params);
    }

    /**
     * @param {string} params.descEn
     * @param {string} params.descLc
     * @param {string} params.imageUrl
     * @param {string} params.nameEn
     * @param {string} params.nameLc
     * @param {string} params.number
     * @param {string} params.position
     * @param {string} params.team
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    createMatch(params) {
        let url = "matches";
        return this.post(`${schema}/${url}`, params);
    }


    /**
     * @param {string} params.id
     * @param {string} params.descEn
     * @param {string} params.descLc
     * @param {string} params.imageUrl
     * @param {string} params.nameEn
     * @param {string} params.nameLc
     * @param {string} params.number
     * @param {string} params.position
     * @param {string} params.team
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    updateMatch(params) {
        let url = "matches";
        return this.put(`${schema}/${url}`, params);
    }

}
