import consts, {CONTEXT_PATH, BASE_URL} from "../consts";
import React from "react";
import i18n from "i18n";
import utils from "../common/utils";

export default class BaseRequest {

    version = "BOLA_CMS/api";


    prefix() {
        return '';
    }

    async get(url, params = {}) {
        try {
            const response = await window.axios.get(`${this.version}/${url}`, {params});
            return this._responseHandler(response);
        } catch (error) {
            this._errorHandler(error);
        }
    }

    async getWithTimeout(url, params = {}, timeout) {
        try {
            const response = await window.axios.get(`${this.version}/${url}`, {params, timeout});
            return this._responseHandler(response);
        } catch (error) {
            this._errorHandler(error);
        }
    }

    async put(url, data = {}, query = {}) {
        try {
            const response = await window.axios.put(`${this.version}/${url}`, data, {params: query});
            return this._responseHandler(response);
        } catch (error) {
            this._errorHandler(error);
        }
    }

    async post(url, data = {}, query = {}) {
        console.log(`${this.version}/${url}`, 'url')
        try {
            const response = await window.axios.post(`${this.version}/${url}`, data, {params: query});
            return this._responseHandler(response);
        } catch (error) {
            this._errorHandler(error);
        }
    }

    async del(url, params = {}) {
        try {
            const response = await window.axios.delete(`${this.version}/${url}`, params);
            return this._responseHandler(response);
        } catch (error) {
            this._errorHandler(error);
        }
    }

    async _responseHandler(response) {
        const {data} = response;
        if (data.errorCode != "0") {
            utils.showNotification(
                i18n.t("common-req"),
                data.message,
                "error"
            );
            return {
                data: null,
                status: false
            }
        }

        if(data.errorCode == "0" && data.data && data.data.type && data.data.type == 'SMS'){
            utils.showNotification(
                i18n.t("common-req"),
                data.message,
                "success"
            );
            return {data: data.data, status: true};
        }

        return {data: data.data, status: true};
    }

    _errorHandler(err) {
        if (err.response && err.response.status === 401) { // Unauthorized (session timeout)
            window.location.href = '/';
        } else if (err.response.status === 400) {
            utils.showNotification('Notification', "Request to server error", 'error')
        }
        throw err;
    }
}
