import BaseRequest from './BaseRequest';

const schema = 'secure';

export default class PlayerRequest extends BaseRequest {

    /**
     * @param {string} params.key
     * @param {string} params.page
     * @param {string} params.size
     * @param {string} params.status
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    getListPlayer(params) {
        let url = "players";
        return this.get(`${schema}/${url}`, params);
    }

    /**
     * @param {string} params.descEn
     * @param {string} params.descLc
     * @param {string} params.imageUrl
     * @param {string} params.nameEn
     * @param {string} params.nameLc
     * @param {string} params.number
     * @param {string} params.position
     * @param {string} params.team
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    createPlayer(params) {
        let url = "players";
        return this.post(`${schema}/${url}`, params);
    }


    /**
     * @param {string} params.id
     * @param {string} params.descEn
     * @param {string} params.descLc
     * @param {string} params.imageUrl
     * @param {string} params.nameEn
     * @param {string} params.nameLc
     * @param {string} params.number
     * @param {string} params.position
     * @param {string} params.team
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    updatePlayer(params) {
        let url = "players";
        return this.put(`${schema}/${url}`, params);
    }
}
