import BaseRequest from './BaseRequest';

const schema = 'secure';

export default class HistoryRequest extends BaseRequest {

    /**
     * @param {string} params.key
     * @param {string} params.page
     * @param {string} params.size
     * @param {string} params.status
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    getListHistory(params) {
        let url = "turns";
        return this.get(`${schema}/${url}`, params);
    }

}
