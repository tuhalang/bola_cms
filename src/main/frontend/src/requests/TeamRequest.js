import BaseRequest from './BaseRequest';

const schema = 'secure';

export default class TeamRequest extends BaseRequest {

    /**
     * @param {string} params.key
     * @param {string} params.page
     * @param {string} params.size
     * @param {string} params.status
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    getListTeam(params) {
        let url = "teams";
        return this.get(`${schema}/${url}`, params);
    }

    /**
     * @param {string} params.descEn
     * @param {string} params.descLc
     * @param {string} params.code
     * @param {string} params.nameEn
     * @param {string} params.nameLc
     * @param {string} params.group
     * @param {string} params.imageUrl
     * @param {string} params.state
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    createTeam(params) {
        let url = "teams";
        return this.post(`${schema}/${url}`, params);
    }


    /**
     * @param {string} params.descEn
     * @param {string} params.descLc
     * @param {string} params.code
     * @param {string} params.nameEn
     * @param {string} params.nameLc
     * @param {string} params.group
     * @param {string} params.imageUrl
     * @param {string} params.state
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    updateTeam(params) {
        let url = "teams";
        return this.put(`${schema}/${url}`, params);
    }


}
