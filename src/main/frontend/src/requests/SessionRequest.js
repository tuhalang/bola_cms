import BaseRequest from './BaseRequest';

const schema = 'secure';

export default class SessionRequest extends BaseRequest {

    /**
     * @param {string} params.key
     * @param {string} params.page
     * @param {string} params.size
     * @param {string} params.status
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    getListSession(params) {
        let url = "sessions";
        return this.get(`${schema}/${url}`, params);
    }

    /**
     * @param {string} params.descEn
     * @param {string} params.descLc
     * @param {string} params.imageUrl
     * @param {string} params.nameEn
     * @param {string} params.nameLc
     * @param {string} params.number
     * @param {string} params.position
     * @param {string} params.team
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    createSession(params) {
        let url = "sessions";
        return this.post(`${schema}/${url}`, params);
    }


    /**
     * @param {string} params.id
     * @param {string} params.descEn
     * @param {string} params.descLc
     * @param {string} params.imageUrl
     * @param {string} params.nameEn
     * @param {string} params.nameLc
     * @param {string} params.number
     * @param {string} params.position
     * @param {string} params.team
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    updateSession(params) {
        let url = "sessions";
        return this.put(`${schema}/${url}`, params);
    }

}
