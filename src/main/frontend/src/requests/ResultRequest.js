import BaseRequest from './BaseRequest';

const schema = 'secure';

export default class ResultRequest extends BaseRequest {

    /**
     * @param {string} params.page
     * @param {string} params.size
     * @param {string} params.predictionSession
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    getListResult(params) {
        let url = "results";
        return this.get(`${schema}/${url}`, params);
    }

}
