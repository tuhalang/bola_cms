import BaseRequest from './BaseRequest';

const schema = 'auth';

export default class AuthRequest extends BaseRequest {

    /**
     * @param {string} params.username
     * @param {string} params.password
     * @returns {Promise<BaseRequest._responseHandler.props.data|undefined>}
     */
    login(params) {
        let url = "signIn";
        return this.post(`${schema}/${url}`, params);
    }
}
