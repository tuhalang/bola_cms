import React, {useState, Suspense} from 'react';
import "assets/css/material-dashboard-react.css?v=1.9.0";
import 'antd/dist/antd.css';
import {connect} from 'react-redux';
import {Redirect, Route, Router, Switch, BrowserRouter} from "react-router-dom";
import Admin from "./containers/AdminContainer";
import Auth from "./containers/AuthContainer";
import {createBrowserHistory} from "history";
import Loading from "./components/Loading";

const hist = createBrowserHistory();

function App(props) {
    return (
        <Suspense fallback={<Loading/>}>
            {props.authenticated ? <Router history={hist}>
                <Switch>
                    <Route path="/BOLA_CMS" component={Admin}/>
                    <Redirect from="/" to="/BOLA_CMS"/>
                </Switch>
            </Router> : <BrowserRouter><Auth/></BrowserRouter>}
        </Suspense>
    );
}

const mapStateToProps = (state) => ({
    authenticated: state.auth.authenticated,
});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(App);
