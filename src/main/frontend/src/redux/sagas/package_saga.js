import {all, call, fork, put, takeLatest} from "redux-saga/effects";
import rf from "../../requests/RequestFactory";
import _ from 'lodash';
import actions from "../actions/package";
import {
    CREATE_PACKAGE, 
    GET_LIST_PACKAGE,
    UPDATE_PACKAGE,
} from "../actions/package/action_types";

function* getListPackage(action) {
    console.log("=== getListPackage ===", action);
    try {
        const {data, status} = yield call(
            (params) => rf.getRequest("PackageRequest").getListPackage(params),
            action.params
        );
        if (_.isFunction(action.callback)) action.callback();

        if (!status) {
            yield put(actions.getListPackageFailed(new Error("Error Step 1")));
            return;
        }
        yield put(actions.getListPackageSucceed(data));
    } catch (err) {
        yield put(actions.getListPackageFailed(err));
    }
}

function* createPackage(action) {
    console.log("=== createPackage ===", action);
    try {
        const {data, status} = yield call(
            (params) => rf.getRequest("PackageRequest").createPackage(params),
            action.params
        );
        if (_.isFunction(action.callback)) action.callback();

        if (!status) {
            yield put(actions.createPackageFailed(new Error("Error Step 1")));
            return;
        }
        yield put(actions.createPackageSucceed(data));
    } catch (err) {
        yield put(actions.createPackageFailed(err));
    }
}

function* updatePackage(action) {
    console.log("=== updatePackage ===", action);
    try {
        const {data, status} = yield call(
            (params) => rf.getRequest("PackageRequest").updatePackage(params),
            action.params
        );
        if (_.isFunction(action.callback)) action.callback();

        if (!status) {
            yield put(actions.updatePackageFailed(new Error("Error Step 1")));
            return;
        }
        yield put(actions.updatePackageSucceed(data));
    } catch (err) {
        yield put(actions.updatePackageFailed(err));
    }
}

function* watchAllPackage() {
    yield takeLatest(GET_LIST_PACKAGE, getListPackage);
    yield takeLatest(UPDATE_PACKAGE, updatePackage);
    yield takeLatest(CREATE_PACKAGE, createPackage);
}

export default function* rootSaga() {
    yield all([fork(watchAllPackage)]);
}
