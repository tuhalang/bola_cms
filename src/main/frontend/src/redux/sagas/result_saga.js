import {all, call, fork, put, takeLatest} from "redux-saga/effects";
import rf from "../../requests/RequestFactory";
import _ from 'lodash';
import actions from "../actions/result";
import {GET_LIST_RESULT} from "../actions/result/action_types";

function* getListResult(action) {
    console.log("=== getListResult ===", action);
    try {
        const {data, status} = yield call(
            (params) => rf.getRequest("ResultRequest").getListResult(params),
            action.params
        );
        if (_.isFunction(action.callback)) action.callback();
        console.log(data,"dataa")

        if (!status) {
            yield put(actions.getListResultFailed(new Error("Error Step 1")));
            return;
        }
        yield put(actions.getListResultSucceed(data));
    } catch (err) {
        yield put(actions.getListResultFailed(err));
    }
}

function* watchAllResult() {
    yield takeLatest(GET_LIST_RESULT, getListResult);
}

export default function* rootSaga() {
    yield all([fork(watchAllResult)]);
}
