import {all, call, fork, put, takeLatest} from "redux-saga/effects";
import rf from "../../requests/RequestFactory";
import _ from 'lodash';
import actions from "../actions/news";
import {
    CREATE_NEWS,
    GET_LIST_NEWS,
    UPDATE_NEWS,
    TEST_SMS_NEWS
} from "../actions/news/action_types";

function* getListNews(action) {
    console.log("=== getListNews ===", action);
    try {
        const {data, status} = yield call(
            (params) => rf.getRequest("NewsRequest").getListNews(params),
            action.params
        );
        if (_.isFunction(action.callback)) action.callback();

        if (!status) {
            yield put(actions.getListNewsFailed(new Error("Error Step 1")));
            return;
        }
        yield put(actions.getListNewsSucceed(data));
    } catch (err) {
        yield put(actions.getListNewsFailed(err));
    }
}

function* createNews(action) {
    console.log("=== createNews ===", action);
    try {
        const {data, status} = yield call(
            (params) => rf.getRequest("NewsRequest").createNews(params),
            action.params
        );
        if (_.isFunction(action.callback)) action.callback();

        if (!status) {
            yield put(actions.createNewsFailed(new Error("Error Step 1")));
            return;
        }
        yield put(actions.createNewsSucceed(data));
    } catch (err) {
        yield put(actions.createNewsFailed(err));
    }
}

function* updateNews(action) {
    console.log("=== updateNews ===", action);
    try {
        const {data, status} = yield call(
            (params) => rf.getRequest("NewsRequest").updateNews(params),
            action.params
        );

        if (_.isFunction(action.callback)) action.callback();

        if (!status) {
            yield put(actions.updateNewsFailed(new Error("Error Step 1")));
            return;
        }
        yield put(actions.updateNewsSucceed(data));
    } catch (err) {
        yield put(actions.updateNewsFailed(err));
    }
}

function* testSmsNews(action) {
    console.log("=== testSmsNews ===", action);
    try {
        const {data, status} = yield call(
            (params) => rf.getRequest("NewsRequest").testSmsNews(params),
            action.params
        );
        if (_.isFunction(action.callback)) action.callback();

        if (!status) {
            yield put(actions.testSmsNewsFailed(new Error("Error Step 1")));
            return;
        }
        yield put(actions.testSmsNewsSucceed(data));
    } catch (err) {
        yield put(actions.testSmsNewsFailed(err));
    }
}




function* watchAllNews() {
    yield takeLatest(GET_LIST_NEWS, getListNews);
    yield takeLatest(UPDATE_NEWS, updateNews);
    yield takeLatest(CREATE_NEWS, createNews);
    yield takeLatest(TEST_SMS_NEWS, testSmsNews)
}

export default function* rootSaga() {
    yield all([fork(watchAllNews)]);
}
