import {all, call, fork, put, takeLatest} from "redux-saga/effects";
import rf from "../../requests/RequestFactory";
import _ from 'lodash';
import actions from "../actions/prize";
import {
    CREATE_PRIZE, CREATE_PRIZE_PACKAGE,
    GET_LIST_PRIZE,
    GET_LIST_PRIZE_PACKAGE,
    GET_LIST_PACKAGE,
    UPDATE_PRIZE,
    UPDATE_PRIZE_PACKAGE
} from "../actions/prize/action_types";

function* getListPrize(action) {
    console.log("=== getListPrize ===", action);
    try {
        const {data, status} = yield call(
            (params) => rf.getRequest("PrizeRequest").getListPrize(params),
            action.params
        );
        if (_.isFunction(action.callback)) action.callback();

        if (!status) {
            yield put(actions.getListPrizeFailed(new Error("Error Step 1")));
            return;
        }
        yield put(actions.getListPrizeSucceed(data));
    } catch (err) {
        yield put(actions.getListPrizeFailed(err));
    }
}

function* createPrize(action) {
    console.log("=== createPrize ===", action);
    try {
        const {data, status} = yield call(
            (params) => rf.getRequest("PrizeRequest").createPrize(params),
            action.params
        );
        if (_.isFunction(action.callback)) action.callback();

        if (!status) {
            yield put(actions.createPrizeFailed(new Error("Error Step 1")));
            return;
        }
        yield put(actions.createPrizeSucceed(data));
    } catch (err) {
        yield put(actions.createPrizeFailed(err));
    }
}

function* updatePrize(action) {
    console.log("=== updatePrize ===", action);
    try {
        const {data, status} = yield call(
            (params) => rf.getRequest("PrizeRequest").updatePrize(params),
            action.params
        );
        if (_.isFunction(action.callback)) action.callback();

        if (!status) {
            yield put(actions.updatePrizeFailed(new Error("Error Step 1")));
            return;
        }
        yield put(actions.updatePrizeSucceed(data));
    } catch (err) {
        yield put(actions.updatePrizeFailed(err));
    }
}


function* getListPrizePackage(action) {
    console.log("=== getListPrizePackage ===", action);
    try {
        const {data, status} = yield call(
            (params) => rf.getRequest("PrizeRequest").getListPrizePackage(params),
            action.params
        );
        if (_.isFunction(action.callback)) action.callback();

        if (!status) {
            yield put(actions.getListPrizePackageFailed(new Error("Error Step 1")));
            return;
        }
        yield put(actions.getListPrizePackageSucceed(data));
    } catch (err) {
        yield put(actions.getListPrizePackageFailed(err));
    }
}

function* createPrizePackage(action) {
    console.log("=== createPrizePackage ===", action);
    try {
        const {data, status} = yield call(
            (params) => rf.getRequest("PrizeRequest").createPrizePackage(params),
            action.params
        );
        if (_.isFunction(action.callback)) action.callback();

        if (!status) {
            yield put(actions.createPrizePackageFailed(new Error("Error Step 1")));
            return;
        }
        yield put(actions.createPrizePackageSucceed(data));
    } catch (err) {
        yield put(actions.createPrizePackageFailed(err));
    }
}

function* updatePrizePackage(action) {
    console.log("=== updatePrizePackage ===", action);
    try {
        const {data, status} = yield call(
            (params) => rf.getRequest("PrizeRequest").updatePrizePackage(params),
            action.params
        );
        if (_.isFunction(action.callback)) action.callback();

        if (!status) {
            yield put(actions.updatePrizePackageFailed(new Error("Error Step 1")));
            return;
        }
        yield put(actions.updatePrizePackageSucceed(data));
    } catch (err) {
        yield put(actions.updatePrizePackageFailed(err));
    }
}

function* getListPackage(action) {
    console.log("=== getListPackage ===", action);
    try {
        const {data, status} = yield call(
            (params) => rf.getRequest("PackageRequest").getListPackage(params),
            action.params
        );
        if (_.isFunction(action.callback)) action.callback();

        if (!status) {
            yield put(actions.getListPackageFailed(new Error("Error Step 1")));
            return;
        }
        yield put(actions.getListPackageSucceed(data));
    } catch (err) {
        yield put(actions.getListPackageFailed(err));
    }
}

function* watchAllPrize() {
    yield takeLatest(GET_LIST_PRIZE, getListPrize);
    yield takeLatest(UPDATE_PRIZE, updatePrize);
    yield takeLatest(CREATE_PRIZE, createPrize);
    yield takeLatest(GET_LIST_PRIZE_PACKAGE, getListPrizePackage);
    yield takeLatest(UPDATE_PRIZE_PACKAGE, updatePrizePackage);
    yield takeLatest(CREATE_PRIZE_PACKAGE, createPrizePackage);
    yield takeLatest(GET_LIST_PACKAGE, getListPackage);
}

export default function* rootSaga() {
    yield all([fork(watchAllPrize)]);
}
