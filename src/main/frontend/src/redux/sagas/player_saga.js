import {all, call, fork, put, takeLatest} from "redux-saga/effects";
import rf from "../../requests/RequestFactory";
import _ from 'lodash';
import actions from "../actions/player";
import {CREATE_PLAYER, GET_LIST_PLAYER, UPDATE_PLAYER} from "../actions/player/action_types";

function* getListPlayer(action) {
    console.log("=== getListPlayer ===", action);
    try {
        const {data, status} = yield call(
            (params) => rf.getRequest("PlayerRequest").getListPlayer(params),
            action.params
        );
        if (_.isFunction(action.callback)) action.callback();

        if (!status) {
            yield put(actions.getListPlayerFailed(new Error("Error Step 1")));
            return;
        }
        yield put(actions.getListPlayerSucceed(data));
    } catch (err) {
        yield put(actions.getListPlayerFailed(err));
    }
}

function* createPlayer(action) {
    console.log("=== createPlayer ===", action);
    try {
        const {data, status} = yield call(
            (params) => rf.getRequest("PlayerRequest").createPlayer(params),
            action.params
        );
        if (_.isFunction(action.callback)) action.callback();

        if (!status) {
            yield put(actions.createPlayerFailed(new Error("Error Step 1")));
            return;
        }
        yield put(actions.createPlayerSucceed(data));
    } catch (err) {
        yield put(actions.createPlayerFailed(err));
    }
}

function* updatePlayer(action) {
    console.log("=== updatePlayer ===", action);
    try {
        const {data, status} = yield call(
            (params) => rf.getRequest("PlayerRequest").updatePlayer(params),
            action.params
        );
        if (_.isFunction(action.callback)) action.callback();

        if (!status) {
            yield put(actions.updatePlayerFailed(new Error("Error Step 1")));
            return;
        }
        yield put(actions.updatePlayerSucceed(data));
    } catch (err) {
        yield put(actions.updatePlayerFailed(err));
    }
}

function* watchAllPlayer() {
    yield takeLatest(GET_LIST_PLAYER, getListPlayer);
    yield takeLatest(CREATE_PLAYER, createPlayer);
    yield takeLatest(UPDATE_PLAYER, updatePlayer);
}

export default function* rootSaga() {
    yield all([fork(watchAllPlayer)]);
}
