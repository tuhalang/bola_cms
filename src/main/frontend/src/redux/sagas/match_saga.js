import {all, call, fork, put, takeLatest} from "redux-saga/effects";
import rf from "../../requests/RequestFactory";
import _ from 'lodash';
import actions from "../actions/match";
import {
    CREATE_MATCH,
    GET_LIST_MATCH,
    UPDATE_MATCH,
} from "../actions/match/action_types";

function* getListMatch(action) {
    console.log("=== getListMatch ===", action);
    try {
        const {data, status} = yield call(
            (params) => rf.getRequest("MatchRequest").getListMatch(params),
            action.params
        );
        if (_.isFunction(action.callback)) action.callback();

        if (!status) {
            yield put(actions.getListMatchFailed(new Error("Error Step 1")));
            return;
        }
        yield put(actions.getListMatchSucceed(data));
    } catch (err) {
        yield put(actions.getListMatchFailed(err));
    }
}

function* createMatch(action) {
    console.log("=== createMatch ===", action);
    try {
        const {data, status} = yield call(
            (params) => rf.getRequest("MatchRequest").createMatch(params),
            action.params
        );
        if (_.isFunction(action.callback)) action.callback();

        if (!status) {
            yield put(actions.createMatchFailed(new Error("Error Step 1")));
            return;
        }
        yield put(actions.createMatchSucceed(data));
    } catch (err) {
        yield put(actions.createMatchFailed(err));
    }
}

function* updateMatch(action) {
    console.log("=== updateMatch ===", action);
    try {
        const {data, status} = yield call(
            (params) => rf.getRequest("MatchRequest").updateMatch(params),
            action.params
        );
        if (_.isFunction(action.callback)) action.callback();

        if (!status) {
            yield put(actions.updateMatchFailed(new Error("Error Step 1")));
            return;
        }
        yield put(actions.updateMatchSucceed(data));
    } catch (err) {
        yield put(actions.updateMatchFailed(err));
    }
}

function* watchAllMatch() {
    yield takeLatest(GET_LIST_MATCH, getListMatch);
    yield takeLatest(CREATE_MATCH, createMatch);
    yield takeLatest(UPDATE_MATCH, updateMatch);
}

export default function* rootSaga() {
    yield all([fork(watchAllMatch)]);
}
