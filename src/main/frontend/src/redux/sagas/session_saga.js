import {all, call, fork, put, takeLatest} from "redux-saga/effects";
import rf from "../../requests/RequestFactory";
import _ from 'lodash';
import actions from "../actions/session";
import {
    CREATE_SESSION,
    GET_LIST_SESSION,
    UPDATE_SESSION,
} from "../actions/session/action_types";

function* getListSession(action) {
    console.log("=== getListSession ===", action);
    try {
        const {data, status} = yield call(
            (params) => rf.getRequest("SessionRequest").getListSession(params),
            action.params
        );
        if (_.isFunction(action.callback)) action.callback();

        if (!status) {
            yield put(actions.getListSessionFailed(new Error("Error Step 1")));
            return;
        }
        yield put(actions.getListSessionSucceed(data));
    } catch (err) {
        yield put(actions.getListSessionFailed(err));
    }
}

function* createSession(action) {
    console.log("=== createSession ===", action);
    try {
        const {data, status} = yield call(
            (params) => rf.getRequest("SessionRequest").createSession(params),
            action.params
        );
        if (_.isFunction(action.callback)) action.callback();

        if (!status) {
            yield put(actions.createSessionFailed(new Error("Error Step 1")));
            return;
        }
        yield put(actions.createSessionSucceed(data));
    } catch (err) {
        yield put(actions.createSessionFailed(err));
    }
}

function* updateSession(action) {
    console.log("=== updateSession ===", action);
    try {
        const {data, status} = yield call(
            (params) => rf.getRequest("SessionRequest").updateSession(params),
            action.params
        );
        if (_.isFunction(action.callback)) action.callback();

        if (!status) {
            yield put(actions.updateSessionFailed(new Error("Error Step 1")));
            return;
        }
        yield put(actions.updateSessionSucceed(data));
    } catch (err) {
        yield put(actions.updateSessionFailed(err));
    }
}

function* watchAllSession() {
    yield takeLatest(GET_LIST_SESSION, getListSession);
    yield takeLatest(CREATE_SESSION, createSession);
    yield takeLatest(UPDATE_SESSION, updateSession);
}

export default function* rootSaga() {
    yield all([fork(watchAllSession)]);
}
