import {all} from 'redux-saga/effects';
import watchAllAuth from './auth_saga'
import watchAllHistory from './history_saga'
import watchAllMatch from './match_saga'
import watchAllPlayer from './player_saga'
import watchAllPrize from './prize_saga'
import watchAllResult from './result_saga';
import watchAllSession from './session_saga';
import watchAllTeam from './team_saga';
import watchAllPackage from './package_saga';
import watchAllNews from './news_saga';

function* rootSaga() {
    yield all([
        watchAllAuth(),
        watchAllHistory(),
        watchAllMatch(),
        watchAllPlayer(),
        watchAllPrize(),
        watchAllResult(),
        watchAllSession(),
        watchAllTeam(),
        watchAllPackage(),
        watchAllNews(),
    ]);
}

export default rootSaga;
