import {all, call, fork, put, takeLatest} from "redux-saga/effects";
import rf from "../../requests/RequestFactory";
import _ from 'lodash';
import actions from "../actions/team";
import {CREATE_TEAM, GET_LIST_TEAM, UPDATE_TEAM} from "../actions/team/action_types";

function* getListTeam(action) {
    console.log("=== getListTeam ===", action);
    try {
        const {data, status} = yield call(
            (params) => rf.getRequest("TeamRequest").getListTeam(params),
            action.params
        );
        if (_.isFunction(action.callback)) action.callback();

        if (!status) {
            yield put(actions.getListTeamFailed(new Error("Error Step 1")));
            return;
        }
        yield put(actions.getListTeamSucceed(data));
    } catch (err) {
        yield put(actions.getListTeamFailed(err));
    }
}

function* createTeam(action) {
    console.log("=== createTeam ===", action);
    try {
        const {data, status} = yield call(
            (params) => rf.getRequest("TeamRequest").createTeam(params),
            action.params
        );
        if (_.isFunction(action.callback)) action.callback();

        if (!status) {
            yield put(actions.createTeamFailed(new Error("Error Step 1")));
            return;
        }
        yield put(actions.createTeamSucceed(data));
    } catch (err) {
        yield put(actions.createTeamFailed(err));
    }
}

function* updateTeam(action) {
    console.log("=== updateTeam ===", action);
    try {
        const {data, status} = yield call(
            (params) => rf.getRequest("TeamRequest").updateTeam(params),
            action.params
        );
        if (_.isFunction(action.callback)) action.callback();

        if (!status) {
            yield put(actions.updateTeamFailed(new Error("Error Step 1")));
            return;
        }
        yield put(actions.updateTeamSucceed(data));
    } catch (err) {
        yield put(actions.updateTeamFailed(err));
    }
}

function* watchAllTeam() {
    yield takeLatest(GET_LIST_TEAM, getListTeam);
    yield takeLatest(UPDATE_TEAM, updateTeam);
    yield takeLatest(CREATE_TEAM, createTeam);
}

export default function* rootSaga() {
    yield all([fork(watchAllTeam)]);
}
