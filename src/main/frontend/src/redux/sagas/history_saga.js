import {all, call, fork, put, takeLatest} from "redux-saga/effects";
import rf from "../../requests/RequestFactory";
import _ from 'lodash';
import actions from "../actions/history";
import {GET_LIST_HISTORY} from "../actions/history/action_types";

function* getListHistory(action) {
    console.log("=== getListHistory ===", action);
    try {
        const {data, status} = yield call(
            (params) => rf.getRequest("HistoryRequest").getListHistory(params),
            action.params
        );
        if (_.isFunction(action.callback)) action.callback();

        if (!status) {
            yield put(actions.getListHistoryFailed(new Error("Error Step 1")));
            return;
        }
        yield put(actions.getListHistorySucceed(data));
    } catch (err) {
        yield put(actions.getListHistoryFailed(err));
    }
}


function* watchAllHistory() {
    yield takeLatest(GET_LIST_HISTORY, getListHistory);
}

export default function* rootSaga() {
    yield all([fork(watchAllHistory)]);
}
