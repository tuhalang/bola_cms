import {all, call, fork, put, takeLatest} from "redux-saga/effects";
import rf from "../../requests/RequestFactory";
import _ from 'lodash';
import actions from "redux/actions/auth"
import {SIGN_IN} from "../actions/auth/action_types";

function* signIn(action) {
    console.log("=== login ===", action);
    try {
        const {data, status} = yield call(
            (params) => rf.getRequest("AuthRequest").login(params),
            action.params
        );
        console.log(data, "data login");

        if (!status) {
            yield put(actions.signInFailed(new Error("Error Step 1")));
            return;
        }
        yield put(actions.signInSucceed(data.token));
        if (_.isFunction(action.callback)) action.callback();
    } catch (err) {
        yield put(actions.signInFailed(err));
    }
}

function* watchAllAuth() {
    yield takeLatest(SIGN_IN, signIn);
}

export default function* rootSaga() {
    yield all([fork(watchAllAuth)]);
}
