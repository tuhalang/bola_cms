import {
    CREATE_PACKAGE,
    CREATE_PACKAGE_FAILED,
    CREATE_PACKAGE_SUCCEED,
    GET_LIST_PACKAGE,
    GET_LIST_PACKAGE_FAILED,
    GET_LIST_PACKAGE_SUCCEED,
    UPDATE_PACKAGE,
    UPDATE_PACKAGE_FAILED,
    UPDATE_PACKAGE_SUCCEED
} from "./action_types";

export default {
    getListPackage: (params, callback) => ({
        type: GET_LIST_PACKAGE,
        params,
        callback,
    }),
    getListPackageSucceed: (data) => ({
        type: GET_LIST_PACKAGE_SUCCEED,
        data,
    }),
    getListPackageFailed: (err) => ({
        type: GET_LIST_PACKAGE_FAILED,
        err,
    }),

    updatePackage: (params, callback) => ({
        type: UPDATE_PACKAGE,
        params,
        callback,
    }),
    updatePackageSucceed: (data) => ({
        type: UPDATE_PACKAGE_SUCCEED,
        data,
    }),
    updatePackageFailed: (err) => ({
        type: UPDATE_PACKAGE_FAILED,
        err,
    }),

    createPackage: (params, callback) => ({
        type: CREATE_PACKAGE,
        params,
        callback,
    }),
    createPackageSucceed: (data) => ({
        type: CREATE_PACKAGE_SUCCEED,
        data,
    }),
    createPackageFailed: (err) => ({
        type: CREATE_PACKAGE_FAILED,
        err,
    }),
}
