import {
    CREATE_TEAM, CREATE_TEAM_FAILED, CREATE_TEAM_SUCCEED,
    GET_LIST_TEAM,
    GET_LIST_TEAM_FAILED,
    GET_LIST_TEAM_SUCCEED,
    UPDATE_TEAM, UPDATE_TEAM_FAILED,
    UPDATE_TEAM_SUCCEED
} from "./action_types";

export default {
    getListTeam: (params, callback) => ({
        type: GET_LIST_TEAM,
        params,
        callback,
    }),
    getListTeamSucceed: (data) => ({
        type: GET_LIST_TEAM_SUCCEED,
        data,
    }),
    getListTeamFailed: (err) => ({
        type: GET_LIST_TEAM_FAILED,
        err,
    }),

    updateTeam: (params, callback) => ({
        type: UPDATE_TEAM,
        params,
        callback,
    }),
    updateTeamSucceed: (data) => ({
        type: UPDATE_TEAM_SUCCEED,
        data,
    }),
    updateTeamFailed: (err) => ({
        type: UPDATE_TEAM_FAILED,
        err,
    }),

    createTeam: (params, callback) => ({
        type: CREATE_TEAM,
        params,
        callback,
    }),
    createTeamSucceed: (data) => ({
        type: CREATE_TEAM_SUCCEED,
        data,
    }),
    createTeamFailed: (err) => ({
        type: CREATE_TEAM_FAILED,
        err,
    }),
}
