import {
    CREATE_NEWS,
    CREATE_NEWS_FAILED,
    CREATE_NEWS_SUCCEED,
    GET_LIST_NEWS,
    GET_LIST_NEWS_FAILED,
    GET_LIST_NEWS_SUCCEED,
    UPDATE_NEWS,
    UPDATE_NEWS_FAILED,
    UPDATE_NEWS_SUCCEED,
    TEST_SMS_NEWS,
    TEST_SMS_NEWS_SUCCEED,
    TEST_SMS_NEWS_FAILED
} from "./action_types";

export default {
    getListNews: (params, callback) => ({
        type: GET_LIST_NEWS,
        params,
        callback,
    }),
    getListNewsSucceed: (data) => ({
        type: GET_LIST_NEWS_SUCCEED,
        data,
    }),
    getListNewsFailed: (err) => ({
        type: GET_LIST_NEWS_FAILED,
        err,
    }),

    updateNews: (params, callback) => ({
        type: UPDATE_NEWS,
        params,
        callback,
    }),
    updateNewsSucceed: (data) => ({
        type: UPDATE_NEWS_SUCCEED,
        data,
    }),
    updateNewsFailed: (err) => ({
        type: UPDATE_NEWS_FAILED,
        err,
    }),

    createNews: (params, callback) => ({
        type: CREATE_NEWS,
        params,
        callback,
    }),
    createNewsSucceed: (data) => ({
        type: CREATE_NEWS_SUCCEED,
        data,
    }),
    createNewsFailed: (err) => ({
        type: CREATE_NEWS_FAILED,
        err,
    }),
    
    testSmsNews: (params, callback) => ({
        type: TEST_SMS_NEWS,
        params,
        callback,
    }),
    testSmsNewsSucceed: (data) => ({
        type: TEST_SMS_NEWS_SUCCEED,
        data,
    }),
    testSmsNewsFailed: (err) => ({
        type: TEST_SMS_NEWS_FAILED,
        err,
    }),
}
