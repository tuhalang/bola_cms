import {GET_LIST_HISTORY, GET_LIST_HISTORY_FAILED, GET_LIST_HISTORY_SUCCEED} from "./action_types";

export default {
    getListHistory: (params, callback) => ({
        type: GET_LIST_HISTORY,
        params,
        callback,
    }),
    getListHistorySucceed: (data) => ({
        type: GET_LIST_HISTORY_SUCCEED,
        data,
    }),
    getListHistoryFailed: (err) => ({
        type: GET_LIST_HISTORY_FAILED,
        err,
    }),
}
