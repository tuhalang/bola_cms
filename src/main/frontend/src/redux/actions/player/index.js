import {
    CREATE_PLAYER, CREATE_PLAYER_FAILED, CREATE_PLAYER_SUCCEED,
    GET_LIST_PLAYER,
    GET_LIST_PLAYER_FAILED,
    GET_LIST_PLAYER_SUCCEED,
    UPDATE_PLAYER, UPDATE_PLAYER_FAILED,
    UPDATE_PLAYER_SUCCEED
} from "./action_types";

export default {
    getListPlayer: (params, callback) => ({
        type: GET_LIST_PLAYER,
        params,
        callback,
    }),
    getListPlayerSucceed: (data) => ({
        type: GET_LIST_PLAYER_SUCCEED,
        data,
    }),
    getListPlayerFailed: (err) => ({
        type: GET_LIST_PLAYER_FAILED,
        err,
    }),

    updatePlayer: (params, callback) => ({
        type: UPDATE_PLAYER,
        params,
        callback,
    }),
    updatePlayerSucceed: (data) => ({
        type: UPDATE_PLAYER_SUCCEED,
        data,
    }),
    updatePlayerFailed: (err) => ({
        type: UPDATE_PLAYER_FAILED,
        err,
    }),

    createPlayer: (params, callback) => ({
        type: CREATE_PLAYER,
        params,
        callback,
    }),
    createPlayerSucceed: (data) => ({
        type: CREATE_PLAYER_SUCCEED,
        data,
    }),
    createPlayerFailed: (err) => ({
        type: CREATE_PLAYER_FAILED,
        err,
    }),
}
