import {
    CREATE_MATCH, CREATE_MATCH_FAILED, CREATE_MATCH_SUCCEED,
    GET_LIST_MATCH,
    GET_LIST_MATCH_FAILED,
    GET_LIST_MATCH_SUCCEED,
    UPDATE_MATCH, UPDATE_MATCH_FAILED,
    UPDATE_MATCH_SUCCEED
} from "./action_types";

export default {
    getListMatch: (params, callback) => ({
        type: GET_LIST_MATCH,
        params,
        callback,
    }),
    getListMatchSucceed: (data) => ({
        type: GET_LIST_MATCH_SUCCEED,
        data,
    }),
    getListMatchFailed: (err) => ({
        type: GET_LIST_MATCH_FAILED,
        err,
    }),

    updateMatch: (params, callback) => ({
        type: UPDATE_MATCH,
        params,
        callback,
    }),
    updateMatchSucceed: (data) => ({
        type: UPDATE_MATCH_SUCCEED,
        data,
    }),
    updateMatchFailed: (err) => ({
        type: UPDATE_MATCH_FAILED,
        err,
    }),

    createMatch: (params, callback) => ({
        type: CREATE_MATCH,
        params,
        callback,
    }),
    createMatchSucceed: (data) => ({
        type: CREATE_MATCH_SUCCEED,
        data,
    }),
    createMatchFailed: (err) => ({
        type: CREATE_MATCH_FAILED,
        err,
    }),
}
