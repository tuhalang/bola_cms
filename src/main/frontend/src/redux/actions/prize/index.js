import {
    CREATE_PRIZE,
    CREATE_PRIZE_FAILED,
    CREATE_PRIZE_PACKAGE,
    CREATE_PRIZE_PACKAGE_FAILED,
    CREATE_PRIZE_PACKAGE_SUCCEED,
    CREATE_PRIZE_SUCCEED,
    GET_LIST_PRIZE,
    GET_LIST_PRIZE_FAILED,
    GET_LIST_PRIZE_PACKAGE,
    GET_LIST_PRIZE_PACKAGE_FAILED,
    GET_LIST_PRIZE_PACKAGE_SUCCEED,
    GET_LIST_PRIZE_SUCCEED,
    UPDATE_PRIZE,
    UPDATE_PRIZE_FAILED,
    UPDATE_PRIZE_PACKAGE,
    UPDATE_PRIZE_PACKAGE_FAILED,
    UPDATE_PRIZE_PACKAGE_SUCCEED,
    UPDATE_PRIZE_SUCCEED,
    GET_LIST_PACKAGE,
    GET_LIST_PACKAGE_FAILED,
    GET_LIST_PACKAGE_SUCCEED
} from "./action_types";

export default {
    getListPrize: (params, callback) => ({
        type: GET_LIST_PRIZE,
        params,
        callback,
    }),
    getListPrizeSucceed: (data) => ({
        type: GET_LIST_PRIZE_SUCCEED,
        data,
    }),
    getListPrizeFailed: (err) => ({
        type: GET_LIST_PRIZE_FAILED,
        err,
    }),

    updatePrize: (params, callback) => ({
        type: UPDATE_PRIZE,
        params,
        callback,
    }),
    updatePrizeSucceed: (data) => ({
        type: UPDATE_PRIZE_SUCCEED,
        data,
    }),
    updatePrizeFailed: (err) => ({
        type: UPDATE_PRIZE_FAILED,
        err,
    }),

    createPrize: (params, callback) => ({
        type: CREATE_PRIZE,
        params,
        callback,
    }),
    createPrizeSucceed: (data) => ({
        type: CREATE_PRIZE_SUCCEED,
        data,
    }),
    createPrizeFailed: (err) => ({
        type: CREATE_PRIZE_FAILED,
        err,
    }),


    getListPrizePackage: (params, callback) => ({
        type: GET_LIST_PRIZE_PACKAGE,
        params,
        callback,
    }),
    getListPrizePackageSucceed: (data) => ({
        type: GET_LIST_PRIZE_PACKAGE_SUCCEED,
        data,
    }),
    getListPrizePackageFailed: (err) => ({
        type: GET_LIST_PRIZE_PACKAGE_FAILED,
        err,
    }),

    updatePrizePackage: (params, callback) => ({
        type: UPDATE_PRIZE_PACKAGE,
        params,
        callback,
    }),
    updatePrizePackageSucceed: (data) => ({
        type: UPDATE_PRIZE_PACKAGE_SUCCEED,
        data,
    }),
    updatePrizePackageFailed: (err) => ({
        type: UPDATE_PRIZE_PACKAGE_FAILED,
        err,
    }),

    createPrizePackage: (params, callback) => ({
        type: CREATE_PRIZE_PACKAGE,
        params,
        callback,
    }),
    createPrizePackageSucceed: (data) => ({
        type: CREATE_PRIZE_PACKAGE_SUCCEED,
        data,
    }),
    createPrizePackageFailed: (err) => ({
        type: CREATE_PRIZE_PACKAGE_FAILED,
        err,
    }),

    getListPackage: (params, callback) => ({
        type: GET_LIST_PACKAGE,
        params,
        callback,
    }),
    getListPackageSucceed: (data) => ({
        type: GET_LIST_PACKAGE_SUCCEED,
        data,
    }),
    getListPackageFailed: (err) => ({
        type: GET_LIST_PACKAGE_FAILED,
        err,
    }),
}
