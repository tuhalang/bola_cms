import {GET_LIST_RESULT, GET_LIST_RESULT_FAILED, GET_LIST_RESULT_SUCCEED} from "./action_types";

export default {
    getListResult: (params, callback) => ({
        type: GET_LIST_RESULT,
        params,
        callback,
    }),
    getListResultSucceed: (data) => ({
        type: GET_LIST_RESULT_SUCCEED,
        data,
    }),
    getListResultFailed: (err) => ({
        type: GET_LIST_RESULT_FAILED,
        err,
    }),
}
