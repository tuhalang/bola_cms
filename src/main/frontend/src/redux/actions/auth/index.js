import {
    CHECK_SIGN_IN,
    CHECK_SIGN_IN_FAILED,
    CHECK_SIGN_IN_SUCCEED, LOGOUT,
    SIGN_IN,
    SIGN_IN_FAILED,
    SIGN_IN_SUCCEED
} from "./action_types";

export default {
    signIn: (params, callback) => ({
        type: SIGN_IN,
        params,
        callback,
    }),
    signInSucceed: (token) => ({
        type: SIGN_IN_SUCCEED,
        token,
    }),
    signInFailed: (err) => ({
        type: SIGN_IN_FAILED,
        err,
    }),

    checkSignIn: () => ({
        type: CHECK_SIGN_IN,
    }),

    logout: () => ({
        type: LOGOUT,
    }),
}
