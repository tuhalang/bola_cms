import {
    CREATE_SESSION, CREATE_SESSION_FAILED, CREATE_SESSION_SUCCEED,
    GET_LIST_SESSION,
    GET_LIST_SESSION_FAILED,
    GET_LIST_SESSION_SUCCEED,
    UPDATE_SESSION, UPDATE_SESSION_FAILED,
    UPDATE_SESSION_SUCCEED
} from "./action_types";

export default {
    getListSession: (params, callback) => ({
        type: GET_LIST_SESSION,
        params,
        callback,
    }),
    getListSessionSucceed: (data) => ({
        type: GET_LIST_SESSION_SUCCEED,
        data,
    }),
    getListSessionFailed: (err) => ({
        type: GET_LIST_SESSION_FAILED,
        err,
    }),

    updateSession: (params, callback) => ({
        type: UPDATE_SESSION,
        params,
        callback,
    }),
    updateSessionSucceed: (data) => ({
        type: UPDATE_SESSION_SUCCEED,
        data,
    }),
    updateSessionFailed: (err) => ({
        type: UPDATE_SESSION_FAILED,
        err,
    }),

    createSession: (params, callback) => ({
        type: CREATE_SESSION,
        params,
        callback,
    }),
    createSessionSucceed: (data) => ({
        type: CREATE_SESSION_SUCCEED,
        data,
    }),
    createSessionFailed: (err) => ({
        type: CREATE_SESSION_FAILED,
        err,
    }),
}
