export const GET_LIST_SESSION = 'GET_LIST_SESSION'
export const GET_LIST_SESSION_SUCCEED = 'GET_LIST_SESSION_SUCCEED'
export const GET_LIST_SESSION_FAILED = 'GET_LIST_SESSION_FAILED'

export const UPDATE_SESSION = 'UPDATE_SESSION'
export const UPDATE_SESSION_SUCCEED = 'UPDATE_SESSION_SUCCEED'
export const UPDATE_SESSION_FAILED = 'UPDATE_SESSION_FAILED'

export const CREATE_SESSION = 'CREATE_SESSION'
export const CREATE_SESSION_SUCCEED = 'CREATE_SESSION_SUCCEED'
export const CREATE_SESSION_FAILED = 'CREATE_SESSION_FAILED'