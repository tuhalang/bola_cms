import {GET_LIST_MATCH_SUCCEED} from "../actions/match/action_types";

const defaultParams = {
    match: [],
    totalElements: 0,
    totalPages: 0,
};

export default (state = defaultParams, action) => {
    switch (action.type) {
        case GET_LIST_MATCH_SUCCEED:
            return {
                ...state,
                match: action.data.items,
                totalElements: action.data.totalElements,
                totalPages: action.data.totalPages,
            }
        default:
            return {
                ...state,
            };
    }
};
