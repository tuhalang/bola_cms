import { GET_LIST_TEAM_SUCCEED} from "../actions/team/action_types";

const defaultParams = {
    teams: [],
    totalElements: 0,
    totalPages: 0,
};

export default (state = defaultParams, action) => {
    switch (action.type) {
        case GET_LIST_TEAM_SUCCEED:
            return {
                ...state,
                teams: action.data.items,
                totalElements: action.data.totalElements,
                totalPages: action.data.totalPages,
            }
        default:
            return {
                ...state,
            };
    }
};
