import {GET_LIST_NEWS_SUCCEED} from "../actions/news/action_types";

const defaultParams = {
    news: [],
    totalElements: 0,
    totalPages: 0
};

export default (state = defaultParams, action) => {
    switch (action.type) {
        case GET_LIST_NEWS_SUCCEED:
            return {
                ...state,
                news: action.data.items,
                totalElements: action.data.totalElements,
                totalPages: action.data.totalPages,
            }
        default:
            return {
                ...state,
            };
    }
};
