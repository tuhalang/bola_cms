import {GET_LIST_RESULT_SUCCEED} from "../actions/result/action_types";

const defaultParams = {
    results: [],
    totalElements: 0,
    totalPages: 0,
};

export default (state = defaultParams, action) => {
    switch (action.type) {
        case GET_LIST_RESULT_SUCCEED:
            return {
                ...state,
                results: action.data.items,
                totalElements: action.data.totalElements,
                totalPages: action.data.totalPages,
            }
        default:
            return {
                ...state,
            };
    }
};
