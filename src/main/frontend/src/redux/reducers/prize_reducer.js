import {GET_LIST_PRIZE_PACKAGE_SUCCEED, GET_LIST_PRIZE_SUCCEED, GET_LIST_PACKAGE_SUCCEED} from "../actions/prize/action_types";

const defaultParams = {
    prize: [],
    prizePackage: [],
    totalElements: 0,
    totalPages: 0,
    totalElementsPackage: 0,
    totalPagesPackage: 0,
    packages: [],
    totalPackageElements: 0,
    totalPackagePages: 0,
};

export default (state = defaultParams, action) => {
    switch (action.type) {
        case GET_LIST_PRIZE_SUCCEED:
            return {
                ...state,
                prize: action.data.items,
                totalElements: action.data.totalElements,
                totalPages: action.data.totalPages,
            }
        case GET_LIST_PRIZE_PACKAGE_SUCCEED:
            return {
                ...state,
                prizePackage: action.data.items,
                totalElementsPackage: action.data.totalElements,
                totalPagesPackage: action.data.totalPages,
            }
        case GET_LIST_PACKAGE_SUCCEED:
            return {
                ...state,
                packages: action.data.items,
                totalPackageElements: action.data.totalElements,
                totalPackagePages: action.data.totalPages,
            }
        default:
            return {
                ...state,
            };
    }
};
