import {CHECK_SIGN_IN, LOGOUT, SIGN_IN_SUCCEED} from "../actions/auth/action_types";

const defaultParams = {
    authenticated: false,
};

export default (state = defaultParams, action) => {
    switch (action.type) {
        case SIGN_IN_SUCCEED:
            localStorage.setItem("token", action.token)
            window.axios.interceptors.request.use(function (config) {
                config.headers.Authorization = action.token;
                return config;
            });
            return {
                ...state,
                authenticated: true
            }
        case CHECK_SIGN_IN:
            let token = localStorage.getItem("token")
            return {
                ...state,
                authenticated: token && token != ''
            }
        case LOGOUT:
            localStorage.removeItem("token")
            return {
                ...state,
                authenticated: false,
            }
        default:
            return {
                ...state,
            };
    }
};
