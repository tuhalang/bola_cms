import {GET_LIST_SESSION_SUCCEED} from "../actions/session/action_types";

const defaultParams = {
    session: [],
    totalElements: 0,
    totalPages: 0,
};

export default (state = defaultParams, action) => {
    switch (action.type) {
        case GET_LIST_SESSION_SUCCEED:
            return {
                ...state,
                session: action.data.items,
                totalElements: action.data.totalElements,
                totalPages: action.data.totalPages,
            }
        default:
            return {
                ...state,
            };
    }
};
