import {combineReducers} from 'redux';
import auth from './auth_reducer';
import history from './history_reducer';
import match from './match_reducer';
import player from './player_reducer'
import prize from './prize_reducer'
import result from './result_reducer';
import session from './session_reducer';
import team from './team_reducer';
import winnerPackage from './package_reducer';
import news from './news_reducer';

const allReducers = combineReducers({
    auth,
    history,
    match,
    player,
    prize,
    result,
    session,
    team,
    winnerPackage,
    news,
});

export default allReducers;
