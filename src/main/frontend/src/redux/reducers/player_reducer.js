import {GET_LIST_PLAYER_SUCCEED} from "../actions/player/action_types";

const defaultParams = {
    player: [],
    totalElements: 0,
    totalPages: 0,
};

export default (state = defaultParams, action) => {
    switch (action.type) {
        case GET_LIST_PLAYER_SUCCEED:
            return {
                ...state,
                player: action.data.items,
                totalElements: action.data.totalElements,
                totalPages: action.data.totalPages,
            }
        default:
            return {
                ...state,
            };
    }
};
