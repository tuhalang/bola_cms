import {GET_LIST_PACKAGE_SUCCEED} from "../actions/package/action_types";

const defaultParams = {
    packages: [],
    totalElements: 0,
    totalPages: 0,
};

export default (state = defaultParams, action) => {
    switch (action.type) {
        case GET_LIST_PACKAGE_SUCCEED:
            return {
                ...state,
                packages: action.data.items,
                totalElements: action.data.totalElements,
                totalPages: action.data.totalPages,
            }
        default:
            return {
                ...state,
            };
    }
};
