import {GET_LIST_HISTORY_SUCCEED} from "../actions/history/action_types";

const defaultParams = {
    histories: [],
    totalElements: 0,
    totalPages: 0,
};

export default (state = defaultParams, action) => {
    switch (action.type) {
        case GET_LIST_HISTORY_SUCCEED:
            return {
                ...state,
                histories: action.data.items,
                totalElements: action.data.totalElements,
                totalPages: action.data.totalPages,
            }
        default:
            return {
                ...state,
            };
    }
};
