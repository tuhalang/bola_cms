import {notification} from 'antd';
import _ from 'lodash';
import consts, {DNS_IMAGE} from '../consts';
import moment from "moment";

// let timeoutID;
const showNotification = (message = 'title', description = 'description', type = 'success') => {
    notification[type]({
        message,
        description,
    });
};

// const clone = (obj) => Object.assign({}, obj);

const renameKey = (object, key, newKey) => {
    const clonedObj = {...object};
    const targetKey = clonedObj[key];
    delete clonedObj[key];
    clonedObj[newKey] = targetKey;
    return clonedObj;
};


const addHeader = (data, header) => {
    // header = header.map(col => window._.startCase(window._.toLower(col)));
    data.unshift(header);
    return data;
};

const trimAll = (obj) => {
    if (window._.isArray(obj)) {
        console.log('please pass object here! src/common/utils.js:29');
        return;
    }

    window._.forEach(obj, (value, key) => {
        if (typeof obj[`${key}`] === 'object') {
            trimAll(obj[`${key}`]);
        } else {
            if (key !== `${key}`.trim()) {
                delete obj[`${key}`];
                key = `${key}`.trim();
            }
            if (typeof value === 'string') {
                obj[`${key}`] = window._.trim(value);
            }
        }
    });
};

const paddingZeros = (num, count = 19) => ('0'.repeat(count) + num).substr(-count, count);
const trimRightZero = (num) => (`${num}`.split('.').length === 2 ? window._.trimEnd(num, '0') : `${num}`);
const trimDot = (num) => window._.trimEnd(`${num}`, '.');
const trimRightZeroAndDot = (num) => trimDot(trimRightZero(num));

const fillTable = (arr) => {
    let newArr = arr;
    if (_.isArray(newArr)) {
        while (newArr?.length % consts.DEFAULT_PAGE_SIZE !== 0) {
            newArr = [...newArr, {}]
        }
        return newArr
    }
}

function getCoinName(coins, symbol) {
    return window._.find(coins, ['symbol', symbol]);
}

function resetCacheTimeTVChart() {
    sessionStorage.setItem('currentTimeRequest', '');
    sessionStorage.setItem('previousTimeRequest', '');
}

function upperCaseFirst(input) {
    return window._.upperFirst(input);
}

/**
 * Check if string is HEX, requires a 0x in front
 *
 * @method isHexStrict
 * @param {String} hex to be checked
 * @returns {Boolean}
 */
const isHexStrict = function (hex) {
    return ((_.isString(hex) || _.isNumber(hex)) && /^(-)?0x[0-9a-f]*$/i.test(hex));
};

/**
 * Convert a hex string to a byte array
 *
 * Note: Implementation from crypto-js
 *
 * @method hexToBytes
 * @param {string} hex
 * @return {Array} the byte array
 */
const hexToBytes = function (hex) {
    let truncateHex = hex.toString(16);

    if (!isHexStrict(hex)) {
        throw new Error(`Given value "${hex}" is not a valid hex string.`);
    }

    truncateHex = truncateHex.replace(/^0x/i, '');
    const bytes = [];
    for (let c = 0; c < truncateHex.length; c += 2) {
        bytes.push(parseInt(truncateHex.substr(c, 2), 16));
    }

    return bytes;
};

const getBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
});

const convertPrice = price => {
    if (typeof (price) == 'string') price = Number.parseInt(price)
    return price.toFixed(3).replace(/\d(?=(\d{3})+\.)/g, '$&,');
}

const convertDateFormat = timestamp => {
    return moment(timestamp * 1000 + 24 * 60 * 60 * 1000).format('yyyy-MM-DD')
}

async function readXlsx() {

}

function getImageUrl(path) {
    if (!path) return ''
    return path.startsWith('http://') || path.startsWith('https://') ? path : DNS_IMAGE + path
}

export default {
    resetCacheTimeTVChart,
    trimAll,
    showNotification,
    paddingZeros,
    trimRightZero,
    trimRightZeroAndDot,
    upperCaseFirst,
    renameKey,
    fillTable,
    getBase64,
    convertPrice,
    convertDateFormat,
    readXlsx,
    getImageUrl
};
