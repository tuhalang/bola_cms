import React from 'react';
import 'assets/css/dot.css'

function Loading() {
    return (
        <div style={{
            width: '100%',
            height: '100%',
            display: 'flex',
            justifyContent: 'center',
            position: 'absolute',
            left: 0,
            top: 0
        }}
             onClick={() => {
             }}>
            <div className={'container-dot'}>
            <span className="dots-cont">
                    <span className="dot dot-1"></span>
                    <span className="dot dot-2"></span>
                    <span className="dot dot-3"></span>
                </span>
            </div>
        </div>
    );
}

export default Loading;
