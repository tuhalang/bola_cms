import React, {useState} from 'react';
import {AutoComplete, DatePicker, Form, Input, InputNumber, Select} from "antd";
import _ from 'lodash';
import {makeStyles} from "@material-ui/core/styles";
import {useTranslation} from "react-i18next";
import styles from 'assets/jss/cms/components/modal';
import moment from 'moment';


const useStyles = makeStyles(styles);

const layout = {
    labelCol: {span: 8},
    wrapperCol: {span: 16},
};

function EditMatch(props) {
    const classes = useStyles();
    const {
        dataProps,
        action,
        type,
    } = props
    const {form} = Form.useForm();
    const {t} = useTranslation('translation')
    const [options, setOptions] = React.useState([]);

    const columns = [
        {
            title: t('firstTeam'),
            type: 'auto1',
            name: 'firstTeam',
            value: dataProps['firstTeam']
        },
        {
            title: t('firstTeamGoals'),
            type: 'number',
            name: 'firstTeamGoals',
            value: dataProps['firstTeamGoals']
        },
        {
            title: t('secondTeam'),
            type: 'auto2',
            name: 'secondTeam',
            value: dataProps['secondTeam']
        },
        {
            title: t('secondTeamGoals'),
            type: 'number',
            name: 'secondTeamGoals',
            value: dataProps['secondTeamGoals']
        },
        {
            title: t('placeEn'),
            type: 'string',
            name: 'placeEn',
            value: dataProps['placeEn']
        },
        {
            title: t('placeLc'),
            type: 'string',
            name: 'placeLc',
            value: dataProps['placeLc']
        },
        {
            title: t('descEn'),
            type: 'string',
            name: 'descEn',
            value: dataProps['descEn']
        },
        {
            title: t('descLc'),
            type: 'string',
            name: 'descLc',
            value: dataProps['descLc']
        },
        {
            title: t('startTime'),
            type: 'date',
            name: 'startTime',
            value: dataProps['startTime']
        },
        {
            title: t('endTime'),
            type: 'date',
            name: 'endTime',
            value: dataProps['endTime']
        },
        {
            title: t('status'),
            type: 'combobox',
            name: 'status',
            value: dataProps['status']
        }
    ]


    const [match, setMatch] = useState({
        id: dataProps['id'] || '',
        firstTeam: dataProps['firstTeam'] || '',
        firstTeamGoals: dataProps['firstTeamGoals'] || 0,
        secondTeam: dataProps['secondTeam'] || '',
        secondTeamGoals: dataProps['secondTeamGoals'] || 0,
        placeEn: dataProps['placeEn'] || '',
        placeLc: dataProps['placeLc'] || '',
        descEn: dataProps['descEn'] || '',
        descLc: dataProps['descLc'],
        startTime: dataProps['startTime'] || '2021-06-12 00:00:00',
        endTime: dataProps['endTime'] || '2021-06-12 00:00:00',
        status: dataProps['status'] == undefined ? 0 : dataProps['status'],
    })

    const onFinish = (values) => {
        action(match)
    }

    const onSelectFirst = (data) => {
        match['firstTeam'] = _.find(props.teams, e => e.code = data).code
        setMatch({
            ...match,
        })
    };

    const onSelectSecond = (data) => {
        match['secondTeam'] = _.find(props.teams, e => e.code = data).code
        setMatch({
            ...match,
        })
    };

    const initValue = (data, type) => {
        if(type === 'date'){
            if(data === undefined || data === null || data === ''){
                return moment(new Date())
            }
            return moment(data)
        }
        return data
    }

    React.useEffect(() => {
        if (props.teams && props.teams.length > 0) {
            console.log(props.teams, "props.teams,")
            setOptions(_.map(_.unionBy(props.teams, e => e.nameEn), e => {
                return {
                    value: e.code,
                }
            }))
        } else {
            props.getListTeam({key: '', page: 1, size: 100, status: 1})
        }
    }, [props.teams])

    return (
        <>
            <Form onFinish={onFinish} {...layout} name={type} form={form}
                  id={type}>
                {_.map(type == 'createPlayer' ? _.remove(columns, o => o.name != 'id') : columns, e => {
                        const inputNode = e.type === 'number' ?
                            <InputNumber className={classes.inputForm} value={match[e.name]} onChange={(value) => {
                                match[e.name] = value
                                setMatch({
                                    ...match,
                                })
                            }
                            } min={0}/> : e.type === 'string'
                                ? <Input className={classes.inputForm} value={match[e.name]} onChange={(event) => {
                                    match[e.name] = event.target.value
                                    setMatch({
                                        ...match,
                                    })
                                }}/> : e.type === 'auto1' ? <AutoComplete
                                    className={classes.inputForm}
                                    options={options}
                                    style={{width: 200}}
                                    onSelect={onSelectFirst}
                                    onSearch={() => {
                                    }}
                                    placeholder="Please select team"
                                /> : e.type === 'auto2' ? <AutoComplete
                                className={classes.inputForm}
                                options={options}
                                style={{width: 200}}
                                onSelect={onSelectSecond}
                                onSearch={() => {
                                }}
                                placeholder="Please select team"
                            />: e.type === 'combobox' ? <Select defaultValue={0} style={{ width: 200 }} onChange={(value) => {
                                match[e.name] = value
                                setMatch({
                                    ...match,
                                })
                            }}>
                            <Select.Option value={0}>Comming Soon</Select.Option>
                            <Select.Option value={1}>Is Comming</Select.Option>
                            <Select.Option value={2}>Expired</Select.Option>
                        </Select>: e.type === 'date' ?
                                    <DatePicker showTime className={classes.inputForm} onChange={(date, dateString) => {
                                        match[e.name] = dateString
                                        setMatch({
                                            ...match,
                                        })
                                    }}/> : <div/>;
                        return (<Form.Item
                            className={classes.formItem}
                            name={e.name}
                            initialValue={initValue(match[e.name], e.type)}
                            label={e.title}
                            rules={[{
                                required: true,
                                message: t('formItemRequire').replace("{title}", e.title),
                            }]}
                        >
                            {inputNode}
                        </Form.Item>)
                    }
                )}
            </Form>
        </>
    )
}

export default EditMatch;
