import React, {useEffect, useState} from 'react';
import {Button, Form, Input, InputNumber, Modal, notification, Switch, Tooltip, Select} from "antd";
import _ from 'lodash';
import {makeStyles} from "@material-ui/core/styles";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faImage, faUpload} from "@fortawesome/free-solid-svg-icons";
import UploadFile from "../UploadFile";
import utils from "../../common/utils";
import {DNS_IMAGE} from "../../consts";
import {useTranslation} from "react-i18next";
import styles from 'assets/jss/cms/components/modal'

const useStyles = makeStyles(styles);

const layout = {
    labelCol: {span: 8},
    wrapperCol: {span: 16},
};

function EditTeam(props) {
    const classes = useStyles();
    const {
        dataProps,
        action,
        type,
    } = props
    const {form} = Form.useForm();
    const {t} = useTranslation('translation')

    const columns = [
        {
            title: t('code'),
            type: 'string',
            name: 'code',
            value: dataProps['code']
        },
        {
            title: t('nameEn'),
            type: 'string',
            name: 'nameEn',
            value: dataProps['nameEn']
        },
        {
            title: t('nameLc'),
            type: 'string',
            name: 'nameLc',
            value: dataProps['nameLc']
        },
        {
            title: t('descEn'),
            type: 'string',
            name: 'descEn',
            value: dataProps['descEn']
        },
        {
            title: t('descLc'),
            type: 'string',
            name: 'descLc',
            value: dataProps['descLc']
        },
        {
            title: t('group'),
            type: 'combobox',
            name: 'group',
            value: dataProps['group']
        },
        {
            title: t('imageUrl'),
            type: 'string',
            name: 'imageUrl',
            value: dataProps['imageUrl']
        },
        {
            title: t('scores'),
            type: 'string',
            name: 'scores',
            value: dataProps['scores']
        },
        {
            title: t('state'),
            type: 'combobox',
            name: 'state',
            value: dataProps['state']
        },
        {
            title: t('status'),
            type: 'combobox',
            name: 'status',
            value: dataProps['status']
        }
    ]


    const [team, setTeam] = useState({
        descEn: dataProps['descEn'] || '',
        descLc: dataProps['descLc'] || '',
        imageUrl: dataProps['imageUrl'] || '',
        code: dataProps['code'] || '',
        id: dataProps['id'] || '',
        group: dataProps['group'] || 'A',
        nameEn: dataProps['nameEn'] || '',
        nameLc: dataProps['nameLc'] || '',
        status: dataProps['status'] || 1,
        state: dataProps['state'] || 1,
        scores: dataProps['scores'] || 0,
    })

    const onFinish = (values) => {
        console.log("edit team")
        action(team)
    }

    return (
        <>
            <Form onFinish={onFinish} {...layout} name={type} form={form}
                  id={type}>
                {_.map(type == 'createTeam' ? _.remove(columns, o => o.name != 'id') : columns, e => {
                        let inputNode = null

                        if(e.type == 'number'){
                            inputNode = <InputNumber className={classes.inputForm} value={team[e.name]} onChange={(value) => {
                                team[e.name] = value
                                setTeam({
                                    ...team,
                                })
                            }
                            } min={0}/>
                        }

                        if(e.type == 'string'){
                            inputNode = <Input className={classes.inputForm} value={team[e.name]} onChange={(event) => {
                                team[e.name] = event.target.value
                                setTeam({
                                    ...team,
                                })
                            }}/>
                        }

                        if(e.type == 'combobox'){

                            if(e.name == 'status'){
                                inputNode = <Select defaultValue={1} style={{ width: 200 }} onChange={(value) => {
                                    team[e.name] = value
                                    setTeam({
                                        ...team,
                                    })
                                }}>
                                                <Select.Option value={0}>Disable</Select.Option>
                                                <Select.Option value={1}>Enable</Select.Option>
                                        </Select>
                            }else if(e.name == 'group'){
                                inputNode = <Select defaultValue={"A"} style={{ width: 200 }} onChange={(value) => {
                                    team[e.name] = value
                                    setTeam({
                                        ...team,
                                    })
                                }}>
                                                <Select.Option value="A">Group A</Select.Option>
                                                <Select.Option value="B">Group B</Select.Option>
                                                <Select.Option value="C">Group C</Select.Option>
                                                <Select.Option value="D">Group D</Select.Option>
                                                <Select.Option value="E">Group E</Select.Option>
                                            </Select>
                            } else if(e.name == 'state'){
                                inputNode = <Select defaultValue={1} style={{ width: 200 }} onChange={(value) => {
                                    team[e.name] = value
                                    setTeam({
                                        ...team,
                                    })
                                }}>
                                                <Select.Option value={1}>Playing</Select.Option>
                                                <Select.Option value={0}>Disqualified</Select.Option>
                                            </Select>
                            }
                            
                        }
                        
                        return (<Form.Item
                            className={classes.formItem}
                            name={e.name}
                            initialValue={team[e.name]}
                            label={e.title}
                            key={e.name}
                            rules={[{
                                required: true,
                                message: t('formItemRequire').replace("{title}", e.title),
                            }]}
                        >
                            {inputNode}
                        </Form.Item>)
                    }
                )}
            </Form>
        </>
    )
}

export default EditTeam;
