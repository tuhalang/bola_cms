import React, {useEffect, useState} from 'react';
import {Button, Form, Input, InputNumber, Modal, notification, Switch, Tooltip, Select} from "antd";
import _ from 'lodash';
import {makeStyles} from "@material-ui/core/styles";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faImage, faUpload} from "@fortawesome/free-solid-svg-icons";
import UploadFile from "../UploadFile";
import utils from "../../common/utils";
import {DNS_IMAGE} from "../../consts";
import {useTranslation} from "react-i18next";
import styles from 'assets/jss/cms/components/modal'

const useStyles = makeStyles(styles);

const layout = {
    labelCol: {span: 8},
    wrapperCol: {span: 16},
};
const tailLayout = {
    wrapperCol: {offset: 8, span: 16},
};

function EditPackage(props) {
    const classes = useStyles();
    const {
        dataProps,
        action,
        type,
    } = props
    const {form} = Form.useForm();
    const {t} = useTranslation('translation')

    const columns = [
        {
            title: t('nameEn'),
            type: 'string',
            name: 'nameEn',
            value: dataProps['nameEn']
        },
        {
            title: t('nameLc'),
            type: 'string',
            name: 'nameLc',
            value: dataProps['nameLc']
        },
        {
            title: t('descEn'),
            type: 'string',
            name: 'descEn',
            value: dataProps['descEn']
        },
        {
            title: t('descLc'),
            type: 'string',
            name: 'descLc',
            value: dataProps['descLc']
        },
        {
            title: t('status'),
            type: 'switch',
            name: 'status',
            value: dataProps['status']
        },
        {
            title: t('totalPrize'),
            type: 'number',
            name: 'totalPrize',
            value: dataProps['totalPrize']
        },
    ]


    const [prize, setPrize] = useState({
        descEn: dataProps['descEn'] || '',
        descLc: dataProps['descLc'] || '',
        id: dataProps['id'] || '',
        type: dataProps['type'] || 1,
        nameEn: dataProps['nameEn'] || '',
        nameLc: dataProps['nameLc'] || '',
        status: dataProps['status'] || 1,
        totalPrize: dataProps['totalPrize'] || 0,
    })

    const onFinish = (values) => {
        action(prize)
    }

    return (
        <>
            <Form onFinish={onFinish} {...layout} name={type} form={form}
                  id={type}>
                {_.map(type == 'createPrize' ? columns : columns, e => {
                        
                        let inputNode = null

                        if(e.type === 'number'){
                            inputNode = <InputNumber className={classes.inputForm} value={prize[e.name]} onChange={(value) => {
                                            prize[e.name] = value
                                            setPrize({
                                                ...prize,
                                            })
                                        }
                                        } min={0}/>
                        }

                        if(e.type == 'string'){
                            inputNode = <Input className={classes.inputForm} value={prize[e.name]} onChange={(event) => {
                                prize[e.name] = event.target.value
                                setPrize({
                                    ...prize,
                                })
                            }}/>
                        }

                        if(e.type === 'switch'){
                            inputNode = <div>
                                            <Switch onChange={(checked) => setPrize({
                                                ...prize,
                                                status: checked == true ? 1 : 0
                                            })} defaultChecked={prize.status == 1 ? true : false}/>
                                            <span style={{
                                                color: 'rgba(0,0,0,0.4)',
                                                marginLeft: 8
                                            }}>{prize.status ? t('active') : t('inactive')}</span>
                                        </div>
                        }

                        if(e.type === 'combobox'){
                            inputNode = <Select defaultValue={1} style={{ width: 200 }} onChange={(value) => {
                                                prize[e.name] = value
                                                setPrize({
                                                    ...prize,
                                                })
                                            }}>
                                            <Select.Option value={1}>Artifacts</Select.Option>
                                            <Select.Option value={2}>Money</Select.Option>
                                            <Select.Option value={3}>Data</Select.Option>
                                        </Select>
                        }
                        


                        return (<Form.Item
                            className={classes.formItem}
                            name={e.name}
                            initialValue={prize[e.name]}
                            label={e.title}
                            rules={[{
                                required: true,
                                message: t('formItemRequire').replace("{title}", e.title),
                            }]}
                        >
                            {inputNode}
                        </Form.Item>)
                    }
                )}
            </Form>
        </>
    )
}

export default EditPackage;
