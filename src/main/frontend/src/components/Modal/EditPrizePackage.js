import React, {useState} from 'react';
import {Form, Input, InputNumber, Switch, AutoComplete} from "antd";
import _ from 'lodash';
import {makeStyles} from "@material-ui/core/styles";
import {useTranslation} from "react-i18next";
import styles from 'assets/jss/cms/components/modal'

const useStyles = makeStyles(styles);

const layout = {
    labelCol: {span: 8},
    wrapperCol: {span: 16},
};

function EditPrizePackage(props) {
    const classes = useStyles();
    const {
        dataProps,
        action,
        type,
    } = props
    const {form} = Form.useForm();
    const {t} = useTranslation('translation')

    const [prize, setPrize] = useState({
        packageId: dataProps['packageId'] || '',
        prizeId: dataProps['prizeId'] || '',
        rank: dataProps['rank'] || 1,
        amount: dataProps['amount'] || 1,
        status: dataProps['status'] || 1,
        prizeName: dataProps['prizeName'] || '',
        winnerPackageName: dataProps['winnerPackageName'] || ''
    })
    const [options1, setOptions1] = useState([])
    const [options2, setOptions2] = useState([])

    const onSelect1 = (data) => {
        let winnerPackageId = _.find(props.packages, e => e.nameEn == data).id
        prize['packageId'] = winnerPackageId
        setPrize({...prize})
    };
    React.useEffect(() => {
        if (props.packages && props.packages.length > 0) {
            setOptions1(_.map(_.unionBy(props.packages, e => e.nameEn), e => {
                return {
                    value: e.nameEn,
                }
            }))
        } else {
            props.getListPackage({key: '', page: 1, size: 100, status: 1})
        }
    }, [props.packages])

    const onSelect2 = (data) => {
        let prizeId = _.find(props.prizes, e => e.nameEn == data).id
        prize['prizeId'] = prizeId
        setPrize({...prize})
    };
    React.useEffect(() => {
        if (props.prizes && props.prizes.length > 0) {
            setOptions2(_.map(_.unionBy(props.prizes, e => e.nameEn), e => {
                return {
                    value: e.nameEn,
                }
            }))
        } else {
            props.getListPrize({key: '', page: 1, size: 100, status: 1})
        }
    }, [props.prizes])

    const columns = [
        {
            title: t('package'),
            type: 'auto1',
            name: 'winnerPackageName',
            value: dataProps['winnerPackageName']
        },
        {
            title: t('prize'),
            type: 'auto2',
            name: 'prizeName',
            value: dataProps['prizeName']
        },
        {
            title: t('rank'),
            type: 'number',
            name: 'rank',
            value: dataProps['rank']
        },
        {
            title: t('amount'),
            type: 'number',
            name: 'amount',
            value: dataProps['amount']
        },
        {
            title: t('status'),
            type: 'switch',
            name: 'status',
            value: dataProps['status']
        },
    ]


    

    const onFinish = (values) => {
        action(prize)
    }

    return (
        <>
            <Form onFinish={onFinish} {...layout} name={type} form={form}
                  id={type}>
                {_.map(type == 'createPrizePackage' ? columns : columns, e => {
                        const inputNode = e.type === 'number' ?
                            <InputNumber className={classes.inputForm} value={prize[e.name]} onChange={(value) => {
                                prize[e.name] = value
                                setPrize({
                                    ...prize,
                                })
                            }
                            } min={0}/> : e.type === 'string'
                                ? <Input className={classes.inputForm} value={prize[e.name]} onChange={(event) => {
                                    prize[e.name] = event.target.value
                                    setPrize({
                                        ...prize,
                                    })
                                }}/> :e.type === 'auto1' ? <AutoComplete
                                className={classes.inputForm}
                                options={options1}
                                style={{width: 200}}
                                onSelect={onSelect1}
                                onSearch={() => {
                                }}
                                placeholder="Please select package"
                            /> :e.type === 'auto2' ? <AutoComplete
                            className={classes.inputForm}
                            options={options2}
                            style={{width: 200}}
                            onSelect={onSelect2}
                            onSearch={() => {
                            }}
                            placeholder="Please select prize"
                        /> : e.type === 'switch'
                                    ? <div>
                                        <Switch onChange={(checked) => setPrize({
                                            ...prize,
                                            status: checked == true ? 1 : 0
                                        })} defaultChecked={prize.status == 1 ? true : false}/>
                                        <span style={{
                                            color: 'rgba(0,0,0,0.4)',
                                            marginLeft: 8
                                        }}>{prize.status ? t('active') : t('inactive')}</span>
                                    </div> : <div/>;
                        return (<Form.Item
                            className={classes.formItem}
                            name={e.name}
                            initialValue={prize[e.name]}
                            label={e.title}
                            rules={[{
                                required: true,
                                message: t('formItemRequire').replace("{title}", e.title),
                            }]}
                        >
                            {inputNode}
                        </Form.Item>)
                    }
                )}
            </Form>
        </>
    )
}

export default EditPrizePackage;
