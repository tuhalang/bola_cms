import React from 'react';
import {Form, Input, InputNumber, Switch, AutoComplete, Select, DatePicker} from "antd";
import _ from 'lodash';
import {makeStyles} from "@material-ui/core/styles";
import {useTranslation} from "react-i18next";
import styles from 'assets/jss/cms/components/modal';
import moment from 'moment';

const useStyles = makeStyles(styles);

const layout = {
    labelCol: {span: 8},
    wrapperCol: {span: 16},
};
const tailLayout = {
    wrapperCol: {offset: 8, span: 16},
};

function EditSession(props) {
    const classes = useStyles();
    const {
        dataProps,
        action,
        type,
    } = props
    const {form} = Form.useForm();
    const {t} = useTranslation('translation')
    const [options, setOptions] = React.useState([]);
    const [options1, setOptions1] = React.useState([]);
    const [options2, setOptions2] = React.useState([]);
    const [options3, setOptions3] = React.useState([]);

    const onSelect = (data) => {
        let matchId = _.find(props.match, e => e.matchName == data).id
        session['match'] = matchId
        setSession({...session})
    };

    const onSelect1 = (data) => {
        let winnerPackageId = _.find(props.winnerPackage, e => e.nameEn == data).id
        session['winnerPackage'] = winnerPackageId
        setSession({...session})
    };

    const onSelect2 = (data) => {
        let teamId = _.find(props.teams, e => e.nameEn == data).id
        session['result'] = teamId
        setSession({...session})
    };

    const onSelect3 = (data) => {
        let playerId = _.find(props.player, e => e.nameEn == data).id
        session['result'] = playerId
        setSession({...session})
    };

    React.useEffect(() => {
        if (props.player && props.player.length > 15) {
            setOptions3(_.map(_.unionBy(props.player, e => e.nameEn), e => {
                return {
                    value: e.nameEn,
                }
            }))
        } else {
            props.getListPlayer({key: '', page: 1, size: 1000, status: null})
        }
    }, [props.player])

    React.useEffect(() => {
        if (props.teams && props.teams.length > 15) {
            setOptions2(_.map(_.unionBy(props.teams, e => e.nameEn), e => {
                return {
                    value: e.nameEn,
                }
            }))
        } else {
            props.getListTeam({key: '', page: 1, size: 100, status: null})
        }
    }, [props.teams])

    React.useEffect(() => {
        if (props.match && props.match.length > 15) {
            setOptions(_.map(_.unionBy(props.match, e => e.matchName), e => {
                return {
                    value: e.matchName,
                }
            }))
        } else {
            props.getListMatch({key: '', page: 1, size: 100, status: null})
        }
    }, [props.match])

    React.useEffect(() => {
        if (props.winnerPackage && props.winnerPackage.length > 0) {
            setOptions1(_.map(_.unionBy(props.winnerPackage, e => e.nameEn), e => {
                return {
                    value: e.nameEn,
                }
            }))
        } else {
            props.getListWinnerPackage({key: '', page: 1, size: 100, status: 1})
        }
    }, [props.winnerPackage])

    const columns = [
        {
            title: t('nameEn'),
            type: 'string',
            name: 'nameEn',
            required: true,
            value: dataProps['nameEn']
        },
        {
            title: t('nameLc'),
            type: 'string',
            name: 'nameLc',
            required: true,
            value: dataProps['nameLc']
        },
        {
            title: t('type'),
            type: 'combobox',
            name: 'type',
            required: true,
            value: dataProps['type']
        },
        {
            title: t('match'),
            type: 'auto',
            name: 'matchName',
            required: false,
            value: dataProps['matchName']
        },
        {
            title: t('result'),
            type: 'result',
            name: 'result',
            required: false,
            value: dataProps['result']
        },
        {
            title: t('package'),
            type: 'auto1',
            name: 'winnerPackageName',
            required: false,
            value: dataProps['winnerPackageName']
        },
        {
            title: t('startTime'),
            type: 'date',
            name: 'startTime',
            required: true,
            value: dataProps['startTime']
        },
        {
            title: t('endTime'),
            type: 'date',
            name: 'endTime',
            required: true,
            value: dataProps['endTime']
        },
        {
            title: t('questionEn'),
            type: 'string',
            name: 'questionEn',
            required: true,
            value: dataProps['questionEn']
        },
        {
            title: t('questionLc'),
            type: 'string',
            name: 'questionLc',
            required: true,
            value: dataProps['questionLc']
        },
        {
            title: t('descEn'),
            type: 'string',
            name: 'descEn',
            required: true,
            value: dataProps['descEn']
        },
        {
            title: t('descLc'),
            type: 'string',
            name: 'descLc',
            required: true,
            value: dataProps['descLc']
        },
        {
            title: t('status'),
            type: 'combobox1',
            name: 'status',
            required: true,
            value: dataProps['status']
        },
    ]

    const initValue = (data, type) => {
        if(type === 'date'){
            console.log('date: ' + data)
            if(data === undefined || data === null || data === ''){
                return moment(new Date())
            }
            return moment(data)
        }
        return data
    }


    const [session, setSession] = React.useState({
        descEn: dataProps['descEn'] || '',
        descLc: dataProps['descLc'] || '',
        id: dataProps['id'] || '',
        type: dataProps['type'] || 1,
        nameEn: dataProps['nameEn'] || '',
        nameLc: dataProps['nameLc'] || '',
        status: dataProps['status'] == undefined ? 0 : dataProps['status'],
        matchName: dataProps['matchName'] || '',
        match: dataProps['match'] || '',
        questionEn: dataProps['questionEn'] || '',
        questionLc: dataProps['questionLc'] || '',
        winnerPackage: dataProps['winnerPackage'] || '',
        winnerPackageName: dataProps['winnerPackageName'] || '',
        startTime: dataProps['startTime'] || '2021-06-12 00:00:00',
        endTime: dataProps['endTime'] || '2021-06-12 00:00:00',
        result: dataProps['result'] || ''
    })

    const onFinish = (values) => {
        action(session)
    }

    return (
        <>
            <Form onFinish={onFinish} {...layout} name={type} form={form}
                  id={type}>
                {_.map(type == 'createPrize' ? columns : columns, e => {

                        let inputNode = null;

                        if(e.type === 'number'){
                            inputNode = <InputNumber className={classes.inputForm} value={session[e.name]} onChange={(value) => {
                                session[e.name] = value
                                setSession({
                                    ...session,
                                })
                            }
                            } min={0}/>
                        }

                        if(e.type === 'auto'){
                            inputNode = <AutoComplete
                                            className={classes.inputForm}
                                            options={options}
                                            style={{width: 200}}
                                            onSelect={onSelect}
                                            onSearch={() => {
                                            }}
                                            placeholder="Please select match"
                                        />
                        }

                        if(e.type === 'auto1'){
                            inputNode = <AutoComplete
                                            className={classes.inputForm}
                                            options={options1}
                                            style={{width: 200}}
                                            onSelect={onSelect1}
                                            onSearch={() => {
                                            }}
                                            placeholder="Please select match"
                                        />
                        }

                        if(e.type === 'combobox'){
                            inputNode = <Select defaultValue={1} style={{ width: 200 }} onChange={(value) => {
                                                session[e.name] = value
                                                setSession({
                                                    ...session,
                                                })
                                            }}>
                                            <Select.Option value={1}>The Match Prediction</Select.Option>
                                            <Select.Option value={2}>The Champion Prediction</Select.Option>
                                            <Select.Option value={3}>The Top Scorer Prediction</Select.Option>
                                        </Select>
                        }

                        if(e.type === 'combobox1'){
                            inputNode = <Select defaultValue={0} style={{ width: 200 }} onChange={(value) => {
                                                session[e.name] = value
                                                setSession({
                                                    ...session,
                                                })
                                            }}>
                                            <Select.Option value={0}>Comming Soon</Select.Option>
                                            <Select.Option value={1}>Is Comming</Select.Option>
                                            <Select.Option value={2}>Expired</Select.Option>
                                        </Select>
                        }

                        if(e.type === 'string'){
                            inputNode = <Input className={classes.inputForm} value={session[e.name]} onChange={(event) => {
                                session[e.name] = event.target.value
                                setSession({
                                    ...session,
                                })
                            }}/>
                        }

                        if(e.type === 'switch'){
                            inputNode = <div>
                                            <Switch onChange={(checked) => setSession({
                                                ...session,
                                                status: checked == true ? 1 : 0
                                            })} defaultChecked={session.status == 1 ? true : false}/>
                                            <span style={{
                                                color: 'rgba(0,0,0,0.4)',
                                                marginLeft: 8
                                            }}>{session.status ? t('active') : t('inactive')}</span>
                                        </div>
                        }

                        if(e.type === 'date'){
                            inputNode = <DatePicker showTime className={classes.inputForm} onChange={(date, dateString) => {
                                            session[e.name] = dateString
                                            setSession({
                                                ...session,
                                            })
                                        }}/>
                        }

                        if(e.type === 'result'){
                            if(type == 'createPrize'){
                                inputNode = <Input readOnly={true} className={classes.inputForm} value={session[e.name]} onChange={(event) => {
                                                session[e.name] = event.target.value
                                                setSession({
                                                    ...session,
                                                })
                                            }}/>
                            }else{
                                if(session['type'] === 1){
                                    inputNode = <Select defaultValue="WIN" style={{ width: 200 }} onChange={(value) => {
                                                        session[e.name] = value
                                                        setSession({
                                                            ...session,
                                                        })
                                                    }}>
                                                    <Select.Option value="WIN">WIN</Select.Option>
                                                    <Select.Option value="DRAWN">DRAWN</Select.Option>
                                                    <Select.Option value="LOSE">LOSE</Select.Option>
                                                </Select>
                                }else if(session['type'] === 2){
                                    inputNode = <AutoComplete
                                                    className={classes.inputForm}
                                                    options={options2}
                                                    style={{width: 200}}
                                                    onSelect={onSelect2}
                                                    onSearch={() => {
                                                    }}
                                                    placeholder="Please select team"
                                                />
                                }else if(session['type'] === 3){
                                    inputNode = <AutoComplete
                                                    className={classes.inputForm}
                                                    options={options3}
                                                    style={{width: 200}}
                                                    onSelect={onSelect3}
                                                    onSearch={() => {
                                                    }}
                                                    placeholder="Please select player"
                                                />
                                }

                            }
                        }

                               
                        return (<Form.Item
                            className={classes.formItem}
                            name={e.name}
                            initialValue={initValue(session[e.name], e.type)}
                            label={e.title}
                            rules={[{
                                required: e.required,
                                message: t('formItemRequire').replace("{title}", e.title),
                            }]}
                            
                        >
                            {inputNode}
                        </Form.Item>)
                    }
                )}
            </Form>
        </>
    )
}

export default EditSession;
