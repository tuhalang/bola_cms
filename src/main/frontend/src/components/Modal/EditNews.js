import React, {useEffect, useState} from 'react';
import {Button, Form, Input, InputNumber, Modal, notification, Switch, Tooltip, Select, DatePicker} from "antd";
import _ from 'lodash';
import {makeStyles} from "@material-ui/core/styles";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faImage, faUpload} from "@fortawesome/free-solid-svg-icons";
import UploadFile from "../UploadFile";
import utils from "../../common/utils";
import {DNS_IMAGE} from "../../consts";
import {useTranslation} from "react-i18next";
import styles from 'assets/jss/cms/components/modal';
import moment from 'moment';

const useStyles = makeStyles(styles);

const layout = {
    labelCol: {span: 8},
    wrapperCol: {span: 16},
};
const tailLayout = {
    wrapperCol: {offset: 8, span: 16},
};



function EditNews(props) {
    const classes = useStyles();
    const {
        dataProps,
        action,
        type,
    } = props
    const {form} = Form.useForm();
    const {t} = useTranslation('translation')

    const columns = [
        {
            title: t('contentEn'),
            type: 'string',
            name: 'contentEn',
            value: dataProps['contentEn']
        },
        {
            title: t('contentLc'),
            type: 'string',
            name: 'contentLc',
            value: dataProps['contentLc']
        },
        {
            title: t('newsType'),
            type: 'combobox',
            name: 'newsType',
            value: dataProps['newsType']
        },
        {
            title: t('publishTime'),
            type: 'date',
            name: 'publishTime',
            value: dataProps['publishTime']
        },
        {
            title: t('testMobilePhone'),
            type: 'string1',
            name: 'testMobilePhone',
            value: dataProps['testMobilePhone']
        },
        {
            title: t('status'),
            type: 'combobox1',
            name: 'status',
            value: dataProps['status']
        }
    ]

    const initValue = (data, type) => {
        if(type === 'date'){
            if(data === undefined || data === null || data === ''){
                return moment(new Date())
            }
            return moment(data)
        }
        return data
    }


    const [news, setNews] = useState({
        contentEn: dataProps['contentEn'] || '',
        contentLc: dataProps['contentLc'] || '',
        id: dataProps['id'] || '',
        newsType: dataProps['newsType'] || 1,
        status: dataProps['status'] || 3,
        publishTime: dataProps['publishTime'] || '2021-06-12 00:00:00',
        testMobilePhone: dataProps['testMobilePhone'] || ''
    })

    const onFinish = (values) => {
        action(news)
    }

    return (
        <>
            <Form onFinish={onFinish} {...layout} name={type} form={form}
                  id={type}>
                {_.map(type == 'createNews' ? columns : columns, e => {
                        
                        let inputNode = null

                        if(e.type === 'number'){
                            inputNode = <InputNumber className={classes.inputForm} value={news[e.name]} onChange={(value) => {
                                            news[e.name] = value
                                            setNews({
                                                ...news,
                                            })
                                        }
                                        } min={0}/>
                        }

                        if(e.type == 'string'){
                            inputNode = <Input.TextArea rows={4} className={classes.inputForm} value={news[e.name]} onChange={(event) => {
                                news[e.name] = event.target.value
                                setNews({
                                    ...news,
                                })
                            }}/>
                        }

                        if(e.type == 'string1'){
                            inputNode = <Input className={classes.inputForm} value={news[e.name]} onChange={(event) => {
                                news[e.name] = event.target.value
                                setNews({
                                    ...news,
                                })
                            }}/>
                        }

                        if(e.type === 'switch'){
                            inputNode = <div>
                                            <Switch onChange={(checked) => setNews({
                                                ...news,
                                                status: checked == true ? 1 : 0
                                            })} defaultChecked={news.status == 1 ? true : false}/>
                                            <span style={{
                                                color: 'rgba(0,0,0,0.4)',
                                                marginLeft: 8
                                            }}>{news.status ? t('active') : t('inactive')}</span>
                                        </div>
                        }

                        if(e.type === 'date'){
                            inputNode = <DatePicker showTime className={classes.inputForm} onChange={(date, dateString) => {
                                            news[e.name] = dateString
                                            setNews({
                                                ...news,
                                            })
                                        }}/>
                        }

                        if(e.type === 'combobox'){
                            inputNode = <Select  defaultValue="1" style={{ width: 200 }} onChange={(value) => {
                                                news[e.name] = value
                                                setNews({
                                                    ...news,
                                                })
                                            }}>
                                            <Select.Option value="1">Hot News</Select.Option>
                                            <Select.Option value="2">Schedule</Select.Option>
                                            <Select.Option value="3">Result</Select.Option>
                                        </Select>
                        }

                        if(e.type === 'combobox1'){
                            inputNode = <Select disabled={news[e.name] == 3} defaultValue={3} style={{ width: 200 }} onChange={(value) => {
                                                news[e.name] = value
                                                setNews({
                                                    ...news,
                                                })
                                            }}>
                                            <Select.Option value={3}>NEW</Select.Option>
                                            <Select.Option value={2}>TESTED</Select.Option>
                                            <Select.Option value={1}>APPROVE</Select.Option>
                                        </Select>
                        }
                        


                        return (<Form.Item
                            className={classes.formItem}
                            name={e.name}
                            initialValue={initValue(news[e.name], e.type)}
                            label={e.title}
                            rules={[{
                                required: true,
                                message: t('formItemRequire').replace("{title}", e.title),
                            }]}
                        >
                            {inputNode}
                        </Form.Item>)
                    }
                )}
            </Form>
        </>
    )
}

export default EditNews;
