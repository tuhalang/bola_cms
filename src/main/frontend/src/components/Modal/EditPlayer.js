import React, {useState} from 'react';
import {AutoComplete, Form, Input, InputNumber} from "antd";
import _ from 'lodash';
import {makeStyles} from "@material-ui/core/styles";
import {useTranslation} from "react-i18next";
import styles from 'assets/jss/cms/components/modal'


const useStyles = makeStyles(styles);

const layout = {
    labelCol: {span: 8},
    wrapperCol: {span: 16},
};

function EditPlayer(props) {
    const classes = useStyles();
    const {
        dataProps,
        action,
        type,
    } = props
    const {form} = Form.useForm();
    const {t} = useTranslation('translation')
    const [options, setOptions] = React.useState([]);

    const columns = [
        {
            title: t('nameEn'),
            type: 'string',
            name: 'nameEn',
            value: dataProps['nameEn']
        },
        {
            title: t('nameLc'),
            type: 'string',
            name: 'nameLc',
            value: dataProps['nameLc']
        },
        {
            title: t('descEn'),
            type: 'string',
            name: 'descEn',
            value: dataProps['descEn']
        },
        {
            title: t('descLc'),
            type: 'string',
            name: 'descLc',
            value: dataProps['descLc']
        },
        {
            title: t('number'),
            type: 'number',
            name: 'number',
            value: dataProps['number']
        },
        {
            title: t('imageUrl'),
            type: 'string',
            name: 'imageUrl',
            value: dataProps['imageUrl']
        },
        {
            title: t('position'),
            type: 'string',
            name: 'position',
            value: dataProps['position']
        },
        {
            title: t('team'),
            type: 'auto',
            name: 'team',
            value: dataProps['team']
        }
    ]


    const [player, setPlayer] = useState({
        descEn: dataProps['descEn'] || '',
        descLc: dataProps['descLc'] || '',
        imageUrl: dataProps['imageUrl'] || '',
        number: dataProps['number'] != null && dataProps['price'] >= 0 ? dataProps['price'] : 0,
        id: dataProps['id'],
        nameEn: dataProps['nameEn'] || '',
        nameLc: dataProps['nameLc'] || '',
        position: dataProps['position'],
        team: dataProps['team']
    })

    const onFinish = (values) => {
        action(player)
    }

    const onSelect = (data) => {
        player['team'] = _.find(props.teams, e => e.code == data).id
        setPlayer({
            ...player,
        })
    };

    React.useEffect(() => {
        if (props.teams && props.teams.length > 0) {
            console.log(props.teams, "props.teams,")
            setOptions(_.map(_.unionBy(props.teams, e => e.nameEn), e => {
                return {
                    value: e.code,
                }
            }))
        } else {
            props.getListTeam({key: '', page: 1, size: 100, status: 1})
        }
    }, [props.teams])

    return (
        <>
            <Form onFinish={onFinish} {...layout} name={type} form={form}
                  id={type}>
                {_.map(type == 'createPlayer' ? _.remove(columns, o => o.name != 'id') : columns, e => {
                        const inputNode = e.type === 'number' ?
                            <InputNumber className={classes.inputForm} value={player[e.name]} onChange={(value) => {
                                player[e.name] = value
                                setPlayer({
                                    ...player,
                                })
                            }
                            } min={0}/> : e.type === 'string'
                                ? <Input className={classes.inputForm} value={player[e.name]} onChange={(event) => {
                                    player[e.name] = event.target.value
                                    setPlayer({
                                        ...player,
                                    })
                                }}/> : e.type === 'auto' ? <AutoComplete
                                    className={classes.inputForm}
                                    options={options}
                                    style={{width: 200}}
                                    onSelect={onSelect}
                                    onSearch={() => {
                                    }}
                                    placeholder="Please select team"
                                /> : <div/>;
                        return (<Form.Item
                            className={classes.formItem}
                            name={e.name}
                            initialValue={player[e.name]}
                            label={e.title}
                            rules={[{
                                required: true,
                                message: t('formItemRequire').replace("{title}", e.title),
                            }]}
                        >
                            {inputNode}
                        </Form.Item>)
                    }
                )}
            </Form>
        </>
    )
}

export default EditPlayer;
