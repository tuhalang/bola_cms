import React, {useEffect} from 'react';
import {useDropzone} from 'react-dropzone';
import axios from 'axios';
import Button from '@material-ui/core/Button'
import _ from 'lodash'
import {notification} from "antd";

function UploadFile(props) {
    const {type, callback, ext} = props
    const {acceptedFiles, getRootProps, getInputProps} = useDropzone({
        accept: ext.join(', ') || 'image/jpeg, image/png, image/jpg',
        multiple: false
    });

    const uploadFile = async (files) => {
        if (files == null || files.length == 0) return;
        let file = files[0]
        console.log(file, "type of file")
        let filePath = file.name
        callback(type, file, filePath)
        // const resp = await axios.post('http://127.0.0.1:5000/upload_file', formData, {})
        // await handlerResp('Upload thành công', 'Upload thất bại', resp.status == 200 ? 1 : 0)
    }

    const handlerResp = async (content_success, content_fail, status) => {
        if (status == 1) {
            notification['success']({
                message: 'Thông báo',
                description:
                content_success,
            });
        } else {
            notification['error']({
                message: 'Thông báo',
                description:
                content_fail,
            });
        }
    }

    const files = acceptedFiles.map(file => {
        return (
            <li key={file.path}>
                {file.path} - {file.size} bytes
            </li>
        )
    });

    useEffect(() => {
        console.log(ext.join(', '), 'ext')
    }, [])

    return (
        <section className="container" style={{display: 'flex', justifyContent: 'center', flexDirection: 'column'}}>
            <div {...getRootProps({className: 'dropzone'})}
                 style={{
                     borderStyle: 'dotted',
                     borderWidth: 2,
                     borderColor: 'rgba(0,0,0,0.2)',
                     width: '100%',
                     display: 'flex',
                     justifyContent: 'center',
                     alignItems: 'center',
                     marginTop: '10%',
                     paddingTop: 16,
                     paddingBottom: 16,
                     marginBottom: 8
                 }}>
                <input {...getInputProps()} />
                <div>
                    <span style={{color: 'rgba(0,0,0,0.4)', width: '100%'}}>Kéo thả tệp hoặc nhấn để chọn tệp (jpg, png, jpeg, xlsx)</span>
                </div>
            </div>
            <aside>
                <h4>Tệp đã chọn</h4>
                <ul>{files}</ul>
                <Button variant="contained" color="primary" onClick={() => uploadFile(acceptedFiles)}
                        style={{
                            backgroundColor: 'rgba(0,0,0,0.1)',
                            borderRadius: 8,
                            padding: 8,
                            textAlign: 'center',
                            color: 'black',
                            width: '100%'
                        }}>
                    Upload
                </Button>
            </aside>
        </section>
    );
}

export default UploadFile;
