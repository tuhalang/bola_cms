import React, {useEffect} from "react";
import classNames from "classnames";
import {makeStyles} from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Hidden from "@material-ui/core/Hidden";
import Poppers from "@material-ui/core/Popper";
import Divider from "@material-ui/core/Divider";
import Person from "@material-ui/icons/Person";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import Dashboard from "@material-ui/icons/Dashboard";
import Search from "@material-ui/icons/Search";
import Button from "components/CustomButtons/Button.js";
import {useHistory} from "react-router-dom";
import styles from "assets/jss/cms/components/headerLinksStyle.js";
import _ from "lodash";
import {AutoComplete} from "antd";
import {Input} from "@material-ui/core";
import consts from "../../consts";
import {faGlobe} from "@fortawesome/free-solid-svg-icons";
import {useTranslation} from "react-i18next";
import {blackColor, hexToRgb, primaryColor, whiteColor} from "../../assets/jss/material-dashboard-react";

const useStyles = makeStyles(styles);

export default function AdminNavbarLinks(props) {
    const classes = useStyles();
    const {routes, makeBrand} = props
    const [openNotification, setOpenNotification] = React.useState(null);
    const [openProfile, setOpenProfile] = React.useState(null);
    const [value, setValue] = React.useState('');
    const [options, setOptions] = React.useState([])
    const [type, setType] = React.useState('')
    const history = useHistory()
    const {t, i18n} = useTranslation(['translation']);

    const handleClickNotification = event => {
        if (openNotification && openNotification.contains(event.target)) {
            setOpenNotification(null);
        } else {
            setOpenNotification(event.currentTarget);
        }
    };
    const handleLanguage = (lng = '') => {
        setOpenNotification(false)
        i18n.changeLanguage(lng == '' ? 'en' : lng);
    };
    const handleClickProfile = event => {
        if (openProfile && openProfile.contains(event.target)) {
            setOpenProfile(null);
        } else {
            setOpenProfile(event.currentTarget);
        }
    };
    const handleLogout = () => {
        props.logout()
    };

    const handleCloseProfile = () => {
        setOpenProfile(null);
    }

    const onSearchComplete = () => {
        props.callback(_.isFunction(makeBrand) ? makeBrand() : '', value)
    }

    const onSelect = (data) => {
        props.callback(_.isFunction(makeBrand) ? makeBrand() : '', data)
    };

    const onSearch = (searchText) => {
        setValue(searchText)
        props.onSearchProduct({
            key: searchText,
            page: 1,
            size: consts.MAX_AUTOCOMPLETE,
            status: 1
        }, () => {
        })
        props.callback(_.isFunction(makeBrand) ? makeBrand() : '', searchText)
    };

    const underlineClasses = classNames({
        [classes.underlineSuccess]: false,
        [classes.underline]: true
    });
    const marginTop = classNames({
        [classes.marginTop]: true
    });

    useEffect(() => {
        let screen = _.isFunction(makeBrand) ? makeBrand() : '';
        setType(screen)
        switch (screen) {
            case 'Manage products':
                setOptions(props.products)
                break
            case 'Manage sessions':
                setOptions(props.sessions)
            default:
                break
        }
    }, [props.products])

    console.log(i18n.language, "language")

    return (
        <div>
            <div className={classes.searchWrapper}>
                <AutoComplete
                    options={options}
                    children={
                        <Input
                            placeholder={t('search')}
                            inputProps={{
                                "aria-label": "Search"
                            }}
                            classes={{
                                root: marginTop,
                                disabled: classes.disabled,
                                underline: underlineClasses
                            }}
                        />
                    }
                    onSelect={onSelect}
                    onSearch={onSearch}
                />
                <Button color="white" aria-label="edit" justIcon round onClick={onSearchComplete}>
                    <Search/>
                </Button>
            </div>
            <Button
                color={window.innerWidth > 959 ? "transparent" : "white"}
                justIcon={window.innerWidth > 959}
                simple={!(window.innerWidth > 959)}
                aria-label="Dashboard"
                className={classes.buttonLink}
            >
                <Dashboard className={classes.icons}/>
                <Hidden mdUp implementation="css">
                    <p className={classes.linkText}>Dashboard</p>
                </Hidden>
            </Button>
            <div className={classes.manager}>
                <Button
                    color={window.innerWidth > 959 ? "transparent" : "white"}
                    justIcon={window.innerWidth > 959}
                    simple={!(window.innerWidth > 959)}
                    aria-owns={openNotification ? "language-menu-list-grow" : null}
                    aria-haspopup="true"
                    onClick={handleClickNotification}
                    className={classes.buttonLink}
                >
                    <FontAwesomeIcon icon={faGlobe}/>
                </Button>
                <Poppers
                    open={Boolean(openNotification)}
                    anchorEl={openNotification}
                    transition
                    disablePortal
                    className={
                        classNames({[classes.popperClose]: !openNotification}) +
                        " " +
                        classes.popperNav
                    }
                >
                    {({TransitionProps, placement}) => (
                        <Grow
                            {...TransitionProps}
                            id="language-menu-list-grow"
                            style={{
                                transformOrigin:
                                    placement === "bottom" ? "center top" : "center bottom"
                            }}
                        >
                            <Paper>
                                <ClickAwayListener onClickAway={() => setOpenNotification(false)}>
                                    <MenuList role="menu">
                                        <MenuItem
                                            onClick={() => handleLanguage('en')}
                                            className={classes.dropdownItem}
                                            style={i18n.language.startsWith('en') ? {
                                                backgroundColor: primaryColor[0],
                                                color: whiteColor,
                                                boxShadow:
                                                    "0 4px 20px 0 rgba(" +
                                                    hexToRgb(blackColor) +
                                                    ",.14), 0 7px 10px -5px rgba(" +
                                                    hexToRgb(primaryColor[0]) +
                                                    ",.4)"
                                            } : {}}
                                        >
                                            {t('en')}
                                        </MenuItem>
                                        <MenuItem
                                            onClick={() => handleLanguage('vi')}
                                            className={classes.dropdownItem}
                                            style={i18n.language.startsWith('vi') ? {
                                                backgroundColor: primaryColor[0],
                                                color: whiteColor,
                                                boxShadow:
                                                    "0 4px 20px 0 rgba(" +
                                                    hexToRgb(blackColor) +
                                                    ",.14), 0 7px 10px -5px rgba(" +
                                                    hexToRgb(primaryColor[0]) +
                                                    ",.4)"
                                            } : {}}
                                        >
                                            {t('vi')}
                                        </MenuItem>
                                    </MenuList>
                                </ClickAwayListener>
                            </Paper>
                        </Grow>
                    )}
                </Poppers>
            </div>
            <div className={classes.manager}>
                <Button
                    color={window.innerWidth > 959 ? "transparent" : "white"}
                    justIcon={window.innerWidth > 959}
                    simple={!(window.innerWidth > 959)}
                    aria-owns={openProfile ? "profile-menu-list-grow" : null}
                    aria-haspopup="true"
                    onClick={handleClickProfile}
                    className={classes.buttonLink}
                >
                    <Person className={classes.icons}/>
                    <Hidden mdUp implementation="css">
                        <p className={classes.linkText}>Profile</p>
                    </Hidden>
                </Button>
                <Poppers
                    open={Boolean(openProfile)}
                    anchorEl={openProfile}
                    transition
                    disablePortal
                    className={
                        classNames({[classes.popperClose]: !openProfile}) +
                        " " +
                        classes.popperNav
                    }
                >
                    {({TransitionProps, placement}) => (
                        <Grow
                            {...TransitionProps}
                            id="profile-menu-list-grow"
                            style={{
                                transformOrigin:
                                    placement === "bottom" ? "center top" : "center bottom"
                            }}
                        >
                            <Paper>
                                <ClickAwayListener onClickAway={handleCloseProfile}>
                                    <MenuList role="menu">
                                        <MenuItem
                                            onClick={handleCloseProfile}
                                            className={classes.dropdownItem}
                                        >
                                            Profile
                                        </MenuItem>
                                        <Divider light/>
                                        <MenuItem
                                            onClick={handleLogout}
                                            className={classes.dropdownItem}
                                        >
                                            {t('logout')}
                                        </MenuItem>
                                    </MenuList>
                                </ClickAwayListener>
                            </Paper>
                        </Grow>
                    )}
                </Poppers>
            </div>
        </div>
    );
}
