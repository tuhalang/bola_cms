import React, {useEffect} from "react";
import Card from "../components/Card/Card";
import CardBody from "../components/Card/CardBody";
import styles from 'assets/jss/cms/views/team'
import {makeStyles} from "@material-ui/core/styles";
import {useTranslation} from "react-i18next";
import {AutoComplete, Form, Input, Modal, Table} from "antd";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit, faUpload} from "@fortawesome/free-solid-svg-icons";
import {Button} from "@material-ui/core";
import EditTeam from "../components/Modal/EditTeam";
import UploadFile from "../components/UploadFile";
import consts from "../consts";
import _ from "lodash";
import Loading from "../components/Loading";


const useStyles = makeStyles(styles);
const tailLayout = {
    wrapperCol: {offset: 8, span: 16},
};


function Team(props) {
    const classes = useStyles();
    const {t} = useTranslation();
    const {teams, totalElements, totalPages} = props;
    const [visible, setVisible] = React.useState(false);
    const [uploading, setUploading] = React.useState(false);
    const [loading, setLoading] = React.useState(false);
    const [page, setPage] = React.useState(1);
    const [dataEditing, setDataEditing] = React.useState(null);
    const [searchText, setSearchText] = React.useState('');

    const columns = [
        {
            title: t('no'),
            dataIndex: 'no',
            width: '5%',
        },
        {
            title: t('code'),
            dataIndex: 'code',
            width: '5%',
        },
        {
            title: t('nameEn'),
            dataIndex: 'nameEn',
            width: '9%',
        },
        {
            title: t('nameLc'),
            dataIndex: 'nameLc',
            width: '9%',
        },
        {
            title: t('descEn'),
            dataIndex: 'descEn',
            width: '12%',
        },
        {
            title: t('descLc'),
            dataIndex: 'descLc',
            width: '12%',
            render: text => <span style={{wordWrap: 'break-word'}}>{text}</span>
        },
        {
            title: t('group'),
            dataIndex: 'group',
            width: '5%',
        },
        {
            title: t('imageUrl'),
            dataIndex: 'imageUrl',
            width: '8%',
        },
        {
            title: t('scores'),
            dataIndex: 'scores',
            width: '5%',
        },
        {
            title: t('state'),
            dataIndex: 'state',
            width: '5%',
            render: (__, {state}) => {
                return <span>{_.find(consts.TYPE_STATUS_TEAM, e => e.value == state).label}</span>
            }
        },
        {
            title: t('status'),
            dataIndex: 'status',
            width: '5%',
            render: (__, {status}) => {
                return <span>{_.find(consts.TYPE_STATUS_HISTORY, e => e.value == status).label}</span>
            }
        },
        {
            title: t('update'),
            dataIndex: 'update',
            render: (_, record) => {
                return <a onClick={() => {
                    setDataEditing(record)
                }}>{t('update')}</a>
            }
        },
    ];


    const handleOk = () => {
        setVisible(false);
        setDataEditing(null);
    };

    const handleCancel = () => {
        setVisible(false);
        setDataEditing(null);
    };

    const fetchData = (key = searchText, page = 1, size = consts.PAGE_SIZE) => {
        setLoading(true)
        props.getListTeam({
            key,
            page,
            size,
            status: 1,
        }, () => {
            setLoading(false)
        })
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div>
            <Card className={classes.searchHeader}>
                <Input
                    className={classes.searchInput}
                    allowClear={true}
                    onChange={e => {
                        setSearchText(e.target.value)
                        fetchData(e.target.value)
                    }}
                    placeholder={'Search team...'}
                />
                <Button onClick={() => {
                    setVisible(true)
                }}>
                    <FontAwesomeIcon icon={faEdit}/>
                </Button>
                <Button onClick={() => setUploading(true)}>
                    <FontAwesomeIcon icon={faUpload}/>
                </Button>
            </Card>
            <Card>
                <CardBody>
                    <Table bordered
                           scroll={{
                               x: 'max-content',
                           }}
                           dataSource={_.map(teams, (e, idx) => _.assign(e, {no: (page - 1) * consts.PAGE_SIZE + idx + 1})) || []}
                           columns={columns}
                           rowClassName="editable-row"
                           pagination={{
                               defaultPageSize: consts.PAGE_SIZE,
                               current: page,
                               showSizeChanger: false,
                               total: totalElements,
                               onChange: (p, pageSize) => {
                                   setPage(p)
                                   setLoading(true)
                                   fetchData(searchText, p, pageSize)
                               },
                               hideOnSinglePage: true,
                               responsive: true
                           }} bordered
                    />

                    {loading ? <Loading/> : <div/>}
                </CardBody>
            </Card>
            <Modal
                title="Create Team"
                visible={visible}
                onOk={handleOk}
                destroyOnClose={true}
                onCancel={handleCancel}
                width={820}
                footer={[
                    <Form.Item {...tailLayout}>
                        <Button form="createTeam" key="submit" htmlType="submit" type={'primary'}>
                            {t('create')}
                        </Button>
                    </Form.Item>
                ]}
            >
                <EditTeam dataProps={{}}
                          action={(team) => props.createTeam({...team}, () => {
                              fetchData()
                              handleOk()
                          })}
                          type={'createTeam'}/>
            </Modal>

            <Modal
                centered
                visible={uploading}
                destroyOnClose={true}
                onOk={() => setUploading(false)}
                onCancel={() => setUploading(false)}
                footer={[]}>
                <UploadFile type={uploading} callback={async (type, file, filePath) => {
                    // files[type] = await utils.getBase64(file)
                    // product[type] = await filePath
                    // setProduct({
                    //     ...product
                    // })
                    // setUploading('')
                }} ext={['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']}/>
            </Modal>
            <Modal
                title="Update Team"
                visible={dataEditing ? true : false}
                onOk={handleOk}
                destroyOnClose={true}
                onCancel={handleCancel}
                width={820}
                footer={[
                    <Form.Item {...tailLayout}>
                        <Button form="updateTeam" key="submit" htmlType="submit" type={'primary'}>
                            {t('update')}
                        </Button>
                    </Form.Item>
                ]}

            >
                <EditTeam dataProps={dataEditing}
                          action={(team) => props.updateTeam({...team}, () => {
                              fetchData()
                              handleOk()
                          })}
                          type={'updateTeam'}/>
            </Modal>
        </div>
    )
}

export default Team;
