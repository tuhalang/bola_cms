import React, {useEffect} from "react";
import {useTranslation} from "react-i18next";
import {makeStyles} from "@material-ui/core/styles";
import styles from "../assets/jss/cms/views/prize";
import CardBody from "../components/Card/CardBody";
import Card from "../components/Card/Card";
import {Form, Input, Modal, Table, AutoComplete, Alert} from "antd";
import {Button} from "@material-ui/core";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit} from "@fortawesome/free-solid-svg-icons";
import consts from "../consts";
import EditPrize from "../components/Modal/EditPrize";
import EditPrizePackage from "../components/Modal/EditPrizePackage";
import _ from "lodash";
import Loading from "../components/Loading";

const useStyles = makeStyles(styles);
const tailLayout = {
    wrapperCol: {offset: 8, span: 16},
};

function PrizePackage(props) {
    const classes = useStyles();
    const {t} = useTranslation();
    const {prizePackage, totalElements, totalPages, packages, totalPackageElements, totalPackagePages} = props;
    const [visible, setVisible] = React.useState(false);
    const [loading, setLoading] = React.useState(false);
    const [page, setPage] = React.useState(1);
    const [dataEditing, setDataEditing] = React.useState(null);
    const [searchText, setSearchText] = React.useState('');
    const [options, setOptions] = React.useState([]);

    const [packageId, setPackageId] = React.useState(null)

    const handleOk = () => {
        setVisible(false);
        setDataEditing(null);
    };

    const handleCancel = () => {
        setVisible(false);
        setDataEditing(null);
    };

    const fetchData = (packageId = '', page = 1, size = consts.PAGE_SIZE) => {
        setLoading(true)
        props.getListPrizePackage({
            packageId,
            page,
            size,
            status: 1,
        }, () => {
            setLoading(false)
        })
    }

    const onSelect = (data) => {
        let packageId = _.find(props.packages, e => e.nameEn == data).id
        setPackageId(packageId)
        fetchData(packageId, 1, consts.PAGE_SIZE)
    };

    React.useEffect(() => {
        if (props.packages && props.packages.length > 0) {
            setOptions(_.map(_.unionBy(props.packages, e => e.id), e => {
                return {
                    value: e.nameEn
                }
            }))
        } else {
            props.getListPackage({key: '', page: 1, size: 100, status: 1})
        }
    }, [props.packages])

    const columns = [
        {
            title: t('no'),
            dataIndex: 'no',
            width: '5%',
        },
        {
            title: t('package'),
            dataIndex: 'winnerPackageName',
            width: '20%',
        },
        {
            title: t('prize'),
            dataIndex: 'prizeName',
            width: '20%',
        },
        {
            title: t('rank'),
            dataIndex: 'rank',
            width: '10%',
        },
        {
            title: t('amount'),
            dataIndex: 'amount',
            width: '10%',
            render: text => <span style={{wordWrap: 'break-word'}}>{text}</span>
        },
        {
            title: t('status'),
            dataIndex: 'status',
            width: '9%',
        },
        {
            title: t('update'),
            dataIndex: 'update',
            render: (_, record) => {
                return <a onClick={() => {
                    setDataEditing(record)
                }}>{t('update')}</a>
            }
        },
    ];


    useEffect(() => {
        fetchData()
    }, [])


    return (
        <div>
            <Card className={classes.searchHeader}>
                <AutoComplete
                                    className={classes.searchInput}
                                    allowClear={true}
                                    options={options}
                                    onSelect={onSelect}
                                    onSearch={() => {
                                    }}
                                    placeholder="Please select package"
                                />
                <Button onClick={() => {
                    setVisible(true)
                }}>
                    <FontAwesomeIcon icon={faEdit}/>
                </Button>
            </Card>
            <Card>
                <CardBody>
                    <Table bordered
                           scroll={{
                               x: 'max-content',
                           }}
                           dataSource={_.map(prizePackage, (e, idx) => _.assign(e, {no: (page - 1) * consts.PAGE_SIZE + idx + 1})) || []}
                           columns={columns}
                           rowClassName="editable-row"
                           pagination={{
                               defaultPageSize: consts.PAGE_SIZE,
                               current: page,
                               showSizeChanger: false,
                               total: totalElements,
                               onChange: (p, pageSize) => {
                                   setPage(p)
                                   setLoading(true)
                                   if(packageId != null){
                                      fetchData(packageId, p, consts.PAGE_SIZE, 1)
                                   }else{
                                      fetchData('', p, consts.PAGE_SIZE, 1)
                                   }
                                   
                               },
                               hideOnSinglePage: true,
                               responsive: true
                           }} bordered
                    />

                    {loading ? <Loading/> : <div/>}
                </CardBody>
            </Card>
            <Modal
                title="Edit Prize"
                visible={visible}
                onOk={handleOk}
                destroyOnClose={true}
                onCancel={handleCancel}
                width={820}
                footer={[
                    <Form.Item {...tailLayout}>
                        <Button form="createPrize" key="submit" htmlType="submit" type={'primary'}>
                            {t('create')}
                        </Button>
                    </Form.Item>
                ]}
            >
                <EditPrizePackage dataProps={{}}
                           action={(prize) => props.createPrizePackage({...prize}, () => {
                            if(packageId != null){
                                fetchData(packageId, 1, consts.PAGE_SIZE, 1)
                             }else{
                                fetchData('', 1, consts.PAGE_SIZE, 1)
                             }
                               handleOk()
                           })}
                           {...props}
                           type={'createPrize'}/>
            </Modal>
            <Modal
                title="Update Prize"
                visible={dataEditing ? true : false}
                onOk={handleOk}
                destroyOnClose={true}
                onCancel={handleCancel}
                width={820}
                footer={[
                    <Form.Item {...tailLayout}>
                        <Button form="updatePrize" key="submit" htmlType="submit" type={'primary'}>
                            {t('update')}
                        </Button>
                    </Form.Item>
                ]}
            >
                <EditPrizePackage dataProps={dataEditing}
                           action={(prize) => props.updatePrizePackage({...prize}, () => {
                               
                            if(packageId != null){
                                fetchData(packageId, 1, consts.PAGE_SIZE, 1)
                             }else{
                                fetchData('', 1, consts.PAGE_SIZE, 1)
                             }
                               handleOk()
                           })}
                           {...props}
                           type={'updatePrize'}/>
            </Modal>
        </div>
    )
}

export default PrizePackage;
