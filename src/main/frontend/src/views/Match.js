import React, {useEffect} from "react";
import {useTranslation} from "react-i18next";
import {makeStyles} from "@material-ui/core/styles";
import styles from "../assets/jss/cms/views/match";
import CardBody from "../components/Card/CardBody";
import Card from "../components/Card/Card";
import {Form, Input, Modal, Table, Divider, AutoComplete} from "antd";
import {Button} from "@material-ui/core";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit, faUpload} from "@fortawesome/free-solid-svg-icons";
import UploadFile from "../components/UploadFile";
import consts from "../consts";
import _ from "lodash";
import Loading from "../components/Loading";
import EditMatch from "../components/Modal/EditMatch";


const useStyles = makeStyles(styles);
const tailLayout = {
    wrapperCol: {offset: 8, span: 16},
};

function Match(props) {
    const classes = useStyles();
    const {t} = useTranslation();
    const {match, totalElements, totalPages} = props;
    const [visible, setVisible] = React.useState(false);
    const [uploading, setUploading] = React.useState(false);
    const [loading, setLoading] = React.useState(false);
    const [page, setPage] = React.useState(1);
    const [dataEditing, setDataEditing] = React.useState(null);
    const [searchText, setSearchText] = React.useState('');
    const [stateMatch, setStateMatch] = React.useState('');

    const handleOk = () => {
        setVisible(false);
        setDataEditing(null);
    };

    const handleCancel = () => {
        setVisible(false);
        setDataEditing(null);
    };

    const fetchData = (stateMatch=stateMatch, key = searchText, page = 1, size = consts.PAGE_SIZE) => {
        setLoading(true)
        props.getListMatch({
            key,
            stateMatch,
            page,
            size,
            status: null,
        }, () => {
            setLoading(false)
        })
    }

    const onSelect = (data) => {
        setStateMatch(data)
        fetchData(data, searchText, 1, consts.PAGE_SIZE)
    };

    const options = [
        {value: "Group stage"},
        {value: "Round of 16"},
        {value: "Quater-finals"},
        {value: "Semi-finals"},
        {value: "Final"}
    ]

    const columns = [
        {
            title: t('no'),
            dataIndex: 'no',
            width: '4%',
        },
        {
            title: t('firstTeam'),
            dataIndex: 'firstTeam',
            width: '8%',
        },
        {
            title: t('firstTeamGoals'),
            dataIndex: 'firstTeamGoals',
            width: '6%',
        },
        {
            title: t('secondTeam'),
            dataIndex: 'secondTeam',
            width: '8%',
        },
        {
            title: t('secondTeamGoals'),
            dataIndex: 'secondTeamGoals',
            width: '6%',
        },
        {
            title: t('state'),
            dataIndex: 'state',
            width: '8%',
        },
        {
            title: t('placeEn'),
            dataIndex: 'placeEn',
            width: '8%',
        },
        {
            title: t('placeLc'),
            dataIndex: 'placeLc',
            width: '8%',
        },
        {
            title: t('descEn'),
            dataIndex: 'descEn',
            width: '5%',
        },
        {
            title: t('descLc'),
            dataIndex: 'descLc',
            width: '5%',
        },
        {
            title: t('startTime'),
            dataIndex: 'startTime',
            width: '8%',
        },
        {
            title: t('endTime'),
            dataIndex: 'endTime',
            width: '8%',
        },
        {
            title: t('status'),
            dataIndex: 'status',
            width: '5%',
            render: (__, {status}) => {
                return <span>{_.find(consts.TYPE_STATUS_MATCH, e => e.value == status).label}</span>
            }
        },
        {
            title: t('update'),
            dataIndex: 'update',
            render: (_, record) => {
                return <a onClick={() => {
                    setDataEditing({})
                    setDataEditing(record)
                }}>{t('update')}</a>
            }
        },
    ];


    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div>
            <Card className={classes.searchHeader}>
                <AutoComplete
                    className={classes.searchInput}
                    allowClear={true}
                    options={options}
                    onSelect={onSelect}
                    onSearch={() => {
                    }}
                    onChange={() => {
                        setStateMatch(null)
                        fetchData(null, searchText, 1, consts.PAGE_SIZE)
                    }}
                    placeholder="Please select state"
                />
                
                <Button onClick={() => {
                    setVisible(true)
                }}>
                    <FontAwesomeIcon icon={faEdit}/>
                </Button>
                <Button onClick={() => setUploading(true)}>
                    <FontAwesomeIcon icon={faUpload}/>
                </Button>
            </Card>
            <Card>
                <CardBody>
                    <Table bordered
                           scroll={{
                               x: 'max-content',
                           }}
                           dataSource={_.map(match, (e, idx) => _.assign(e, {no: (page - 1) * consts.PAGE_SIZE + idx + 1})) || []}
                           columns={columns}
                           rowClassName="editable-row"
                           pagination={{
                               defaultPageSize: consts.PAGE_SIZE,
                               current: page,
                               showSizeChanger: false,
                               total: totalElements,
                               onChange: (p, pageSize) => {
                                   setPage(p)
                                   setLoading(true)
                                   fetchData(stateMatch, searchText, p, consts.PAGE_SIZE)
                               },
                               hideOnSinglePage: true,
                               responsive: true
                           }} bordered
                    />

                    {loading ? <Loading/> : <div/>}
                </CardBody>
            </Card>
            <Modal
                title="Edit Match"
                visible={visible}
                onOk={handleOk}
                destroyOnClose={true}
                onCancel={handleCancel}
                width={820}
                footer={[
                    <Form.Item {...tailLayout}>
                        <Button form="createMatch" key="submit" htmlType="submit" type={'primary'}>
                            {t('create')}
                        </Button>
                    </Form.Item>
                ]}
            >
                <EditMatch dataProps={{}}
                           action={(match) => props.createMatch({...match}, () => {
                               fetchData()
                               handleOk()
                           })}
                           {...props}
                           type={'createMatch'}/>
            </Modal>

            <Modal
                title="Update Match"
                visible={dataEditing ? true : false}
                onOk={handleOk}
                destroyOnClose={true}
                onCancel={handleCancel}
                width={820}
                footer={[
                    <Form.Item {...tailLayout}>
                        <Button form="updateMatch" key="submit" htmlType="submit" type={'primary'}>
                            {t('update')}
                        </Button>
                    </Form.Item>
                ]}
            >
                <EditMatch dataProps={dataEditing}
                           action={(match) => props.updateMatch({...match}, () => {
                               fetchData()
                               handleOk()
                           })}
                           {...props}
                           type={'updateMatch'}/>
            </Modal>

            <Modal
                centered
                visible={uploading}
                destroyOnClose={true}
                onOk={() => setUploading(false)}
                onCancel={() => setUploading(false)}
                footer={[]}>
                <UploadFile type={uploading} callback={async (type, file, filePath) => {
                    // files[type] = await utils.getBase64(file)
                    // product[type] = await filePath
                    // setProduct({
                    //     ...product
                    // })
                    // setUploading('')
                }} ext={['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']}/>
            </Modal>
        </div>
    )
}

export default Match;
