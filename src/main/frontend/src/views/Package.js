import React, {useEffect} from "react";
import {useTranslation} from "react-i18next";
import {makeStyles} from "@material-ui/core/styles";
import styles from "../assets/jss/cms/views/prize";
import CardBody from "../components/Card/CardBody";
import Card from "../components/Card/Card";
import {Form, Input, Modal, Table} from "antd";
import {Button} from "@material-ui/core";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit} from "@fortawesome/free-solid-svg-icons";
import consts from "../consts";
import Loading from "../components/Loading";
import _ from "lodash";
import EditPackage from "../components/Modal/EditPackage";


const useStyles = makeStyles(styles);
const tailLayout = {
    wrapperCol: {offset: 8, span: 16},
};

function Package(props) {
    const classes = useStyles();
    const {t} = useTranslation();
    const {winnerPackage, totalElements, totalPages} = props;
    const [visible, setVisible] = React.useState(false);
    const [loading, setLoading] = React.useState(false);
    const [page, setPage] = React.useState(1);
    const [dataEditing, setDataEditing] = React.useState(null);
    const [searchText, setSearchText] = React.useState('');

    const handleOk = () => {
        setVisible(false);
        setDataEditing(null);
    };

    const handleCancel = () => {
        setVisible(false);
        setDataEditing(null);
    };

    const fetchData = (key = searchText, page = 1, size = consts.PAGE_SIZE) => {
        setLoading(true)
        props.getListPackage({
            key,
            page,
            size,
            status: 1,
        }, () => {
            setLoading(false)
        })
    }

    const columns = [
        {
            title: t('no'),
            dataIndex: 'no',
            width: '5%',
        },
        {
            title: t('nameEn'),
            dataIndex: 'nameEn',
            width: '15%',
        },
        {
            title: t('nameLc'),
            dataIndex: 'nameLc',
            width: '15%',
        },
        {
            title: t('descLc'),
            dataIndex: 'descLc',
            width: '15%',
        },
        {
            title: t('descEn'),
            dataIndex: 'descEn',
            width: '15%',
            render: text => <span style={{wordWrap: 'break-word'}}>{text}</span>
        },
        {
            title: t('status'),
            dataIndex: 'status',
            width: '9%',
            render: (__, {status}) => {
                return <span>{_.find(consts.TYPE_STATUS_HISTORY, e => e.value == status).label}</span>
            }
        },
        {
            title: t('totalPrize'),
            dataIndex: 'totalPrize',
            width: '10%',
        },
        {
            title: t('update'),
            dataIndex: 'update',
            render: (_, record) => {
                return <a onClick={() => {
                    setDataEditing(record)
                }}>{t('update')}</a>
            }
        },
    ];


    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div>
            <Card className={classes.searchHeader}>
                <Input
                    className={classes.searchInput}
                    allowClear={true}
                    onChange={e=>{
                        setSearchText(e.target.value)
                        fetchData(e.target.value)
                    }}
                    placeholder={'Search package...'}
                />
                <Button onClick={() => {
                    setVisible(true)
                }}>
                    <FontAwesomeIcon icon={faEdit}/>
                </Button>
            </Card>
            <Card>
                <CardBody>
                    <Table bordered
                           scroll={{
                               x: 'max-content',
                           }}
                           dataSource={_.map(winnerPackage, (e, idx) => _.assign(e, {no: (page - 1) * consts.PAGE_SIZE + idx + 1})) || []}
                           columns={columns}
                           rowClassName="editable-row"
                           pagination={{
                               defaultPageSize: consts.PAGE_SIZE,
                               current: page,
                               showSizeChanger: false,
                               total: totalElements,
                               onChange: (p, pageSize) => {
                                   setPage(p)
                                   setLoading(true)
                                   fetchData(searchText, p, consts.PAGE_SIZE)
                               },
                               hideOnSinglePage: true,
                               responsive: true
                           }} bordered
                    />

                    {loading ? <Loading/> : <div/>}
                </CardBody>
            </Card>
            <Modal
                title="Edit Package"
                visible={visible}
                destroyOnClose={true}
                onOk={handleOk}
                onCancel={handleCancel}
                width={820}
                footer={[
                    <Form.Item {...tailLayout}>
                        <Button form="createPackage" key="submit" htmlType="submit" type={'primary'}>
                            {t('create')}
                        </Button>
                    </Form.Item>
                ]}
            >
                <EditPackage dataProps={{}}
                           action={(winnerPackage) => props.createPackage({...winnerPackage}, () => {
                               fetchData()
                               handleOk()
                           })}
                           type={'createPackage'}/>
            </Modal>
            <Modal
                title="Update Package"
                visible={dataEditing ? true : false}
                onOk={handleOk}
                destroyOnClose={true}
                onCancel={handleCancel}
                width={820}
                footer={[
                    <Form.Item {...tailLayout}>
                        <Button form="updatePackage" key="submit" htmlType="submit" type={'primary'}>
                            {t('update')}
                        </Button>
                    </Form.Item>
                ]}
            >
                <EditPackage dataProps={dataEditing}
                           action={(prize) => props.updatePackage({...prize}, () => {
                               fetchData()
                               handleOk()
                           })}
                           type={'updatePackage'}/>
            </Modal>
        </div>
    )
}

export default Package;
