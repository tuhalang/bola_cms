import React, {useEffect} from "react";
import {useTranslation} from "react-i18next";
import {makeStyles} from "@material-ui/core/styles";
import styles from "../assets/jss/cms/views/result";
import CardBody from "../components/Card/CardBody";
import Card from "../components/Card/Card";
import {AutoComplete, Input, Table} from "antd";
import consts from "../consts";
import _ from "lodash";
import Loading from "../components/Loading";

const useStyles = makeStyles(styles)

function Result(props) {
    const classes = useStyles();
    const {results, totalElements, totalPages} = props;
    const {t} = useTranslation();
    const [loading, setLoading] = React.useState(false);
    const [page, setPage] = React.useState(1);
    const [searchText, setSearchText] = React.useState('');
    const [options, setOptions] = React.useState([]);
    const [sessionId, setSessionId] = React.useState(null)


    const onSelect = (data) => {
        let sessionId = _.find(props.sessions, e => (e.nameEn + ': ' + (e.matchName==null?'':e.matchName)) == data).id
        setSessionId(sessionId)
        fetchData(sessionId)
    };

    React.useEffect(() => {
        if (props.sessions && props.sessions.length > 0) {
            setOptions(_.map(_.unionBy(props.sessions, e => e.id), e => {
                return {
                    value: e.nameEn + ': ' + (e.matchName==null?'':e.matchName)
                }
            }))
        } else {
            props.getListSession({key: '', page: 1, size: 100, status: 1})
        }
    }, [props.sessions])

    const fetchData = (predictionSession = searchText, page = 1, size = consts.PAGE_SIZE) => {
        setLoading(true)
        console.log(predictionSession,"predictionSession")
        props.getListResult({
            predictionSession,
            page,
            size,
        }, () => {
            setLoading(false)
        })
    }

    const columns = [
        {
            title: t('no'),
            dataIndex: 'no',
            width: '5%',
        },
        {
            title: t('isdn'),
            dataIndex: 'isdn',
            width: '10%',
        },
        {
            title: t('predictionSession'),
            dataIndex: 'predictionSession',
            width: '10%',
        },
        {
            title: t('predictionTurn'),
            dataIndex: 'predictionTurn',
            width: '10%',
        },
        {
            title: t('prize'),
            dataIndex: 'prize',
            width: '15%',
        },
        
        {
            title: t('rank'),
            dataIndex: 'rank',
            width: '10%',
        },
        {
            title: t('status'),
            dataIndex: 'status',
            width: '9%',
            render: (__, {status}) => {
                return <span>{_.find(consts.TYPE_STATUS_RESULT, e => e.value == status).label}</span>
            }
        }
    ];


    useEffect(() => {
        fetchData()
    }, [])


    return (
        <div>
            <Card className={classes.searchHeader}>
                <AutoComplete
                    className={classes.searchInput}
                    allowClear={true}
                    options={options}
                    onSelect={onSelect}
                    onSearch={() => {
                    }}
                    placeholder="Please select session"
                />
            </Card>
            <Card>
                <CardBody>
                    <Table bordered
                           scroll={{
                               x: 'max-content',
                           }}
                           dataSource={_.map(results, (e, idx) => _.assign(e, {no: (page - 1) * consts.PAGE_SIZE + idx + 1})) || []}
                           columns={columns}
                           rowClassName="editable-row"
                           pagination={{
                               defaultPageSize: consts.PAGE_SIZE,
                               current: page,
                               showSizeChanger: false,
                               total: totalElements,
                               onChange: (p, pageSize) => {
                                   setPage(p)
                                   setLoading(true)
                                   fetchData(p, consts.PAGE_SIZE, 1)
                               },
                               hideOnSinglePage: true,
                               responsive: true
                           }} bordered
                    />

                    {loading ? <Loading/> : <div/>}
                </CardBody>
            </Card>
        </div>
    )
}

export default Result;
