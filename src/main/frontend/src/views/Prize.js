import React, {useEffect} from "react";
import {useTranslation} from "react-i18next";
import {makeStyles} from "@material-ui/core/styles";
import styles from "../assets/jss/cms/views/prize";
import CardBody from "../components/Card/CardBody";
import Card from "../components/Card/Card";
import {Form, Input, Modal, Table} from "antd";
import {Button} from "@material-ui/core";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit} from "@fortawesome/free-solid-svg-icons";
import consts from "../consts";
import Loading from "../components/Loading";
import _ from "lodash";
import EditPrize from "../components/Modal/EditPrize";


const useStyles = makeStyles(styles);
const tailLayout = {
    wrapperCol: {offset: 8, span: 16},
};

function Prize(props) {
    const classes = useStyles();
    const {t} = useTranslation();
    const {prize, totalElements, totalPages} = props;
    const [visible, setVisible] = React.useState(false);
    const [loading, setLoading] = React.useState(false);
    const [page, setPage] = React.useState(1);
    const [dataEditing, setDataEditing] = React.useState(null);
    const [searchText, setSearchText] = React.useState('');

    const handleOk = () => {
        setVisible(false);
        setDataEditing(null);
    };

    const handleCancel = () => {
        setVisible(false);
        setDataEditing(null);
    };

    const fetchData = (key = searchText, page = 1, size = consts.PAGE_SIZE) => {
        setLoading(true)
        props.getListPrize({
            key,
            page,
            size,
            status: 1,
        }, () => {
            setLoading(false)
        })
    }

    const columns = [
        {
            title: t('no'),
            dataIndex: 'no',
            width: '5%',
        },
        {
            title: t('nameEn'),
            dataIndex: 'nameEn',
            width: '10%',
        },
        {
            title: t('nameLc'),
            dataIndex: 'nameLc',
            width: '10%',
        },
        {
            title: t('descLc'),
            dataIndex: 'descLc',
            width: '15%',
        },
        {
            title: t('descEn'),
            dataIndex: 'descEn',
            width: '15%',
            render: text => <span style={{wordWrap: 'break-word'}}>{text}</span>
        },
        {
            title: t('status'),
            dataIndex: 'status',
            width: '9%',
            render: (__, {status}) => {
                return <span>{_.find(consts.TYPE_STATUS_RESULT, e => e.value == status).label}</span>
            }
        },
        {
            title: t('type'),
            dataIndex: 'type',
            width: '10%',
            render: (__, {type}) => {
                return <span>{_.find(consts.TYPE_PRIZE, e => e.value == type).label}</span>
            }
        },
        {
            title: t('value'),
            dataIndex: 'value',
            width: '5%',
        },
        {
            title: t('update'),
            dataIndex: 'update',
            render: (_, record) => {
                return <a onClick={() => {
                    setDataEditing(record)
                }}>{t('update')}</a>
            }
        },
    ];


    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div>
            <Card className={classes.searchHeader}>
                <Input
                    className={classes.searchInput}
                    allowClear={true}
                    onChange={e=>{
                        setSearchText(e.target.value)
                        fetchData(e.target.value)
                    }}
                    placeholder={'Search prize...'}
                />
                <Button onClick={() => {
                    setVisible(true)
                }}>
                    <FontAwesomeIcon icon={faEdit}/>
                </Button>
            </Card>
            <Card>
                <CardBody>
                    <Table bordered
                           scroll={{
                               x: 'max-content',
                           }}
                           dataSource={_.map(prize, (e, idx) => _.assign(e, {no: (page - 1) * consts.PAGE_SIZE + idx + 1})) || []}
                           columns={columns}
                           rowClassName="editable-row"
                           pagination={{
                               defaultPageSize: consts.PAGE_SIZE,
                               current: page,
                               showSizeChanger: false,
                               total: totalElements,
                               onChange: (p, pageSize) => {
                                   setPage(p)
                                   setLoading(true)
                                   fetchData(searchText, p, consts.PAGE_SIZE)
                               },
                               hideOnSinglePage: true,
                               responsive: true
                           }} bordered
                    />

                    {loading ? <Loading/> : <div/>}
                </CardBody>
            </Card>
            <Modal
                title="Edit Prize"
                visible={visible}
                onOk={handleOk}
                destroyOnClose={true}
                onCancel={handleCancel}
                width={820}
                footer={[
                    <Form.Item {...tailLayout}>
                        <Button form="createPrize" key="submit" htmlType="submit" type={'primary'}>
                            {t('create')}
                        </Button>
                    </Form.Item>
                ]}
            >
                <EditPrize dataProps={{}}
                           action={(prize) => props.createPrize({...prize}, () => {
                               fetchData()
                               handleOk()
                           })}
                           type={'createPrize'}/>
            </Modal>
            <Modal
                title="Update Prize"
                visible={dataEditing ? true : false}
                onOk={handleOk}
                destroyOnClose={true}
                onCancel={handleCancel}
                width={820}
                footer={[
                    <Form.Item {...tailLayout}>
                        <Button form="updatePrize" key="submit" htmlType="submit" type={'primary'}>
                            {t('update')}
                        </Button>
                    </Form.Item>
                ]}
            >
                <EditPrize dataProps={dataEditing}
                           action={(prize) => props.updatePrize({...prize}, () => {
                               fetchData()
                               handleOk()
                           })}
                           type={'updatePrize'}/>
            </Modal>
        </div>
    )
}

export default Prize;
