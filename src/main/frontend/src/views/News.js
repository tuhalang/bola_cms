import React, {useEffect} from "react";
import {useTranslation} from "react-i18next";
import {makeStyles} from "@material-ui/core/styles";
import styles from "../assets/jss/cms/views/prize";
import CardBody from "../components/Card/CardBody";
import Card from "../components/Card/Card";
import {Form, Input, Modal, Table} from "antd";
import {Button} from "@material-ui/core";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit} from "@fortawesome/free-solid-svg-icons";
import consts from "../consts";
import Loading from "../components/Loading";
import _ from "lodash";
import EditNews from "../components/Modal/EditNews";


const useStyles = makeStyles(styles);
const tailLayout = {
    wrapperCol: {offset: 8, span: 16},
};

function News(props) {
    const classes = useStyles();
    const {t} = useTranslation();
    const {news, totalElements, totalPages} = props;
    const [visible, setVisible] = React.useState(false);
    const [loading, setLoading] = React.useState(false);
    const [loadingSms, setLoadingSms] = React.useState(false);
    const [page, setPage] = React.useState(1);
    const [dataEditing, setDataEditing] = React.useState(null);
    const [searchText, setSearchText] = React.useState('');

    const handleOk = () => {
        setVisible(false);
        setDataEditing(null);
    };

    const handleCancel = () => {
        setVisible(false);
        setDataEditing(null);
    };

    const fetchData = (key = searchText, page = 1, size = consts.PAGE_SIZE) => {
        setLoading(true)
        props.getListNews({
            key,
            page,
            size,
            status: null,
        }, () => {
            setLoading(false)
        })
    }

    const columns = [
        {
            title: t('no'),
            dataIndex: 'no',
            width: '5%',
        },
        {
            title: t('contentEn'),
            dataIndex: 'contentEn',
            width: '20%',
        },
        {
            title: t('contentLc'),
            dataIndex: 'contentLc',
            width: '20%',
        },
        {
            title: t('newsType'),
            dataIndex: 'newsType',
            width: '7%',
            render: (__, {newsType}) => {
                return <span>{consts.NEWS_TYPE[newsType]}</span>
            }
        },
        {
            title: t('status'),
            dataIndex: 'status',
            width: '7%',
            render: (__, {status}) => {
                return <span>{_.find(consts.TYPE_STATUS_NEWS, e => e.value == status).label}</span>
            }
        },
        {
            title: t('publishTime'),
            dataIndex: 'publishTime',
            width: '11%',
        },
        {
            title: t('createTime'),
            dataIndex: 'createTime',
            width: '8%',
        },
        {
            title: t('updateTime'),
            dataIndex: 'updateTime',
            width: '8%',
        },
        {
            title: t('testMobilePhone'),
            dataIndex: 'testMobilePhone',
            width: '10%',
        },
        {
            title: t('action'),
            dataIndex: 'action',
            render: (_, record) => {
                return <a onClick={() => {
                    setLoadingSms(true)
                    props.testSmsNews({newsId: record.id}, (data) => {
                        fetchData()
                        setLoadingSms(false)
                    })
                }}>{t('test')}</a>
            }
        },
        {
            title: t('update'),
            dataIndex: 'update',
            render: (_, record) => {
                return <a onClick={() => {
                    setDataEditing(record)
                }}>{t('update')}</a>
            }
        },
    ];


    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div>
            {loadingSms ? <Loading/> : <div/>}
            <Card className={classes.searchHeader}>
                <Input
                    className={classes.searchInput}
                    allowClear={true}
                    onChange={e=>{
                        setSearchText(e.target.value)
                        fetchData(e.target.value)
                    }}
                    placeholder={'Search news...'}
                />
                <Button onClick={() => {
                    setVisible(true)
                }}>
                    <FontAwesomeIcon icon={faEdit}/>
                </Button>
            </Card>
            <Card>
                <CardBody>
                    <Table bordered
                           scroll={{
                               x: 'max-content',
                           }}
                           dataSource={_.map(news, (e, idx) => _.assign(e, {no: (page - 1) * consts.PAGE_SIZE + idx + 1})) || []}
                           columns={columns}
                           rowClassName="editable-row"
                           pagination={{
                               defaultPageSize: consts.PAGE_SIZE,
                               current: page,
                               showSizeChanger: false,
                               total: totalElements,
                               onChange: (p, pageSize) => {
                                   setPage(p)
                                   setLoading(true)
                                   fetchData(searchText, p, consts.PAGE_SIZE)
                               },
                               hideOnSinglePage: true,
                               responsive: true
                           }} bordered
                    />

                    {loading ? <Loading/> : <div/>}
                </CardBody>
            </Card>
            <Modal
                title="Edit News"
                visible={visible}
                onOk={handleOk}
                destroyOnClose={true}
                onCancel={handleCancel}
                width={820}
                footer={[
                    <Form.Item {...tailLayout}>
                        <Button form="createPrize" key="submit" htmlType="submit" type={'primary'}>
                            {t('create')}
                        </Button>
                    </Form.Item>
                ]}
            >
                <EditNews dataProps={{}}
                           action={(news) => props.createNews({...news}, () => {
                               console.log("OK")
                               fetchData()
                               handleOk()
                           })}
                           type={'createPrize'}/>
            </Modal>
            <Modal
                title="Update News"
                visible={dataEditing ? true : false}
                onOk={handleOk}
                destroyOnClose={true}
                onCancel={handleCancel}
                width={820}
                footer={[
                    <Form.Item {...tailLayout}>
                        <Button form="updateNews" key="submit" htmlType="submit" type={'primary'}>
                            {t('update')}
                        </Button>
                    </Form.Item>
                ]}
            >
                <EditNews dataProps={dataEditing}
                           action={(news) => props.updateNews({...news}, () => {
                               fetchData()
                               handleOk()
                           })}
                           type={'updateNews'}/>
            </Modal>
        </div>
    )
}

export default News;
