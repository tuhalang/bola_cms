import React, {useEffect} from "react";
import {useTranslation} from "react-i18next";
import {makeStyles} from "@material-ui/core/styles";
import styles from "../assets/jss/cms/views/history";
import CardBody from "../components/Card/CardBody";
import Card from "../components/Card/Card";
import {AutoComplete, Input, Table} from "antd";
import consts from "../consts";
import _ from "lodash";
import Loading from "../components/Loading";

const useStyles = makeStyles(styles)

function History(props) {
    const classes = useStyles();
    const {t} = useTranslation();
    const {histories, totalElements, totalPages} = props;
    const [loading, setLoading] = React.useState(false);
    const [page, setPage] = React.useState(1);
    const [searchText, setSearchText] = React.useState('');

    const fetchData = (isdn = searchText, page = 1, size = consts.PAGE_SIZE) => {
        setLoading(true)
        props.getListHistory({
            isdn,
            page,
            size,
        }, () => {
            setLoading(false)
        })
    }

    const columns = [
        {
            title: t('no'),
            dataIndex: 'no',
            width: '5%',
        },
        {
            title: t('isdn'),
            dataIndex: 'isdn',
            width: '7%',
        },
        {
            title: t('channel'),
            dataIndex: 'channel',
            width: '7%',
        },
        {
            title: t('predictionSession'),
            dataIndex: 'predictionSession',
            width: '10%',
        },
        {
            title: t('predictTime'),
            dataIndex: 'predictTime',
            width: '10%',
        },
        {
            title: t('predictValue'),
            dataIndex: 'predictValue',
            width: '10%',
        },
        {
            title: t('answer'),
            dataIndex: 'answer',
            width: '7%',
        },
        {
            title: t('status'),
            dataIndex: 'status',
            width: '5%',
            render: (__, {status}) => {
                return <span>{_.find(consts.TYPE_STATUS_HISTORY, e => e.value == status).label}</span>
            }
        },
        {
            title: t('type'),
            dataIndex: 'type',
            width: '5%',
            render: (__, {type}) => {
                return <span>{_.find(consts.TYPE_PLAY, e => e.value == type).label}</span>
            }
        },
    ];


    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div>
            <Card className={classes.searchHeader}>
                <Input
                    className={classes.searchInput}
                    allowClear={true}
                    onChange={e => {
                        setSearchText(e.target.value)
                        fetchData(e.target.value)
                    }}
                    placeholder={'Search ISDN...'}
                />
            </Card>
            <Card>
                <CardBody>
                    <Table bordered
                           scroll={{
                               x: 'max-content',
                           }}
                           dataSource={_.map(histories, (e, idx) => _.assign(e, {no: (page - 1) * consts.PAGE_SIZE + idx + 1})) || []}
                           columns={columns}
                           rowClassName="editable-row"
                           pagination={{
                               defaultPageSize: consts.PAGE_SIZE,
                               current: page,
                               showSizeChanger: false,
                               total: totalElements,
                               onChange: (p, pageSize) => {
                                   setPage(p)
                                   setLoading(true)
                                   fetchData(p, consts.PAGE_SIZE, 1)
                               },
                               hideOnSinglePage: true,
                               responsive: true
                           }} bordered
                    />

                    {loading ? <Loading/> : <div/>}
                </CardBody>
            </Card>
        </div>
    )
}

export default History;
