import React, {useEffect} from "react";
import {useTranslation} from "react-i18next";
import {makeStyles} from "@material-ui/core/styles";
import styles from "../assets/jss/cms/views/player";
import CardBody from "../components/Card/CardBody";
import Card from "../components/Card/Card";
import { Modal, Table, Form, Input, Divider, AutoComplete} from "antd";
import {Button} from "@material-ui/core";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit, faUpload} from "@fortawesome/free-solid-svg-icons";
import UploadFile from "../components/UploadFile";
import EditPlayer from "../components/Modal/EditPlayer";
import consts from "../consts";
import Loading from "../components/Loading";
import _ from "lodash";


const useStyles = makeStyles(styles);
const tailLayout = {
    wrapperCol: {offset: 8, span: 16},
};

function Player(props) {
    const classes = useStyles();
    const {t} = useTranslation();
    const {player, totalElements, totalPages} = props;
    const [visible, setVisible] = React.useState(false);
    const [uploading, setUploading] = React.useState(false);
    const [loading, setLoading] = React.useState(false);
    const [page, setPage] = React.useState(1);
    const [dataEditing, setDataEditing] = React.useState(null);
    const [searchText, setSearchText] = React.useState('');
    const [options, setOptions] = React.useState([]);
    const [teamId, setTeamId] = React.useState([]);


    const handleOk = () => {
        setVisible(false);
        setDataEditing(null);
    };

    const handleCancel = () => {
        setVisible(false);
        setDataEditing(null);
    };

    const fetchData = (team=null, key = searchText, page = 1, size = consts.PAGE_SIZE) => {
        setLoading(true)
        props.getListPlayer({
            team,
            key,
            page,
            size,
            status: 1,
        }, () => {
            setLoading(false)
        })
    }

    React.useEffect(() => {
        if (props.teams && props.teams.length > 15) {
            setOptions(_.map(_.unionBy(props.teams, e => e.id), e => {
                return {
                    value: e.code + ' - ' + e.nameEn
                }
            }))
        } else {
            props.getListTeam({key: '', page: 1, size: 100, status: null})
        }
    }, [props.teams])

    const onSelect = (data) => {
        let teamId = _.find(props.teams, e => ((e.code + ' - ' + e.nameEn) == data)).id
        setTeamId(teamId)
        fetchData(teamId, searchText, 1, consts.PAGE_SIZE)
    };

    const columns = [
        {
            title: t('no'),
            dataIndex: 'no',
            width: '5%',
        },
        {
            title: t('nameEn'),
            dataIndex: 'nameEn',
            width: '8%',
        },
        {
            title: t('nameLc'),
            dataIndex: 'nameLc',
            width: '8%',
        },
        {
            title: t('descLc'),
            dataIndex: 'descLc',
            width: '8%',
        },
        {
            title: t('descEn'),
            dataIndex: 'descEn',
            width: '8%',
            render: text => <span style={{wordWrap: 'break-word'}}>{text}</span>
        },
        {
            title: t('team'),
            dataIndex: 'team',
            width: '5%',
        },
        {
            title: t('imageUrl'),
            dataIndex: 'imageUrl',
            width: '9%',
        },
        {
            title: t('number'),
            dataIndex: 'number',
            width: '5%',
        },
        {
            title: t('position'),
            dataIndex: 'position',
            width: '5%',
        },
        {
            title: t('update'),
            dataIndex: 'update',
            render: (_, record) => {
                return <a onClick={() => {
                    setDataEditing(record)
                }}>{t('update')}</a>
            }
        },
    ];

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div>
            <Card className={classes.searchHeader}>
                <AutoComplete
                    className={classes.searchInput}
                    allowClear={true}
                    options={options}
                    onSelect={onSelect}
                    onSearch={() => {
                    }}
                    onChange={() => {
                        setTeamId(null)
                        fetchData(null, searchText, 1, consts.PAGE_SIZE)
                    }}
                    placeholder="Please select team"
                />
                <Divider type="vertical" />
                <Divider type="vertical" />
                <Divider type="vertical" />
                <Input
                    className={classes.searchInput}
                    onChange={(text) => {
                        setSearchText(text.target.value)
                        fetchData(teamId, text.target.value, 1, consts.PAGE_SIZE)
                    }}
                    allowClear={true}
                    value={searchText}
                    placeholder={'Search player...'}
                />
                <Button onClick={() => {
                    setVisible(true)
                }}>
                    <FontAwesomeIcon icon={faEdit}/>
                </Button>
                <Button onClick={() => setUploading(true)}>
                    <FontAwesomeIcon icon={faUpload}/>
                </Button>
            </Card>
            <Card>
                <CardBody>
                    <Table bordered
                           scroll={{
                               x: 'max-content',
                           }}
                           dataSource={_.map(player, (e, idx) => _.assign(e, {no: (page - 1) * consts.PAGE_SIZE + idx + 1})) || []}
                           columns={columns}
                           rowClassName="editable-row"
                           pagination={{
                               defaultPageSize: consts.PAGE_SIZE,
                               current: page,
                               showSizeChanger: false,
                               total: totalElements,
                               onChange: (p, pageSize) => {
                                   setPage(p)
                                   setLoading(true)
                                   fetchData(teamId, searchText, p, consts.PAGE_SIZE)
                               },
                               hideOnSinglePage: true,
                               responsive: true
                           }} bordered
                    />
                    {loading ? <Loading/> : <div/>}
                </CardBody>
            </Card>
            <Modal
                title="Edit Player"
                visible={visible}
                destroyOnClose={true}
                onOk={handleOk}
                onCancel={handleCancel}
                width={820}
                footer={[
                    <Form.Item {...tailLayout}>
                        <Button form="createPlayer" key="submit" htmlType="submit" type={'primary'}>
                            {t('create')}
                        </Button>
                    </Form.Item>
                ]}
            >
                <EditPlayer dataProps={{}}
                            action={(player) => props.createPlayer({...player}, () => {
                                fetchData()
                                handleOk()
                            })}
                            {...props}
                            type={'createPlayer'}/>
            </Modal>

            <Modal
                centered
                visible={uploading}
                destroyOnClose={true}
                onOk={() => setUploading(false)}
                onCancel={() => setUploading(false)}
                footer={[]}>
                <UploadFile type={uploading} callback={async (type, file, filePath) => {
                    // files[type] = await utils.getBase64(file)
                    // product[type] = await filePath
                    // setProduct({
                    //     ...product
                    // })
                    // setUploading('')
                }} ext={['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']}/>
            </Modal>
            <Modal
                title="Update Player"
                visible={dataEditing ? true : false}
                onOk={handleOk}
                destroyOnClose={true}
                onCancel={handleCancel}
                width={820}
                footer={[
                    <Form.Item {...tailLayout}>
                        <Button form="updatePlayer" key="submit" htmlType="submit" type={'primary'}>
                            {t('update')}
                        </Button>
                    </Form.Item>
                ]}
            >
                <EditPlayer dataProps={dataEditing}
                            action={(player) => props.updatePlayer({...player}, () => {
                                fetchData()
                                handleOk()
                            })}
                            {...props}
                            type={'updatePlayer'}/>
            </Modal>
        </div>
    )
}

export default Player;
