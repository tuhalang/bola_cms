import React, {useEffect} from "react";
import CardBody from "../components/Card/CardBody";
import Card from "../components/Card/Card";
import {useTranslation} from "react-i18next";
import {makeStyles} from "@material-ui/core/styles";
import styles from "../assets/jss/cms/views/session";
import {Form, Input, Modal, Table} from "antd";
import {Button} from "@material-ui/core";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit, faUpload} from "@fortawesome/free-solid-svg-icons";
import consts from "../consts";
import _ from "lodash";
import Loading from "../components/Loading";
import EditSession from "../components/Modal/EditSession";

const useStyles = makeStyles(styles);
const tailLayout = {
    wrapperCol: {offset: 8, span: 16},
};

function Session(props) {
    const classes = useStyles();
    const {t} = useTranslation();
    const {session, totalElements, totalPages} = props;
    const [visible, setVisible] = React.useState(false);
    const [loading, setLoading] = React.useState(false);
    const [page, setPage] = React.useState(1);
    const [dataEditing, setDataEditing] = React.useState(null);
    const [searchText, setSearchText] = React.useState('');


    const handleOk = () => {
        setDataEditing(null);
        setVisible(false);
    };

    const handleCancel = () => {
        setDataEditing(null);
        setVisible(false);
    };

    const fetchData = (key = searchText, page = 1, size = consts.PAGE_SIZE) => {
        setLoading(true)
        props.getListSession({
            key,
            page,
            size,
            type: null,
        }, () => {
            setLoading(false)
        })
    }

    const columns = [
        {
            title: t('no'),
            dataIndex: 'no',
            width: '5%',
        },
        {
            title: t('nameEn'),
            dataIndex: 'nameEn',
            width: '5%',
        },
        {
            title: t('nameLc'),
            dataIndex: 'nameLc',
            width: '5%',
        },
        {
            title: t('match'),
            dataIndex: 'matchName',
            width: '10%',
        },
        {
            title: t('type'),
            dataIndex: 'type',
            width: '5%',
            render: (__, {type}) => {
                return <span>{_.find(consts.TYPE_PLAY, e => e.value == type).label}</span>
            }
        },
        {
            title: t('result'),
            dataIndex: 'result',
            width: '5%',
        },
        {
            title: t('package'),
            dataIndex: 'winnerPackageName',
            width: '5%',
        },
        {
            title: t('startTime'),
            dataIndex: 'startTime',
            width: '8%',
        },
        {
            title: t('endTime'),
            dataIndex: 'endTime',
            width: '8%',
        },
        {
            title: t('questionEn'),
            dataIndex: 'questionEn',
            width: '5%',
        },
        {
            title: t('questionLc'),
            dataIndex: 'questionLc',
            width: '5%',
        },
        {
            title: t('descLc'),
            dataIndex: 'descLc',
            width: '5%',
        },
        {
            title: t('descEn'),
            dataIndex: 'descEn',
            width: '5%',
            render: text => <span style={{wordWrap: 'break-word'}}>{text}</span>
        },
        {
            title: t('status'),
            dataIndex: 'status',
            width: '5%',
            render: (__, {status}) => {
                return <span>{_.find(consts.TYPE_STATUS_MATCH, e => e.value == status).label}</span>
            }
        },
        {
            title: t('update'),
            dataIndex: 'update',
            render: (_, record) => {
                return <a onClick={() => {
                    setDataEditing(record)
                }}>{t('update')}</a>
            }
        },
    ];


    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div>
            <Card className={classes.searchHeader}>
                <Input
                    className={classes.searchInput}
                    allowClear={true}
                    onChange={e => {
                        setSearchText(e.target.value)
                        fetchData(e.target.value)
                    }}
                    placeholder={'Search session...'}
                />
                <Button onClick={() => {
                    setVisible(true)
                }}>
                    <FontAwesomeIcon icon={faEdit}/>
                </Button>
            </Card>
            <Card>
                <CardBody>
                    <Table bordered
                           scroll={{
                               x: 'max-content',
                           }}
                           dataSource={_.map(session, (e, idx) => _.assign(e, {no: (page - 1) * consts.PAGE_SIZE + idx + 1})) || []}
                           columns={columns}
                           rowClassName="editable-row"
                           pagination={{
                               defaultPageSize: consts.PAGE_SIZE,
                               current: page,
                               showSizeChanger: false,
                               total: totalElements,
                               onChange: (p, pageSize) => {
                                   setPage(p)
                                   setLoading(true)
                                   fetchData(searchText, p, consts.PAGE_SIZE)
                               },
                               hideOnSinglePage: true,
                               responsive: true
                           }} bordered
                    />

                    {loading ? <Loading/> : <div/>}
                </CardBody>
            </Card>
            <Modal
                title="Edit Session"
                visible={visible}
                onOk={handleOk}
                destroyOnClose={true}
                onCancel={handleCancel}
                width={820}
                footer={[
                    <Form.Item {...tailLayout}>
                        <Button form="createSession" key="submit" htmlType="submit" type={'primary'}>
                            {t('create')}
                        </Button>
                    </Form.Item>
                ]}
            >
                <EditSession dataProps={{}}
                             action={(session) => props.createSession({...session}, () => {
                                 fetchData()
                                 handleOk()
                             })}
                             {...props}
                             type={'createSession'}/>
            </Modal>
            <Modal
                title="Update Session"
                visible={dataEditing ? true : false}
                onOk={handleOk}
                destroyOnClose={true}
                onCancel={handleCancel}
                width={820}
                footer={[
                    <Form.Item {...tailLayout}>
                        <Button form="updateSession" key="submit" htmlType="submit" type={'primary'}>
                            {t('update')}
                        </Button>
                    </Form.Item>
                ]}
            >
                <EditSession dataProps={dataEditing}
                             action={(session) => props.updateSession({...session}, () => {
                                 fetchData()
                                 handleOk()
                             })}
                             {...props}
                             type={'updateSession'}/>
            </Modal>
        </div>
    )
}

export default Session;
