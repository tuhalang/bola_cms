import React, {useEffect, useState} from 'react';
import CardFooter from "../components/Card/CardFooter";
import Button from "@material-ui/core/Button";
import GridContainer from "../components/Grid/GridContainer";
import GridItem from "../components/Grid/GridItem";
import Card from "../components/Card/Card";
import CardHeader from "../components/Card/CardHeader";
import CardBody from "../components/Card/CardBody";
import CustomInput from "../components/CustomInput/CustomInput";
import {InputAdornment} from "@material-ui/core";
import {AccountCircle} from "@material-ui/icons";
import makeStyles from "@material-ui/core/styles/makeStyles";
import styles from 'assets/jss/cms/views/loginStyles.js';
import Icon from "@material-ui/core/Icon";

const useStyles = makeStyles(styles);

function Login(props) {
    const classes = useStyles();
    const [showPwd, setShowPwd] = useState(false)

    const onSubmit = async event => {
        event.preventDefault();
        const {email, password} = event.target
        loginNormal(email.value, password.value)
    }

    const loginNormal = (email, password) => {
        props.onSignInAction({
            username: email,
            password
        }, () => {
            props.history.push('/')
        })
    }

    useEffect(() => {
        props.checkSignIn()
    }, [])

    return (
        <div className={classes.section}>
            <div className={classes.container}>
                <GridContainer justify="center">
                    <GridItem xs={12} sm={6} md={4}>
                        <Card>
                            <form className={classes.form} onSubmit={onSubmit}>
                                <CardHeader color="primary" className={classes.cardHeader}>
                                    <h4 className={classes.loginTitle}>Login</h4>
                                </CardHeader>
                                <CardBody>
                                    <CustomInput
                                        labelText="Username..."
                                        id="email"
                                        name={"email"}
                                        formControlProps={{
                                            fullWidth: true
                                        }}
                                        inputProps={{
                                            type: "text",
                                            endAdornment: (
                                                <InputAdornment position="end">
                                                    <AccountCircle className={classes.inputIconsColor}/>
                                                </InputAdornment>
                                            )
                                        }}
                                    />
                                    <CustomInput
                                        labelText="Password"
                                        id="pass"
                                        name={'password'}
                                        formControlProps={{
                                            fullWidth: true
                                        }}
                                        inputProps={{
                                            type: showPwd ? "password" : 'text',
                                            endAdornment: (
                                                <InputAdornment position="end">
                                                    <Icon className={classes.inputIconsColor}>
                                                        lock_outline
                                                    </Icon>
                                                </InputAdornment>
                                            ),
                                            autoComplete: "off",
                                            onClick: () => setShowPwd(!showPwd),
                                            onFocus: () => showPwd
                                        }}
                                    />
                                </CardBody>
                                <CardFooter className={classes.cardFooter}>
                                    <Button simple color="primary" size="lg" type={"submit"}>
                                        SignIn
                                    </Button>
                                </CardFooter>
                            </form>
                        </Card>
                    </GridItem>
                </GridContainer>
            </div>
        </div>
    )
}

export default Login;
