export const BASE_URL = 'http://localhost:8888/';
export const DNS_IMAGE = '/EURO_CMS/files/'
export const CONTEXT_PATH = '/api';
export const TIME_OF_DEBOUNCE = 300;

const CONTROLS = {
    GET: 'GET',
    POST: 'POST',
    PUT: 'PUT',
    DELETE: 'DELETE',
};

export default {
    PAGE_SIZE: 10,
    EN: 'en',
    VI: 'vi',
    TYPE_PRIZE: [
        {
            value: 1,
            label: 'Artifacts'
        },
        {
            value: 2,
            label: "Money"
        },
        {
            value: 3,
            label: "Data"
        }
    ],
    TYPE_PLAY: [
        {
            value: 1,
            label: "Match Prediction"
        },
        {
            value: 2,
            label: "The Champion Prediction"
        },
        {
            value: 3,
            label: "The Top Scorer Prediction"
        }
    ],
    TYPE_STATE: [
        {
            value: 1,
            label: "Group stage"
        },
        {
            value: 2,
            label: "Round of 16"
        },
        {
            value: 3,
            label: "Quarterfinals"
        },
        {
            value: 4,
            label: "Semifinal"
        },
        {
            value: 5,
            label: "Final"
        }
    ],
    TYPE_STATUS_TEAM: [
        {
            value: 1,
            label: "Playing"
        },
        {
            value: 2,
            label: "Disqualified"
        }
    ],
    TYPE_STATUS_MATCH: [
        {
            value: 0,
            label: "Comming Soon"
        },
        {
            value: 1,
            label: "Is Comming"
        },
        {
            value: 2,
            label: "Expired"
        }
    ],
    TYPE_STATUS_RESULT: [
        {
            value:1,
            label: "Paid"
        },
        {
            value:2,
            label: "Unpaid"
        }
    ],
    TYPE_STATUS_HISTORY: [
        {
            value:0,
            label: "Disable"
        },
        {
            value:1,
            label: "Enable"
        }
    ],
    NEWS_TYPE: {
        "1": "Hot News",
        "2": "Schedule",
        "3": "Result"
    },
    TYPE_STATUS_NEWS: [
        {
            value:3,
            label: "NEW"
        },
        {
            value:2,
            label: "TESTED"
        },
        {
            value:1,
            label: "APPROVE"
        }
    ],
};
