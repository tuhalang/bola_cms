import React from "react";
import ReactDOM from "react-dom";
import {applyMiddleware, createStore, compose} from 'redux';
import createSagaMiddleware from 'redux-saga';
import {Provider} from 'react-redux';
import "assets/css/material-dashboard-react.css?v=1.9.0";
import 'antd/dist/antd.css';
import allReducers from './redux/reducers';
import rootSaga from "./redux/sagas/root_saga";
import App from "./App";
import axios from "axios";
import {BASE_URL} from "./consts";
//multi lang
import './i18n';

window.axios = axios.create({
    baseURL: BASE_URL,
});

const sagaMiddleware = createSagaMiddleware();
const composeEnhancers = typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
    : compose;

const store = createStore(
    allReducers,
    composeEnhancers(applyMiddleware(sagaMiddleware)),
);
sagaMiddleware.run(rootSaga);

window.$dispatch = store.dispatch;
store.dispatch({
    type: '@@__INIT__',
});

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById("root")
);
