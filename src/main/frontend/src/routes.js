import React from "react";
import {
    faAward,
    faList,
    faUsers,
    faUserCog,
    faBox,
    faClock,
    faHistory, faCheckCircle
} from "@fortawesome/free-solid-svg-icons";
import History from "./containers/HistoryContainer";
import Match from "./containers/MatchContainer";
import Player from "./containers/PlayerContainer";
import Prize from "./containers/PrizeContainer";
import Package from "./containers/PackageContainer";
import PrizePackage from "./containers/PrizePackageContainer";
import Result from "./containers/ResultContainer";
import Session from "./containers/SessionContainer";
import Team from './containers/TeamContainer';
import News from './containers/NewsContainer';

const routes = [
    {
        path: "/team",
        name: "Manage Team",
        rtlName: "Manage Team",
        icon: faUsers,
        typeIcon: 'fontawesome',
        component: Team,
        layout: "/BOLA_CMS"
    },
    {
        path: "/player",
        name: "Manage Player",
        rtlName: "Manage Player",
        icon: faUserCog,
        typeIcon: 'fontawesome',
        component: Player,
        layout: "/BOLA_CMS"
    },
    {
        path: "/prize",
        name: "Manage Prize",
        rtlName: "Manage Prize",
        icon: faAward,
        typeIcon: 'fontawesome',
        component: Prize,
        layout: "/BOLA_CMS"
    },
    {
        path: "/package",
        name: "Manage Package",
        rtlName: "Manage Package",
        icon: faAward,
        typeIcon: 'fontawesome',
        component: Package,
        layout: "/BOLA_CMS"
    },
    {
        path: "/prize-package",
        name: "Manage Prize Package",
        rtlName: "Manage Prize Package",
        icon: faBox,
        typeIcon: 'fontawesome',
        component: PrizePackage,
        layout: "/BOLA_CMS"
    },
    {
        path: "/match",
        name: "Manage Match",
        rtlName: "Manage Match",
        icon: faClock,
        typeIcon: 'fontawesome',
        component: Match,
        layout: "/BOLA_CMS"
    },
    {
        path: "/session",
        name: "Manage Session",
        rtlName: "Manage Session",
        icon: faList,
        typeIcon: 'fontawesome',
        component: Session,
        layout: "/BOLA_CMS"
    },
    {
        path: "/result",
        name: "Manage Result",
        rtlName: "Manage Result",
        icon: faCheckCircle,
        typeIcon: 'fontawesome',
        component: Result,
        layout: "/BOLA_CMS"
    },
    {
        path: "/history",
        name: "Manage History",
        rtlName: "Manage History",
        icon: faHistory,
        typeIcon: 'fontawesome',
        component: History,
        layout: "/BOLA_CMS"
    },
    {
        path: "/news",
        name: "Manage News",
        rtlName: "Manage News",
        icon: faHistory,
        typeIcon: 'fontawesome',
        component: News,
        layout: "/BOLA_CMS"
    },
];

export default routes;
