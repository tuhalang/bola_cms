package com.viettel.euro_cms.repo;

import com.viettel.euro_cms.domain.model.Team;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TeamRepo extends JpaRepository<Team, String> {

    @Query(value = "select CAST(sys_guid() as varchar2(255)) from dual", nativeQuery = true)
    String getID();

    @Query(
            value = "select t from Team t where t.status = :status " +
                    "and lower(t.nameEn) like lower(concat('%', :key, '%')) order by t.group"
    )
    Page<Team> find(Pageable pageable, Integer status, String key);

    @Query(
            value = "select t from Team t where " +
                    "lower(t.nameEn) like lower(concat('%', :key, '%')) order by t.group"
    )
    Page<Team> find(Pageable pageable, String key);

    Optional<Team> findByCode(String code);
}
