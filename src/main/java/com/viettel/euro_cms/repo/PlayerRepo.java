package com.viettel.euro_cms.repo;

import com.viettel.euro_cms.domain.dto.request.PlayerDto;
import com.viettel.euro_cms.domain.model.Player;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerRepo extends JpaRepository<Player, String> {

    @Query(value = "select CAST(sys_guid() as varchar2(255)) from dual", nativeQuery = true)
    String getID();

    @Query(
            value = "select t from Player t where " +
                    "lower(t.nameEn) like lower(concat('%', :key, '%')) order by t.goals desc, t.number"
    )
    Page<Player> find(Pageable pageable, String key);


    @Query(
            value = "select new com.viettel.euro_cms.domain.dto.request.PlayerDto(" +
                    "p.id,  p.nameEn, p.nameLc, t.code, p.goals, p.number, p.position," +
                    " p.imageUrl, p.descEn, p.nameLc) from Player p left join Team t on p.team = t.id where " +
                    "lower(p.nameEn) like lower(concat('%', :key, '%')) order by p.goals desc, p.number"
    )
    Page<PlayerDto> findDto(Pageable pageable, String key);

    @Query(
            value = "select new com.viettel.euro_cms.domain.dto.request.PlayerDto(" +
                    "p.id,  p.nameEn, p.nameLc, t.code, p.goals, p.number, p.position," +
                    " p.imageUrl, p.descEn, p.nameLc) from Player p left join Team t on p.team = t.id where " +
                    "t.id = :team and lower(p.nameEn) like lower(concat('%', :key, '%')) order by p.goals desc, p.number"
    )
    Page<PlayerDto> findDto(Pageable pageable, String key, String team);
}
