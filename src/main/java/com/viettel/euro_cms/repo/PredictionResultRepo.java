package com.viettel.euro_cms.repo;

import com.viettel.euro_cms.domain.model.PredictionResult;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PredictionResultRepo extends JpaRepository<PredictionResult, String> {

    @Query(value = "select CAST(sys_guid() as varchar2(255)) from dual", nativeQuery = true)
    String getID();

    @Query(
            value = "select new com.viettel.euro_cms.domain.model.PredictionResult(" +
                    "pr.id, pr.isdn, pr.predictionSession, pr.predictionTurn, p.nameEn, pr.rank, pr.status) " +
                    "from PredictionResult pr, Prize p where pr.prize = p.id and pr.predictionSession = :predictionSession order by pr.rank"
    )
    Page<PredictionResult> find(Pageable pageable, String predictionSession);
}
