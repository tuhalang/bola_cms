package com.viettel.euro_cms.repo;

import com.viettel.euro_cms.domain.dto.request.MatchDto;
import com.viettel.euro_cms.domain.model.Match;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface MatchRepo extends JpaRepository<Match, String> {

    @Query(value = "select CAST(sys_guid() as varchar2(255)) from dual", nativeQuery = true)
    String getID();


    @Query(
            value = "select new com.viettel.euro_cms.domain.dto.request.MatchDto(m.id, t1.code, t2.code," +
                    "m.firstTeamGoals, m.secondTeamGoals, m.startTime, m.endTime, m.placeEn, m.placeLc, " +
                    "m.descEn, m.placeLc, m.status, concat(t1.code, ' - ', t2.code, ' : ', m.startTime), m.state) " +
                    "from Match m left join Team t1 on m.firstTeam = t1.id left join Team t2 on m.secondTeam = t2.id where" +
                    " m.status = :status and lower(m.state) like lower(concat(:stateMatch, '%')) " +
                    " order by m.startTime"
    )
    Page<MatchDto> find(Pageable pageable, Integer status, String stateMatch);

    @Query(
            value = "select new com.viettel.euro_cms.domain.dto.request.MatchDto(m.id, t1.code, t2.code," +
                    "m.firstTeamGoals, m.secondTeamGoals, m.startTime, m.endTime, m.placeEn, m.placeLc, " +
                    "m.descEn, m.placeLc, m.status, concat(t1.code, ' - ', t2.code, ' : ', m.startTime), m.state) " +
                    "from Match m left join Team t1 on m.firstTeam = t1.id left join Team t2 on m.secondTeam = t2.id where" +
                    " lower(m.state) like lower(concat(:stateMatch, '%')) " +
                    " order by m.startTime"
    )
    Page<MatchDto> find(Pageable pageable, String stateMatch);
}
