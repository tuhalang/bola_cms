package com.viettel.euro_cms.repo;

import com.viettel.euro_cms.domain.model.Prize;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PrizeRepo extends JpaRepository<Prize, String> {

    @Query(value = "select CAST(sys_guid() as varchar2(255)) from dual", nativeQuery = true)
    String getID();

    @Query(
            value = "select t from Prize t where t.status = :status " +
                    "and lower(t.nameEn) like lower(concat('%', :key, '%')) order by t.value desc "
    )
    Page<Prize> find(Pageable pageable, Integer status, String key);

    @Query(
            value = "select t from Prize t where " +
                    "lower(t.nameEn) like lower(concat('%', :key, '%')) order by t.value desc "
    )
    Page<Prize> find(Pageable pageable, String key);
}
