package com.viettel.euro_cms.repo;

import com.viettel.euro_cms.domain.dto.request.PredictionSessionDto;
import com.viettel.euro_cms.domain.model.PredictionSession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface PredictionSessionRepo extends JpaRepository<PredictionSession, String> {

    @Query(value = "select CAST(sys_guid() as varchar2(255)) from dual", nativeQuery = true)
    String getID();


    @Query(
            value = "select new com.viettel.euro_cms.domain.dto.request.PredictionSessionDto(p.id," +
                    "p.nameEn, p.nameLc, p.type, m.id, w.id," +
                    "p.startTime, p.endTime, p.descEn, p.descLc, p.questionEn, p.questionLc, p.status, concat(t1.code, ' - ', t2.code, ' : ', m.startTime), w.nameEn, p.result) " +
                    "from PredictionSession p left join WinnerPackage w on p.winnerPackage = w.id, Match m, Team t1, Team t2 " +
                    "where p.match = m.id and m.firstTeam = t1.id and m.secondTeam = t2.id and lower(p.nameEn) like lower(concat('%', :key, '%')) order by p.startTime desc"
    )
    Page<PredictionSessionDto> find(Pageable pageable, String key);

    @Query(
            value = "select new com.viettel.euro_cms.domain.dto.request.PredictionSessionDto(p.id," +
                    "p.nameEn, p.nameLc, p.type, m.id, w.id," +
                    "p.startTime, p.endTime, p.descEn, p.descLc, p.questionEn, p.questionLc, p.status, concat(t1.code, ' - ', t2.code, ' : ', m.startTime), w.nameEn, p.result) " +
                    "from PredictionSession p left join WinnerPackage w on p.winnerPackage = w.id , Match m, Team t1, Team t2 " +
                    "where p.match = m.id and p.type = :type and m.firstTeam = t1.id and m.secondTeam = t2.id  and " +
                    "lower(p.nameEn) like lower(concat('%', :key, '%')) order by p.startTime desc"
    )
    Page<PredictionSessionDto> find(Pageable pageable, Integer type, String key);

    @Query(
            value = "select new com.viettel.euro_cms.domain.dto.request.PredictionSessionDto(p.id," +
                    "p.nameEn, p.nameLc, p.type, '', w.id, p.startTime, p.endTime, p.descEn, p.descLc, p.questionEn, " +
                    "p.questionLc, p.status, '', w.nameEn, p.result) from PredictionSession p left join WinnerPackage w on p.winnerPackage = w.id " +
                    "where p.type in (2, 3) and lower(p.nameEn) like lower(concat('%', :key, '%')) order by p.startTime desc"
    )
    List<PredictionSessionDto> find(String key);

    @Query(
            value = "select ps from PredictionSession ps where ps.match = :matchId"
    )
    Optional<PredictionSession> findByMatch(String matchId);
}
