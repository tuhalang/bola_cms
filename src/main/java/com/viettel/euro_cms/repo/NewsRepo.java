package com.viettel.euro_cms.repo;

import com.viettel.euro_cms.domain.model.News;
import com.viettel.euro_cms.domain.model.Prize;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface NewsRepo extends JpaRepository<News, String> {

    @Query(value = "select CAST(sys_guid() as varchar2(255)) from dual", nativeQuery = true)
    String getID();

    @Query(
            value = "select t from News t where t.status = :status " +
                    "and lower(t.contentEn) like lower(concat('%', :key, '%')) order by t.createTime desc "
    )
    Page<News> find(Pageable pageable, Integer status, String key);

    @Query(
            value = "select t from News t where " +
                    "lower(t.contentEn) like lower(concat('%', :key, '%')) order by t.createTime desc "
    )
    Page<News> find(Pageable pageable, String key);
}
