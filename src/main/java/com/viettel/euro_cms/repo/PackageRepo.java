package com.viettel.euro_cms.repo;

import com.viettel.euro_cms.domain.model.WinnerPackage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PackageRepo extends JpaRepository<WinnerPackage, String> {

    @Query(value = "select CAST(sys_guid() as varchar2(255)) from dual", nativeQuery = true)
    String getID();

    @Query(
            value = "select w from WinnerPackage w where lower(w.nameEn) like lower(concat('%', :key, '%'))"
    )
    Page<WinnerPackage> find(Pageable pageable, String key);


    @Query(
            value = "select w from WinnerPackage w where w.status = :status and lower(w.nameEn) like lower(concat('%', :key, '%'))"
    )
    Page<WinnerPackage> find(Pageable pageable, Integer status, String key);

}
