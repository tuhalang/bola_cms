package com.viettel.euro_cms.repo;

import com.viettel.euro_cms.domain.dto.request.PrizePackageDto;
import com.viettel.euro_cms.domain.model.PrizePackage;
import com.viettel.euro_cms.domain.model.PrizePackageId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PrizePackageRepo extends JpaRepository<PrizePackage, PrizePackageId> {

    @Query(value = "select CAST(sys_guid() as varchar2(255)) from dual", nativeQuery = true)
    String getID();

    @Query(
            value = "select new com.viettel.euro_cms.domain.dto.request.PrizePackageDto(" +
                    "p.id.prizeId, p.id.packageId, p.amount, p.rank, p.status) " +
                    "from PrizePackage p where p.id.packageId = :packageId and p.status = :status"
    )
    Page<PrizePackageDto> find(Pageable pageable, String packageId, Integer status);

    @Query(
            value = "select new com.viettel.euro_cms.domain.dto.request.PrizePackageDto(" +
                    "p.id.prizeId, p.id.packageId, p.amount, p.rank, p.status) " +
                    "from PrizePackage p where p.id.packageId = :packageId"
    )
    Page<PrizePackageDto> find(Pageable pageable, String packageId);
}
