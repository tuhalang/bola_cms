package com.viettel.euro_cms.repo;

import com.viettel.euro_cms.domain.dto.request.PredictionTurnDto;
import com.viettel.euro_cms.domain.model.PredictionTurn;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PredictionTurnRepo extends JpaRepository<PredictionTurn, String> {

    @Query(value = "select CAST(sys_guid() as varchar2(255)) from dual", nativeQuery = true)
    String getID();

    @Query(
            value = "select new com.viettel.euro_cms.domain.dto.request.PredictionTurnDto(" +
                    "pt.id, pt.isdn, ps.nameEn, pt.type, pt.predictValue, pt.answer, pt.predictTime," +
                    "pt.status, pt.channel, pt.updateTime) from PredictionTurn pt, PredictionSession  ps " +
                    "where pt.predictionSession = ps.id and pt.isdn like concat(:isdn,'%') order by pt.updateTime desc"
    )
    Page<PredictionTurnDto> find(Pageable pageable, String isdn);
}
