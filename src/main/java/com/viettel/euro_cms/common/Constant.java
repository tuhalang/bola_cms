package com.viettel.euro_cms.common;

public class Constant {
    public static final String LANGUAGE_EN = "en";
    public static final String LANGUAGE_LC = "lc";
    public static final String ERROR_CODE_OK = "0";
    public static final String ERROR_CODE_NOK = "1";
    public static final String MSG_SIGN_IN_OK = "Login successfully !";
    public static final String MSG_SIGN_IN_NOK = "Username or password incorrect!";
    public static final String MSG_USER_NOT_ACTIVE = "Account is inactive !";
    public static final String MSG_USER_EXIST = "Username is exists !";
    public static final String MSG_SERVER_ERROR = "Server error !";
    public static final String MSG_SUCCESS = "Successfully !";
    public static final String MSG_NOT_EXISTS = "The record(s) is not exists !";
    public static final String MSG_RULE_CODE_IS_EXISTS = "RuleCode is exists !";
}
