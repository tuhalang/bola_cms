package com.viettel.euro_cms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EuroCmsApplication {

    public static void main(String[] args) {
        SpringApplication.run(EuroCmsApplication.class, args);
    }

}
