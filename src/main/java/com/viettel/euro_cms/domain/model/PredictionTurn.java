package com.viettel.euro_cms.domain.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "PREDICTION_TURN")
public class PredictionTurn {

    @Id
    @Column(name = "ID")
    private String id;
    @Column(name = "ISDN")
    private String isdn;
    @Column(name = "PREDICTION_SESSION")
    private String predictionSession;
    @Column(name = "PREDICT_TYPE")
    private Integer type;
    @Column(name = "PREDICT_VALUE")
    private String predictValue;
    @Column(name = "ANSWER")
    private Integer answer;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @Column(name = "PREDICT_TIME")
    @Temporal(TemporalType.DATE)
    private Date predictTime;
    @Column(name = "STATUS")
    private Integer status;
    @Column(name = "CHANNEL")
    private String channel;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @Column(name = "UPDATE_TIME")
    @Temporal(TemporalType.DATE)
    private Date updateTime;
}
