package com.viettel.euro_cms.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "WINNER_PACKAGE")
public class WinnerPackage {

    @Id
    @Column(name = "ID")
    private String id;
    @Column(name = "NAME_EN")
    private String nameEn;
    @Column(name = "NAME_LC")
    private String nameLc;
    @Column(name = "TOTAL_PRIZE")
    private Integer totalPrize;
    @Column(name = "DESC_EN")
    private String descEn;
    @Column(name = "DESC_LC")
    private String descLc;
    @Column(name = "STATUS")
    private Integer status;

}
