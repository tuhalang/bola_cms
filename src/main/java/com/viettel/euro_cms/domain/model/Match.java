package com.viettel.euro_cms.domain.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "MATCH")
public class Match {

    @Id
    @Column(name = "ID")
    private String id;
    @Column(name = "FIRST_TEAM")
    private String firstTeam;
    @Column(name = "SECOND_TEAM")
    private String secondTeam;
    @Column(name = "FIRST_TEAM_GOALS")
    private Integer firstTeamGoals;
    @Column(name = "SECOND_TEAM_GOALS")
    private Integer secondTeamGoals;
    @Column(name = "START_TIME")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.DATE)
    private Date startTime;
    @Column(name = "END_TIME")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.DATE)
    private Date endTime;
    @Column(name = "PLACE_EN")
    private String placeEn;
    @Column(name = "PLACE_LC")
    private String placeLc;
    @Column(name = "DESC_EN")
    private String descEn;
    @Column(name = "DESC_LC")
    private String descLc;
    @Column(name = "STATUS")
    private Integer status;
    @Column(name = "STATE")
    private String state;
}
