package com.viettel.euro_cms.domain.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "NEWS")
public class News {

    @Id
    @Column(name = "ID")
    private String id;
    @Column(name = "NEWS_TYPE")
    private String newsType;
    @Column(name = "CONTENT")
    private String contentEn;
    @Column(name = "CONTENT_LC")
    private String contentLc;
    @Column(name = "CREATE_TIME")
    @Temporal(TemporalType.DATE)
    private Date createTime;
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "STATUS")
    private Integer status;
    @Column(name = "UPDATE_TIME")
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date updateTime;

    @Column(name = "PUBLISH_TIME")
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date publishTime;

    @Column(name = "TEST_MOBILE_PHONE")
    private String testMobilePhone;
}
