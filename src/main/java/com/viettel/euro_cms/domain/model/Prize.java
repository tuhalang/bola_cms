package com.viettel.euro_cms.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "PREDICTION_PRIZE")
public class Prize {

    @Id
    @Column(name = "ID")
    private String id;
    @Column(name = "NAME_EN")
    private String nameEn;
    @Column(name = "NAME_LC")
    private String nameLc;
    @Column(name = "PRIZE_TYPE")
    private Integer type;
    @Column(name = "PRIZE_VALUE")
    private Double value;
    @Column(name = "DESC_EN")
    private String descEn;
    @Column(name = "DESC_LC")
    private String descLc;
    @Column(name = "STATUS")
    private Integer status;
}
