package com.viettel.euro_cms.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "TEAM")
public class Team {

    @Id
    @Column(name = "ID")
    private String id;
    @Column(name = "CODE")
    private String code;
    @Column(name = "NAME_EN")
    private String nameEn;
    @Column(name = "NAME_LC")
    private String nameLc;
    @Column(name = "MEMBER_GROUP")
    private String group;
    @Column(name = "STATE")
    private Integer state;
    @Column(name = "IMAGE_URL")
    private String imageUrl;
    @Column(name = "SCORES")
    private Integer scores;
    @Column(name = "STATUS")
    private Integer status;
    @Column(name = "DESC_EN")
    private String descEn;
    @Column(name = "DESC_LC")
    private String descLc;

}
