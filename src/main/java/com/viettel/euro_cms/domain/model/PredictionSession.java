package com.viettel.euro_cms.domain.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "PREDICTION_SESSION")
public class PredictionSession {

    @Id
    @Column(name = "ID")
    private String id;
    @Column(name = "NAME_EN")
    private String nameEn;
    @Column(name = "NAME_LC")
    private String nameLc;
    @Column(name = "PREDICT_TYPE")
    private Integer type;
    @Column(name = "MATCH")
    private String match;
    @Column(name = "WINNER_PACKAGE")
    private String winnerPackage;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "START_TIME")
    @Temporal(TemporalType.DATE)
    private Date startTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "END_TIME")
    @Temporal(TemporalType.DATE)
    private Date endTime;
    @Column(name = "DESC_EN")
    private String descEn;
    @Column(name = "DESC_LC")
    private String descLc;
    @Column(name = "QUESTION_EN")
    private String questionEn;
    @Column(name = "QUESTION_LC")
    private String questionLc;
    @Column(name = "STATUS")
    private Integer status;
    @Column(name = "FINAL_SCORE")
    private String result;
}
