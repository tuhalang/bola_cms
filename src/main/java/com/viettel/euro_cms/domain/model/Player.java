package com.viettel.euro_cms.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "PLAYER")
public class Player {

    @Id
    @Column(name = "ID")
    private String id;
    @Column(name = "NAME_EN")
    private String nameEn;
    @Column(name = "NAME_LC")
    private String nameLc;
    @Column(name = "TEAM")
    private String team;
    @Column(name = "SQUAD_NUMBER")
    private String number;
    @Column(name = "GOALS")
    private Integer goals;
    @Column(name = "POSITION")
    private String position;
    @Column(name = "IMAGE_URL")
    private String imageUrl;
    @Column(name = "DESC_EN")
    private String descEn;
    @Column(name = "DESC_LC")
    private String descLc;

}
