package com.viettel.euro_cms.domain.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "PRIZE_PACKAGE")
public class PrizePackage {

    @Id
    private PrizePackageId id;
    @Column(name = "PRIZE_AMOUNT")
    private Integer amount;
    @Column(name = "PRIZE_RANK")
    private Integer rank;
    @Column(name = "STATUS")
    private Integer status;
}


