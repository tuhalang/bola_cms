package com.viettel.euro_cms.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "PREDICTION_RESULT")
public class PredictionResult {

    @Id
    @Column(name = "ID")
    private String id;
    @Column(name = "ISDN")
    private String isdn;
    @Column(name = "PREDICTION_SESSION")
    private String predictionSession;
    @Column(name = "PREDICTION_TURN")
    private String predictionTurn;
    @Column(name = "PRIZE")
    private String prize;
    @Column(name = "PRIZE_RANK")
    private Integer rank;
    @Column(name = "STATUS")
    private Integer status;
}
