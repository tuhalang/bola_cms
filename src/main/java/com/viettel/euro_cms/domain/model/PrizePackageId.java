package com.viettel.euro_cms.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Embeddable
public class PrizePackageId implements Serializable {

    @Column(name = "PRIZE_ID")
    private String prizeId;

    @Column(name = "PACKAGE_ID")
    private String packageId;
}