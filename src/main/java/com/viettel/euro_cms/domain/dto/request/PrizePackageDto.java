package com.viettel.euro_cms.domain.dto.request;

import com.viettel.euro_cms.domain.model.Prize;
import com.viettel.euro_cms.domain.model.PrizePackage;
import com.viettel.euro_cms.domain.model.WinnerPackage;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PrizePackageDto implements Serializable {

    private String prizeId;
    private String packageId;
    private Integer amount;
    private Integer rank;
    private Integer status;

    public PrizePackageDto(String prizeId, String packageId, Integer amount, Integer rank, Integer status) {
        this.prizeId = prizeId;
        this.packageId = packageId;
        this.amount = amount;
        this.rank = rank;
        this.status = status;
    }

    private String prizeName;
    private String winnerPackageName;
}
