package com.viettel.euro_cms.domain.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PlayerDto implements Serializable {

    private String id;
    private String nameEn;
    private String nameLc;
    private String team;
    private Integer goals;
    private String number;
    private String position;
    private String imageUrl;
    private String descEn;
    private String descLc;

}
