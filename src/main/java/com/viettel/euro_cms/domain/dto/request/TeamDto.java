package com.viettel.euro_cms.domain.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TeamDto implements Serializable {

    private String code;
    private String nameEn;
    private String nameLc;
    private String group;
    private Integer state;
    private String imageUrl;
    private String descEn;
    private String descLc;
}
