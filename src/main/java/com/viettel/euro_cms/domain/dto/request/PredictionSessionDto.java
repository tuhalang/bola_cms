package com.viettel.euro_cms.domain.dto.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PredictionSessionDto implements Serializable {

    private String id;
    private String nameEn;
    private String nameLc;
    private Integer type;
    private String match;
    private String winnerPackage;
    @JsonFormat(pattern = "YYYY-MM-dd hh:mm:ss")
    @Temporal(TemporalType.DATE)
    private Date startTime;
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "YYYY-MM-dd hh:mm:ss")
    private Date endTime;
    private String descEn;
    private String descLc;
    private String questionEn;
    private String questionLc;
    private Integer status;
    private String matchName;
    private String winnerPackageName;
    private String result;
}
