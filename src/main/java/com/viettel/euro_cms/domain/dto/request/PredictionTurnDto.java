package com.viettel.euro_cms.domain.dto.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PredictionTurnDto implements Serializable {

    private String id;
    private String isdn;
    private String predictionSession;
    private Integer type;
    private String predictValue;
    private Integer answer;
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date predictTime;
    private Integer status;
    private String channel;
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date updateTime;
}
