package com.viettel.euro_cms.domain.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.LinkedHashMap;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResponseDto implements Serializable {

    private String errorCode;
    private String message;
    private LinkedHashMap<String, Object> data;

    public void pushData(String key, Object value){
        if (data == null) {
            data = new LinkedHashMap<>();
        }
        data.put(key, value);
    }
}
