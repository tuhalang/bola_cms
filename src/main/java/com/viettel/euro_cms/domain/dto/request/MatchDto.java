package com.viettel.euro_cms.domain.dto.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MatchDto {

    private String id;
    private String firstTeam;
    private String secondTeam;
    private Integer firstTeamGoals;
    private Integer secondTeamGoals;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.DATE)
    private Date startTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.DATE)
    private Date endTime;
    private String placeEn;
    private String placeLc;
    private String descEn;
    private String descLc;
    private Integer status;
    private String matchName;
    private String state;
}
