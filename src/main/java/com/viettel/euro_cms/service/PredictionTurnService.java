package com.viettel.euro_cms.service;

import com.viettel.euro_cms.domain.dto.response.ResponseDto;
import org.springframework.data.domain.Pageable;

public interface PredictionTurnService {
    void find(ResponseDto responseDto, Pageable pageable, Integer status, String isdn) throws  Exception;
}
