package com.viettel.euro_cms.service;

import com.viettel.euro_cms.domain.dto.response.ResponseDto;
import com.viettel.euro_cms.domain.model.Prize;
import org.springframework.data.domain.Pageable;

public interface PrizeService {

    void create(ResponseDto responseDto, Prize prize) throws  Exception;
    void update(ResponseDto responseDto, Prize prize) throws  Exception;
    void find(ResponseDto responseDto, Pageable pageable, Integer status, String key) throws  Exception;
}
