package com.viettel.euro_cms.service;

import com.viettel.euro_cms.domain.dto.response.ResponseDto;
import com.viettel.euro_cms.domain.model.News;
import org.springframework.data.domain.Pageable;

public interface NewsService {

    void create(ResponseDto responseDto, News news) throws  Exception;
    void update(ResponseDto responseDto, News news) throws  Exception;
    void find(ResponseDto responseDto, Pageable pageable, Integer status, String key) throws  Exception;
    void testSMS(ResponseDto responseDto, String newsId) throws  Exception;
}
