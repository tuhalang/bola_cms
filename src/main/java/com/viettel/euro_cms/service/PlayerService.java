package com.viettel.euro_cms.service;

import com.viettel.euro_cms.domain.dto.request.PlayerDto;
import com.viettel.euro_cms.domain.dto.response.ResponseDto;
import com.viettel.euro_cms.domain.model.Player;
import org.springframework.data.domain.Pageable;

public interface PlayerService {

    void create(ResponseDto responseDto, PlayerDto playerDto) throws  Exception;
    void update(ResponseDto responseDto, Player player) throws  Exception;
    void find(ResponseDto responseDto, Pageable pageable, Integer status, String key, String team) throws  Exception;
}
