package com.viettel.euro_cms.service.impl;

import com.viettel.euro_cms.common.Constant;
import com.viettel.euro_cms.domain.dto.request.PrizePackageDto;
import com.viettel.euro_cms.domain.dto.response.ResponseDto;
import com.viettel.euro_cms.domain.model.Prize;
import com.viettel.euro_cms.domain.model.PrizePackage;
import com.viettel.euro_cms.domain.model.PrizePackageId;
import com.viettel.euro_cms.domain.model.WinnerPackage;
import com.viettel.euro_cms.repo.PackageRepo;
import com.viettel.euro_cms.repo.PrizePackageRepo;
import com.viettel.euro_cms.repo.PrizeRepo;
import com.viettel.euro_cms.service.PrizePackageService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
public class PrizePackageServiceImpl implements PrizePackageService {

    private final PrizePackageRepo prizePackageRepo;
    private final PrizeRepo prizeRepo;
    private final PackageRepo packageRepo;

    @Override
    public void create(ResponseDto responseDto, PrizePackageDto prizePackageDto) throws Exception {
        Prize prize = prizeRepo.findById(prizePackageDto.getPrizeId()).orElse(null);
        WinnerPackage winnerPackage = packageRepo.findById(prizePackageDto.getPackageId()).orElse(null);

        if(prize == null || winnerPackage == null){
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage("Prize or Package is not exists !");
            return;
        }

        PrizePackageId id = new PrizePackageId(prize.getId(), winnerPackage.getId());
        PrizePackage prizePackage = new PrizePackage();
        prizePackage.setId(id);
        prizePackage.setAmount(prizePackageDto.getAmount());
        prizePackage.setRank(prizePackage.getRank());
        prizePackage.setStatus(1);

        prizePackageRepo.saveAndFlush(prizePackage);
        responseDto.setErrorCode(Constant.ERROR_CODE_OK);
        responseDto.setMessage("Success !");
    }

    @Override
    public void update(ResponseDto responseDto, PrizePackageDto prizePackageDto) throws Exception {
        PrizePackageId id = new PrizePackageId(prizePackageDto.getPrizeId(), prizePackageDto.getPackageId());
        PrizePackage oldPrizePackage = prizePackageRepo.findById(id).orElse(null);
        if(oldPrizePackage == null){
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage("PrizePackage is not exists !");
            return;
        }

        if(!ObjectUtils.isEmpty(prizePackageDto.getAmount()) && !prizePackageDto.getAmount().equals(oldPrizePackage.getAmount())){
            oldPrizePackage.setAmount(prizePackageDto.getAmount());
        }

        if(!ObjectUtils.isEmpty(prizePackageDto.getRank()) && !prizePackageDto.getRank().equals(oldPrizePackage.getRank())){
            oldPrizePackage.setRank(prizePackageDto.getRank());
        }

        if(!ObjectUtils.isEmpty(prizePackageDto.getStatus()) && !prizePackageDto.getStatus().equals(oldPrizePackage.getStatus())){
            oldPrizePackage.setStatus(prizePackageDto.getStatus());
        }

        prizePackageRepo.saveAndFlush(oldPrizePackage);
        responseDto.setErrorCode(Constant.ERROR_CODE_OK);
        responseDto.setMessage("Success !");
    }

    @Override
    public void find(ResponseDto responseDto, Pageable pageable, String packageId, Integer status) throws Exception {

        Page<PrizePackageDto> page;

        if(status != null){
            page = prizePackageRepo.find(pageable, packageId, status);
        } else {
            page = prizePackageRepo.find(pageable, packageId);
        }

        List<PrizePackageDto> list = page.getContent();
        list.stream().map(p -> {
            Prize prize = prizeRepo.findById(p.getPrizeId()).orElse(null);
            WinnerPackage winnerPackage = packageRepo.findById(p.getPackageId()).orElse(null);
            if(prize != null){
                p.setPrizeName(prize.getNameEn());
            }
            if(winnerPackage != null){
                p.setWinnerPackageName(winnerPackage.getNameEn());
            }
            return p;
        }).collect(Collectors.toList());

        responseDto.setErrorCode(Constant.ERROR_CODE_OK);
        responseDto.setMessage(Constant.MSG_SUCCESS);
        responseDto.pushData("items", page.getContent());
        responseDto.pushData("totalElements", page.getTotalElements());
        responseDto.pushData("totalPages", page.getTotalPages());
    }
}
