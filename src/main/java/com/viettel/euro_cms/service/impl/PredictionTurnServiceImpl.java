package com.viettel.euro_cms.service.impl;

import com.viettel.euro_cms.common.Constant;
import com.viettel.euro_cms.domain.dto.request.PredictionTurnDto;
import com.viettel.euro_cms.domain.dto.response.ResponseDto;
import com.viettel.euro_cms.domain.model.PredictionResult;
import com.viettel.euro_cms.repo.PredictionTurnRepo;
import com.viettel.euro_cms.service.PredictionTurnService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
@Slf4j
public class PredictionTurnServiceImpl implements PredictionTurnService {

    private final PredictionTurnRepo predictionSession;


    @Override
    public void find(ResponseDto responseDto, Pageable pageable, Integer status, String isdn) throws Exception {
        Page<PredictionTurnDto> page = predictionSession.find(pageable, isdn);
        responseDto.setErrorCode(Constant.ERROR_CODE_OK);
        responseDto.setMessage(Constant.MSG_SUCCESS);
        responseDto.pushData("items", page.getContent());
        responseDto.pushData("totalElements", page.getTotalElements());
        responseDto.pushData("totalPages", page.getTotalPages());
    }
}
