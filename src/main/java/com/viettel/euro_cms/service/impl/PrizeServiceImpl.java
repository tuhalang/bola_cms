package com.viettel.euro_cms.service.impl;

import com.viettel.euro_cms.common.Constant;
import com.viettel.euro_cms.domain.dto.response.ResponseDto;
import com.viettel.euro_cms.domain.model.Prize;
import com.viettel.euro_cms.domain.model.Team;
import com.viettel.euro_cms.repo.PrizeRepo;
import com.viettel.euro_cms.service.PrizeService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
@AllArgsConstructor
@Slf4j
public class PrizeServiceImpl implements PrizeService {

    private final PrizeRepo prizeRepo;


    @Override
    public void create(ResponseDto responseDto, Prize prize) throws Exception {
        prize.setId(prizeRepo.getID());
        prizeRepo.saveAndFlush(prize);
        responseDto.setErrorCode(Constant.ERROR_CODE_OK);
        responseDto.setMessage("Success !");
    }

    @Override
    public void update(ResponseDto responseDto, Prize prize) throws Exception {
        Prize oldPrize = prizeRepo.findById(prize.getId()).orElse(null);
        if(oldPrize == null){
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage("Prize is not exists !");
            return;
        }

        if(!ObjectUtils.isEmpty(prize.getDescEn()) && !prize.getDescEn().equals(oldPrize.getDescEn())){
            oldPrize.setDescEn(prize.getDescEn());
        }

        if(!ObjectUtils.isEmpty(prize.getDescLc()) && !prize.getDescLc().equals(oldPrize.getDescLc())){
            oldPrize.setDescLc(prize.getDescLc());
        }

        if(!ObjectUtils.isEmpty(prize.getStatus()) && !prize.getStatus().equals(oldPrize.getStatus())){
            oldPrize.setStatus(prize.getStatus());
        }

        if(!ObjectUtils.isEmpty(prize.getNameEn()) && !prize.getNameEn().equals(oldPrize.getNameEn())){
            oldPrize.setNameEn(prize.getNameEn());
        }

        if(!ObjectUtils.isEmpty(prize.getNameLc()) && !prize.getNameLc().equals(oldPrize.getNameLc())){
            oldPrize.setNameLc(prize.getNameLc());
        }

        if(!ObjectUtils.isEmpty(prize.getType()) && !prize.getType().equals(oldPrize.getType())){
            oldPrize.setType(prize.getType());
        }

        if(!ObjectUtils.isEmpty(prize.getValue()) && !prize.getValue().equals(oldPrize.getValue())){
            oldPrize.setValue(prize.getValue());
        }

        prizeRepo.saveAndFlush(oldPrize);
        responseDto.setErrorCode(Constant.ERROR_CODE_OK);
        responseDto.setMessage("Success !");
    }

    @Override
    public void find(ResponseDto responseDto, Pageable pageable, Integer status, String key) throws Exception {
        Page<Prize> page;
        if(key == null){
            key = "";
        }
        if(status != null){
            page = prizeRepo.find(pageable, status, key);
        }else{
            page = prizeRepo.find(pageable, key);
        }
        responseDto.setErrorCode(Constant.ERROR_CODE_OK);
        responseDto.setMessage(Constant.MSG_SUCCESS);
        responseDto.pushData("items", page.getContent());
        responseDto.pushData("totalElements", page.getTotalElements());
        responseDto.pushData("totalPages", page.getTotalPages());
    }
}
