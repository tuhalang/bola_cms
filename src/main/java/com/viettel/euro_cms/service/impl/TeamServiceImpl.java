package com.viettel.euro_cms.service.impl;

import com.viettel.euro_cms.common.Constant;
import com.viettel.euro_cms.domain.dto.request.TeamDto;
import com.viettel.euro_cms.domain.dto.response.ResponseDto;
import com.viettel.euro_cms.domain.model.Team;
import com.viettel.euro_cms.repo.TeamRepo;
import com.viettel.euro_cms.service.TeamService;
import com.viettel.euro_cms.util.FileUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
@AllArgsConstructor
@Slf4j
public class TeamServiceImpl implements TeamService {

    private final TeamRepo teamRepo;

    @Override
    public void create(ResponseDto responseDto, TeamDto teamDto) throws Exception {
        Team team = new Team();
        team.setId(teamRepo.getID());

        team.setCode(teamDto.getCode());
        team.setGroup(teamDto.getGroup());
        team.setNameEn(teamDto.getNameEn());
        team.setNameLc(teamDto.getNameLc());
        team.setState(1);
        team.setScores(0);
        team.setStatus(1);
        team.setDescEn(teamDto.getDescEn());
        team.setDescLc(teamDto.getDescLc());
        team.setImageUrl(teamDto.getImageUrl());

        teamRepo.saveAndFlush(team);

        responseDto.setErrorCode(Constant.ERROR_CODE_OK);
        responseDto.setMessage("Success !");
    }

    @Override
    public void update(ResponseDto responseDto, Team team) throws Exception {
        Team oldTeam = teamRepo.findById(team.getId()).orElse(null);
        if(oldTeam == null){
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage("Team is not exists !");
            return;
        }

        if(!ObjectUtils.isEmpty(team.getCode()) && !team.getCode().equals(oldTeam.getCode())){
            oldTeam.setCode(team.getCode());
        }

        if(!ObjectUtils.isEmpty(team.getNameEn()) && !team.getNameEn().equals(oldTeam.getNameEn())){
            oldTeam.setNameLc(team.getNameEn());
        }

        if(!ObjectUtils.isEmpty(team.getNameLc()) && !team.getNameLc().equals(oldTeam.getNameLc())){
            oldTeam.setNameLc(team.getNameLc());
        }

        if(!ObjectUtils.isEmpty(team.getGroup()) && !team.getGroup().equals(oldTeam.getGroup())){
            oldTeam.setGroup(team.getGroup());
        }

        if(!ObjectUtils.isEmpty(team.getDescLc()) && !team.getDescLc().equals(oldTeam.getDescLc())){
            oldTeam.setDescLc(team.getDescLc());
        }

        if(!ObjectUtils.isEmpty(team.getDescEn()) && !team.getDescEn().equals(oldTeam.getDescEn())){
            oldTeam.setDescEn(team.getDescEn());
        }

        if(!ObjectUtils.isEmpty(team.getScores()) && !team.getScores().equals(oldTeam.getScores())){
            oldTeam.setScores(team.getScores());
        }

        if(!ObjectUtils.isEmpty(team.getState()) && !team.getState().equals(oldTeam.getState())){
            oldTeam.setState(team.getState());
        }

        if(!ObjectUtils.isEmpty(team.getStatus()) && !team.getStatus().equals(oldTeam.getStatus())){
            oldTeam.setStatus(team.getStatus());
        }

        if(!ObjectUtils.isEmpty(team.getImageUrl()) && !team.getImageUrl().equals(oldTeam.getImageUrl())){
            oldTeam.setImageUrl(team.getImageUrl());
        }

        teamRepo.saveAndFlush(oldTeam);
        responseDto.setErrorCode(Constant.ERROR_CODE_OK);
        responseDto.setMessage("Success !");

    }

    @Override
    public void find(ResponseDto responseDto, Pageable pageable, Integer status, String key) throws Exception {
        Page<Team> page;
        if(key == null){
            key = "";
        }
        if(status != null){
            page = teamRepo.find(pageable, status, key);
        }else{
            page = teamRepo.find(pageable, key);
        }
        responseDto.setErrorCode(Constant.ERROR_CODE_OK);
        responseDto.setMessage(Constant.MSG_SUCCESS);
        responseDto.pushData("items", page.getContent());
        responseDto.pushData("totalElements", page.getTotalElements());
        responseDto.pushData("totalPages", page.getTotalPages());
    }


}
