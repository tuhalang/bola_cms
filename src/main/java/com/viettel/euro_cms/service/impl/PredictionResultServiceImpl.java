package com.viettel.euro_cms.service.impl;

import com.viettel.euro_cms.common.Constant;
import com.viettel.euro_cms.domain.dto.request.PlayerDto;
import com.viettel.euro_cms.domain.dto.response.ResponseDto;
import com.viettel.euro_cms.domain.model.PredictionResult;
import com.viettel.euro_cms.repo.PredictionResultRepo;
import com.viettel.euro_cms.service.PredictionResultService;
import com.viettel.euro_cms.service.PredictionSessionService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Slf4j
public class PredictionResultServiceImpl implements PredictionResultService {

    private final PredictionResultRepo predictionResultRepo;

    @Override
    public void find(ResponseDto responseDto, Pageable pageable, String predictionSession) throws Exception {
        Page<PredictionResult> page = predictionResultRepo.find(pageable, predictionSession);
        responseDto.setErrorCode(Constant.ERROR_CODE_OK);
        responseDto.setMessage(Constant.MSG_SUCCESS);
        responseDto.pushData("items", page.getContent());
        responseDto.pushData("totalElements", page.getTotalElements());
        responseDto.pushData("totalPages", page.getTotalPages());
    }
}
