package com.viettel.euro_cms.service.impl;

import com.viettel.euro_cms.common.Constant;
import com.viettel.euro_cms.domain.dto.request.AccountDto;
import com.viettel.euro_cms.domain.dto.response.ResponseDto;
import com.viettel.euro_cms.security.JwtTokenProvider;
import com.viettel.euro_cms.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
@AllArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final PasswordEncoder passwordEncoder;

    @Override
    public void signIn(ResponseDto responseDTO, AccountDto accountDTO) throws Exception {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        accountDTO.getUsername(),
                        accountDTO.getPassword(),
                        Collections.emptyList()
                )
        );
        SimpleGrantedAuthority sga = (SimpleGrantedAuthority) authentication.getAuthorities().iterator().next();
        String role = sga.getAuthority();
        String jwt = jwtTokenProvider.generateUserToken(accountDTO.getUsername(), role);


        responseDTO.setErrorCode(Constant.ERROR_CODE_OK);
        responseDTO.setMessage(Constant.MSG_SIGN_IN_OK);
        responseDTO.pushData("token", jwt);
        responseDTO.pushData("roles", role);
    }
}
