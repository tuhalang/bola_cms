package com.viettel.euro_cms.service.impl;

import com.viettel.euro_cms.common.Constant;
import com.viettel.euro_cms.domain.dto.response.ResponseDto;
import com.viettel.euro_cms.domain.model.News;
import com.viettel.euro_cms.domain.model.Prize;
import com.viettel.euro_cms.repo.NewsRepo;
import com.viettel.euro_cms.service.NewsService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
public class NewsServiceImpl implements NewsService {

    private final NewsRepo newsRepo;

    @Value("${API_SMS}")
    private String urlSms;

    @Override
    public void create(ResponseDto responseDto, News news) throws Exception {
        news.setId(newsRepo.getID());
        Date now = new Date();
        news.setCreateTime(now);
        news.setUpdateTime(now);
        news.setStatus(3);
        newsRepo.saveAndFlush(news);
        responseDto.setErrorCode(Constant.ERROR_CODE_OK);
        responseDto.setMessage("Success !");
    }

    @Override
    public void update(ResponseDto responseDto, News news) throws Exception {
        News oldNews = newsRepo.findById(news.getId()).orElse(null);
        if(oldNews == null){
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage("News is not exists !");
            return;
        }

        if(!ObjectUtils.isEmpty(news.getNewsType()) && !news.getNewsType().equals(oldNews.getNewsType())){
            oldNews.setNewsType(news.getNewsType());
        }

        if(!ObjectUtils.isEmpty(news.getContentEn()) && !news.getContentEn().equals(oldNews.getContentEn())){
            oldNews.setContentEn(news.getContentEn());
        }

        if(!ObjectUtils.isEmpty(news.getContentLc()) && !news.getContentLc().equals(oldNews.getContentLc())){
            oldNews.setContentLc(news.getContentLc());
        }

        if(!ObjectUtils.isEmpty(news.getStatus()) && !news.getStatus().equals(oldNews.getStatus())){
            oldNews.setStatus(news.getStatus());
        }

        if(!ObjectUtils.isEmpty(news.getPublishTime()) && !news.getPublishTime().equals(oldNews.getPublishTime())){
            oldNews.setPublishTime(news.getPublishTime());
        }

        if(!ObjectUtils.isEmpty(news.getTestMobilePhone()) && !news.getTestMobilePhone().equals(oldNews.getTestMobilePhone())){
            oldNews.setTestMobilePhone(news.getTestMobilePhone());
        }

        Date now = new Date();
        news.setUpdateTime(now);

        newsRepo.saveAndFlush(oldNews);
        responseDto.setErrorCode(Constant.ERROR_CODE_OK);
        responseDto.setMessage("Success !");
    }

    @Override
    public void testSMS(ResponseDto responseDto, String newsId) throws Exception {
        News news = newsRepo.findById(newsId).orElse(null);
        if(news != null){
            LinkedHashMap<String, Object> body = new LinkedHashMap<>();
            LinkedHashMap<String, Object> wsRequest = new LinkedHashMap<>();

            wsRequest.put("gameCode", "BOLA");
            wsRequest.put("language", "pt");
            wsRequest.put("isdn", news.getTestMobilePhone());
            wsRequest.put("newsId", news.getId());
            wsRequest.put("transId", UUID.randomUUID().toString());

            body.put("wsCode", "wsSentSms");
            body.put("wsRequest", wsRequest);

            try {
                HttpHeaders headers = new HttpHeaders();
                headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
                HttpEntity<LinkedHashMap> entity = new HttpEntity<LinkedHashMap>(body, headers);

                RestTemplate restTemplate = new RestTemplateBuilder()
                        .setConnectTimeout(Duration.ofSeconds(10))
                        .setReadTimeout(Duration.ofSeconds(10))
                        .build();

                LinkedHashMap<String, Object> response = restTemplate.exchange(
                        urlSms, HttpMethod.POST, entity, LinkedHashMap.class).getBody();

                if (response.get("errorCode").equals("0")) {
                    news.setStatus(2);
                    newsRepo.saveAndFlush(news);
                    responseDto.setErrorCode(Constant.ERROR_CODE_OK);
                    responseDto.pushData("type", "SMS");
                    responseDto.setMessage("Send SMS successfully !");
                    return;
                }
            }catch (Exception e){
                log.error(e.getMessage(), e);
                responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
                responseDto.setMessage("Send SMS failed !");
            }
        }
        responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
        responseDto.setMessage("Send SMS failed !");
    }

    @Override
    public void find(ResponseDto responseDto, Pageable pageable, Integer status, String key) throws Exception {
        Page<News> page;
        if(key == null){
            key = "";
        }
        if(status != null){
            page = newsRepo.find(pageable, status, key);
        }else{
            page = newsRepo.find(pageable, key);
        }
        responseDto.setErrorCode(Constant.ERROR_CODE_OK);
        responseDto.setMessage(Constant.MSG_SUCCESS);
        responseDto.pushData("items", page.getContent());
        responseDto.pushData("totalElements", page.getTotalElements());
        responseDto.pushData("totalPages", page.getTotalPages());
    }
}
