package com.viettel.euro_cms.service.impl;

import com.viettel.euro_cms.common.Constant;
import com.viettel.euro_cms.domain.dto.response.ResponseDto;
import com.viettel.euro_cms.domain.model.WinnerPackage;
import com.viettel.euro_cms.repo.PackageRepo;
import com.viettel.euro_cms.service.PackageService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
@AllArgsConstructor
@Slf4j
public class PackageServiceImpl implements PackageService {

    private final PackageRepo packageRepo;

    @Override
    public void create(ResponseDto responseDto, WinnerPackage winnerPackage) throws Exception {
        winnerPackage.setId(packageRepo.getID());
        winnerPackage.setStatus(1);

        packageRepo.saveAndFlush(winnerPackage);
        responseDto.setErrorCode(Constant.ERROR_CODE_OK);
        responseDto.setMessage("Success !");
    }

    @Override
    public void update(ResponseDto responseDto, WinnerPackage winnerPackage) throws Exception {
        WinnerPackage oldPackage = packageRepo.findById(winnerPackage.getId()).orElse(null);
        if(oldPackage == null){
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage("Package is not exists !");
            return;
        }

        if(!ObjectUtils.isEmpty(winnerPackage.getDescEn()) && !winnerPackage.getDescLc().equals(oldPackage.getDescEn())){
            oldPackage.setDescEn(winnerPackage.getDescEn());
        }

        if(!ObjectUtils.isEmpty(winnerPackage.getDescLc()) && !winnerPackage.getDescLc().equals(oldPackage.getDescLc())){
            oldPackage.setDescLc(winnerPackage.getDescLc());
        }

        if(!ObjectUtils.isEmpty(winnerPackage.getNameEn()) && !winnerPackage.getNameEn().equals(oldPackage.getNameEn())){
            oldPackage.setNameEn(winnerPackage.getNameEn());
        }

        if(!ObjectUtils.isEmpty(winnerPackage.getNameLc()) && !winnerPackage.getNameLc().equals(oldPackage.getNameLc())){
            oldPackage.setNameLc(winnerPackage.getNameLc());
        }

        if(!ObjectUtils.isEmpty(winnerPackage.getTotalPrize()) && !winnerPackage.getTotalPrize().equals(oldPackage.getTotalPrize())){
            oldPackage.setTotalPrize(winnerPackage.getTotalPrize());
        }

        if(!ObjectUtils.isEmpty(winnerPackage.getStatus()) && !winnerPackage.getStatus().equals(oldPackage.getStatus())){
            oldPackage.setStatus(winnerPackage.getStatus());
        }

        packageRepo.saveAndFlush(oldPackage);
        responseDto.setErrorCode(Constant.ERROR_CODE_OK);
        responseDto.setMessage("Success !");
    }

    @Override
    public void find(ResponseDto responseDto, Pageable pageable, Integer status, String key) throws Exception {
        if(key == null){
            key = "";
        }

        Page<WinnerPackage> page;
        if(status != null) {
            page = packageRepo.find(pageable, status, key);
        } else {
            page = packageRepo.find(pageable, key);
        }
        responseDto.setErrorCode(Constant.ERROR_CODE_OK);
        responseDto.setMessage(Constant.MSG_SUCCESS);
        responseDto.pushData("items", page.getContent());
        responseDto.pushData("totalElements", page.getTotalElements());
        responseDto.pushData("totalPages", page.getTotalPages());
    }
}
