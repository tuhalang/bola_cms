package com.viettel.euro_cms.service.impl;

import com.viettel.euro_cms.common.Constant;
import com.viettel.euro_cms.domain.dto.request.PlayerDto;
import com.viettel.euro_cms.domain.dto.response.ResponseDto;
import com.viettel.euro_cms.domain.model.Player;
import com.viettel.euro_cms.domain.model.Team;
import com.viettel.euro_cms.repo.PlayerRepo;
import com.viettel.euro_cms.repo.TeamRepo;
import com.viettel.euro_cms.service.PlayerService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
@AllArgsConstructor
@Slf4j
public class PlayerServiceImpl implements PlayerService {

    private final PlayerRepo playerRepo;
    private final TeamRepo teamRepo;

    @Override
    public void create(ResponseDto responseDto, PlayerDto playerDto) throws Exception {
        Player player = new Player();
        Team team = teamRepo.findById(playerDto.getTeam()).orElse(null);
        if(team == null){
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage("Team is not exists !");
            return;
        }

        player.setId(playerRepo.getID());
        player.setGoals(0);
        player.setDescEn(playerDto.getDescEn());
        player.setDescLc(playerDto.getDescLc());
        player.setNameEn(playerDto.getNameEn());
        player.setNameLc(playerDto.getNameLc());
        player.setPosition(playerDto.getPosition());
        player.setNumber(playerDto.getNumber());
        player.setImageUrl(playerDto.getImageUrl());
        player.setTeam(playerDto.getTeam());

        playerRepo.saveAndFlush(player);

        responseDto.setErrorCode(Constant.ERROR_CODE_OK);
        responseDto.setMessage("Success !");
    }

    @Override
    public void update(ResponseDto responseDto, Player player) throws Exception {
        Player oldPlayer = playerRepo.findById(player.getId()).orElse(null);
        if(oldPlayer == null){
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage("Player is not exists !");
            return;
        }

        if(!ObjectUtils.isEmpty(player.getGoals()) && !player.getGoals().equals(oldPlayer.getGoals())){
            oldPlayer.setGoals(player.getGoals());
        }

        if(!ObjectUtils.isEmpty(player.getNameEn()) && !player.getNameEn().equals(oldPlayer.getNameEn())){
            oldPlayer.setNameEn(player.getNameEn());
        }

        if(!ObjectUtils.isEmpty(player.getNameLc()) && !player.getNameLc().equals(oldPlayer.getNameLc())){
            oldPlayer.setNameLc(player.getNameLc());
        }

        if(!ObjectUtils.isEmpty(player.getDescEn()) && !player.getDescEn().equals(oldPlayer.getDescEn())){
            oldPlayer.setDescEn(player.getDescEn());
        }

        if(!ObjectUtils.isEmpty(player.getDescLc()) && !player.getDescLc().equals(oldPlayer.getDescLc())){
            oldPlayer.setDescLc(player.getDescLc());
        }

        if(!ObjectUtils.isEmpty(player.getNumber()) && !player.getNumber().equals(oldPlayer.getNumber())){
            oldPlayer.setNumber(player.getNumber());
        }

        if(!ObjectUtils.isEmpty(player.getPosition()) && !player.getPosition().equals(oldPlayer.getPosition())){
            oldPlayer.setPosition(player.getPosition());
        }

        if(!ObjectUtils.isEmpty(player.getImageUrl()) && !player.getImageUrl().equals(oldPlayer.getImageUrl())){
            oldPlayer.setImageUrl(player.getImageUrl());
        }

        if(!ObjectUtils.isEmpty(player.getTeam()) && !player.getTeam().equals(oldPlayer.getTeam())){
            Team team = teamRepo.findById(player.getTeam()).orElse(null);
            if(team != null) {
                oldPlayer.setTeam(player.getTeam());
            }else{
                responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
                responseDto.setMessage("Team is not exists !");
            }
        }

        playerRepo.saveAndFlush(oldPlayer);
        responseDto.setErrorCode(Constant.ERROR_CODE_OK);
        responseDto.setMessage("Success !");
    }

    @Override
    public void find(ResponseDto responseDto, Pageable pageable, Integer status, String key, String team) throws Exception {
        if(key == null){
            key = "";
        }
        Page<PlayerDto> page;
        if(team != null){
            page = playerRepo.findDto(pageable, key, team);
        }else{
            page = playerRepo.findDto(pageable, key);
        }
        responseDto.setErrorCode(Constant.ERROR_CODE_OK);
        responseDto.setMessage(Constant.MSG_SUCCESS);
        responseDto.pushData("items", page.getContent());
        responseDto.pushData("totalElements", page.getTotalElements());
        responseDto.pushData("totalPages", page.getTotalPages());
    }
}
