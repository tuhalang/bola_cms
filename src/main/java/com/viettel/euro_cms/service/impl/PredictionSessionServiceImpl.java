package com.viettel.euro_cms.service.impl;

import com.viettel.euro_cms.common.Constant;
import com.viettel.euro_cms.domain.dto.request.PredictionSessionDto;
import com.viettel.euro_cms.domain.dto.response.ResponseDto;
import com.viettel.euro_cms.domain.model.*;
import com.viettel.euro_cms.repo.*;
import com.viettel.euro_cms.service.PredictionSessionService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
public class PredictionSessionServiceImpl implements PredictionSessionService {

    private final PredictionSessionRepo predictionSessionRepo;
    private final PackageRepo packageRepo;

    private final MatchRepo matchRepo;
    private final TeamRepo teamRepo;
    private final PlayerRepo playerRepo;


    @Override
    public void create(ResponseDto responseDto, PredictionSession predictionSession) throws Exception {
        if(!ObjectUtils.isEmpty(predictionSession.getWinnerPackage())){
            WinnerPackage winnerPackage = packageRepo.findById(predictionSession.getWinnerPackage()).orElse(null);
            if(winnerPackage == null){
                responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
                responseDto.setMessage("Package is not exists !");
                return;
            }
        }

        if(predictionSession.getType() == 1 && ObjectUtils.isEmpty(predictionSession.getMatch())){
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage("Match cannot be empty !");
            return;
        }

        if(predictionSession.getType() == 1) {
            Match match = matchRepo.findById(predictionSession.getMatch()).orElse(null);
            if (match == null) {
                responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
                responseDto.setMessage("Match is not exists !");
                return;
            }
        }else{
            predictionSession.setMatch(null);
        }


        predictionSession.setId(predictionSessionRepo.getID());
        predictionSessionRepo.saveAndFlush(predictionSession);
        responseDto.setErrorCode(Constant.ERROR_CODE_OK);
        responseDto.setMessage("Success !");
    }

    @Override
    public void update(ResponseDto responseDto, PredictionSession predictionSession) throws Exception {
        PredictionSession oldPredictionSession = predictionSessionRepo.findById(predictionSession.getId()).orElse(null);
        if(oldPredictionSession == null){
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage("Session is not exists !");
            return;
        }

        if(!ObjectUtils.isEmpty(predictionSession.getDescEn()) && !predictionSession.getDescLc().equals(oldPredictionSession.getDescEn())){
            oldPredictionSession.setDescEn(predictionSession.getDescEn());
        }

        if(!ObjectUtils.isEmpty(predictionSession.getDescLc()) && !predictionSession.getDescLc().equals(oldPredictionSession.getDescLc())){
            oldPredictionSession.setDescLc(predictionSession.getDescLc());
        }

        if(!ObjectUtils.isEmpty(predictionSession.getEndTime()) && !predictionSession.getEndTime().equals(oldPredictionSession.getEndTime())){
            oldPredictionSession.setEndTime(predictionSession.getEndTime());
        }

        if(!ObjectUtils.isEmpty(predictionSession.getStartTime()) && !predictionSession.getStartTime().equals(oldPredictionSession.getStartTime())){
            oldPredictionSession.setStartTime(predictionSession.getStartTime());
        }

        if(!ObjectUtils.isEmpty(predictionSession.getNameEn()) && !predictionSession.getNameEn().equals(oldPredictionSession.getNameEn())){
            oldPredictionSession.setNameEn(predictionSession.getNameEn());
        }

        if(!ObjectUtils.isEmpty(predictionSession.getNameLc()) && !predictionSession.getNameLc().equals(oldPredictionSession.getNameLc())){
            oldPredictionSession.setNameLc(predictionSession.getNameLc());
        }

        if(!ObjectUtils.isEmpty(predictionSession.getType()) && !predictionSession.getType().equals(oldPredictionSession.getType())){
            oldPredictionSession.setType(predictionSession.getType());
        }

        if(!ObjectUtils.isEmpty(predictionSession.getQuestionEn()) && !predictionSession.getQuestionEn().equals(oldPredictionSession.getQuestionEn())){
            oldPredictionSession.setQuestionEn(predictionSession.getQuestionEn());
        }

        if(!ObjectUtils.isEmpty(predictionSession.getQuestionLc()) && !predictionSession.getQuestionLc().equals(oldPredictionSession.getQuestionLc())){
            oldPredictionSession.setQuestionLc(predictionSession.getQuestionLc());
        }

        if(!ObjectUtils.isEmpty(predictionSession.getStatus()) && !predictionSession.getStatus().equals(oldPredictionSession.getStatus())){
            oldPredictionSession.setStatus(predictionSession.getStatus());
        }

        if(!ObjectUtils.isEmpty(predictionSession.getResult()) && !predictionSession.getResult().equals(oldPredictionSession.getResult())){
            if(predictionSession.getType() == 1){
                if("WIN".equals(predictionSession.getResult()) || "LOSE".equals(predictionSession.getResult()) || "DRAWN".equals(predictionSession.getResult())){
                    oldPredictionSession.setResult(predictionSession.getResult());
                }else{
                    responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
                    responseDto.setMessage("Result is invalid !");
                    return;
                }
            }else if(predictionSession.getType() == 2){
                Team team = teamRepo.findById(predictionSession.getResult()).orElse(null);
                if(team == null){
                    responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
                    responseDto.setMessage("Result is invalid !");
                    return;
                }
                oldPredictionSession.setResult(predictionSession.getResult());
            }else{
                Player player = playerRepo.findById(predictionSession.getResult()).orElse(null);
                if(player == null){
                    responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
                    responseDto.setMessage("Result is invalid !");
                    return;
                }
                oldPredictionSession.setResult(predictionSession.getResult());
            }
        }

        if(!ObjectUtils.isEmpty(predictionSession.getWinnerPackage()) && !predictionSession.getWinnerPackage().equals(oldPredictionSession.getWinnerPackage())){
            WinnerPackage winnerPackage = packageRepo.findById(predictionSession.getWinnerPackage()).orElse(null);
            if(winnerPackage == null){
                responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
                responseDto.setMessage("Package is not exists !");
                return;
            }
            oldPredictionSession.setWinnerPackage(predictionSession.getWinnerPackage());
        }

        if(!ObjectUtils.isEmpty(predictionSession.getMatch()) && !predictionSession.getMatch().equals(oldPredictionSession.getMatch())){
            Match match = matchRepo.findById(predictionSession.getMatch()).orElse(null);
            if(match == null){
                responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
                responseDto.setMessage("Match is not exists !");
                return;
            }
            oldPredictionSession.setMatch(predictionSession.getMatch());
        }

        predictionSessionRepo.saveAndFlush(oldPredictionSession);
        responseDto.setErrorCode(Constant.ERROR_CODE_OK);
        responseDto.setMessage("Success !");
    }

    @Override
    public void find(ResponseDto responseDto, Pageable pageable, Integer type, String key) throws Exception {
        if(key == null){
            key = "";
        }

        Page<PredictionSessionDto> page;
        if(type == null){
            page = predictionSessionRepo.find(pageable, key);
        }else{
            page = predictionSessionRepo.find(pageable, type, key);
        }
        List<PredictionSessionDto> predictionSessionDtos = new ArrayList<>();
        if(pageable.getPageNumber() == 0) {
            predictionSessionDtos = predictionSessionRepo.find(key);
        }
        predictionSessionDtos.addAll(page.getContent());

        predictionSessionDtos.stream().map(s -> {
            if(!ObjectUtils.isEmpty(s.getResult())) {
                if(s.getType() == 2) {
                    Team team = teamRepo.findById(s.getResult()).orElse(null);
                    if(team != null) {
                        s.setResult(team.getNameEn());
                    }
                }else if(s.getType() == 3){
                    Player player = playerRepo.findById(s.getResult()).orElse(null);
                    if(player != null){
                        s.setResult(player.getNameEn());
                    }
                }
            }
            return s;
        }).collect(Collectors.toList());

        responseDto.setErrorCode(Constant.ERROR_CODE_OK);
        responseDto.setMessage(Constant.MSG_SUCCESS);
        responseDto.pushData("items", predictionSessionDtos);
        responseDto.pushData("totalElements", page.getTotalElements());
        responseDto.pushData("totalPages", page.getTotalPages());
    }
}
