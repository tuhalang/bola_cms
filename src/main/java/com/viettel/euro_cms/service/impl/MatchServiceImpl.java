package com.viettel.euro_cms.service.impl;

import com.viettel.euro_cms.common.Constant;
import com.viettel.euro_cms.domain.dto.request.MatchDto;
import com.viettel.euro_cms.domain.dto.request.PredictionSessionDto;
import com.viettel.euro_cms.domain.dto.response.ResponseDto;
import com.viettel.euro_cms.domain.model.Match;
import com.viettel.euro_cms.domain.model.PredictionSession;
import com.viettel.euro_cms.domain.model.Team;
import com.viettel.euro_cms.repo.MatchRepo;
import com.viettel.euro_cms.repo.PredictionSessionRepo;
import com.viettel.euro_cms.repo.TeamRepo;
import com.viettel.euro_cms.service.MatchService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
@AllArgsConstructor
@Slf4j
public class MatchServiceImpl implements MatchService {

    private final MatchRepo matchRepo;
    private final TeamRepo teamRepo;

    private final PredictionSessionRepo predictionSessionRepo;

    @Override
    public void create(ResponseDto responseDto, Match match) throws Exception {
        if(ObjectUtils.isEmpty(match.getFirstTeam()) || ObjectUtils.isEmpty(match.getSecondTeam())){
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage("Team is not exists !");
            return;
        }
        Team firstTeam = teamRepo.findById(match.getFirstTeam()).orElse(null);
        Team secondTeam = teamRepo.findById(match.getSecondTeam()).orElse(null);
        if(ObjectUtils.isEmpty(firstTeam) || ObjectUtils.isEmpty(secondTeam)){
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage("Team is not exists !");
            return;
        }

        match.setId(matchRepo.getID());
        match.setFirstTeamGoals(0);
        match.setSecondTeamGoals(0);
        match.setStatus(1);
        matchRepo.saveAndFlush(match);
        responseDto.setErrorCode(Constant.ERROR_CODE_OK);
        responseDto.setMessage("Success !");

    }

    @Override
    public void update(ResponseDto responseDto, Match match) throws Exception {
        Match oldMatch = matchRepo.findById(match.getId()).orElse(null);
        if(oldMatch == null){
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage("Match is not exists !");
            return;
        }

        if(!ObjectUtils.isEmpty(match.getFirstTeam()) && !match.getFirstTeam().equals(oldMatch.getFirstTeam())){
            Team firstTeam = teamRepo.findByCode(match.getFirstTeam()).orElse(null);
            if(ObjectUtils.isEmpty(firstTeam)){
                responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
                responseDto.setMessage("Team is not exists !");
                return;
            }
            oldMatch.setFirstTeam(firstTeam.getId());
        }

        if(!ObjectUtils.isEmpty(match.getSecondTeam()) && !match.getSecondTeam().equals(oldMatch.getSecondTeam())){
            Team secondTeam = teamRepo.findByCode(match.getSecondTeam()).orElse(null);
            if(ObjectUtils.isEmpty(secondTeam)){
                responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
                responseDto.setMessage("Team is not exists !");
                return;
            }
            oldMatch.setSecondTeam(secondTeam.getId());
        }

        if(!ObjectUtils.isEmpty(match.getDescEn()) && !match.getDescEn().equals(oldMatch.getDescEn())){
            oldMatch.setDescEn(match.getDescEn());
        }

        if(!ObjectUtils.isEmpty(match.getDescLc()) && !match.getDescLc().equals(oldMatch.getDescLc())){
            oldMatch.setDescLc(match.getDescLc());
        }

        if(!ObjectUtils.isEmpty(match.getEndTime()) && !match.getEndTime().equals(oldMatch.getEndTime())){
            oldMatch.setEndTime(match.getEndTime());
        }

        if(!ObjectUtils.isEmpty(match.getStartTime()) && !match.getStartTime().equals(oldMatch.getStartTime())){
            oldMatch.setStartTime(match.getStartTime());
        }

        if(!ObjectUtils.isEmpty(match.getFirstTeamGoals()) && !match.getFirstTeamGoals().equals(oldMatch.getFirstTeamGoals())){
            oldMatch.setFirstTeamGoals(match.getFirstTeamGoals());
        }

        if(!ObjectUtils.isEmpty(match.getSecondTeamGoals()) && !match.getSecondTeamGoals().equals(oldMatch.getSecondTeamGoals())){
            oldMatch.setSecondTeamGoals(match.getSecondTeamGoals());
        }

        if(!ObjectUtils.isEmpty(match.getPlaceEn()) && !match.getPlaceEn().equals(oldMatch.getPlaceEn())){
            oldMatch.setPlaceEn(match.getPlaceEn());
        }

        if(!ObjectUtils.isEmpty(match.getPlaceLc()) && !match.getPlaceLc().equals(oldMatch.getPlaceLc())){
            oldMatch.setPlaceLc(match.getPlaceLc());
        }

        if(!ObjectUtils.isEmpty(match.getStatus()) && !match.getStatus().equals(oldMatch.getStatus())){
            oldMatch.setStatus(match.getStatus());
        }

        if(oldMatch.getFirstTeamGoals() != null || oldMatch.getSecondTeamGoals() != null){
            if(oldMatch.getFirstTeamGoals() == null || oldMatch.getSecondTeamGoals() == null){
                responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
                responseDto.setMessage("You have type both team goals.");
                return;
            }

            PredictionSession predictionSession = predictionSessionRepo.findByMatch(oldMatch.getId()).orElse(null);
            if(predictionSession == null){
                responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
                responseDto.setMessage("This match hasn't assign for session");
                return;
            }

            if(oldMatch.getFirstTeamGoals() > oldMatch.getSecondTeamGoals()){
                predictionSession.setResult("WIN");
            }

            if(oldMatch.getFirstTeamGoals() < oldMatch.getSecondTeamGoals()){
                predictionSession.setResult("LOSE");
            }

            if(oldMatch.getFirstTeamGoals() == oldMatch.getSecondTeamGoals()){
                predictionSession.setResult("DRAWN");
            }
            predictionSessionRepo.saveAndFlush(predictionSession);
        }


        matchRepo.saveAndFlush(oldMatch);
        responseDto.setErrorCode(Constant.ERROR_CODE_OK);
        responseDto.setMessage("Success !");

    }

    @Override
    public void find(ResponseDto responseDto, Pageable pageable, Integer status, String key, String stateMatch) throws Exception {
        if(key == null){
            key = "";
        }

        if(stateMatch == null){
            stateMatch = "";
        }

        Page<MatchDto> page;
        if(status == null){
            page = matchRepo.find(pageable, stateMatch);
        }else{
            page = matchRepo.find(pageable, status, stateMatch);
        }
        responseDto.setErrorCode(Constant.ERROR_CODE_OK);
        responseDto.setMessage(Constant.MSG_SUCCESS);
        responseDto.pushData("items", page.getContent());
        responseDto.pushData("totalElements", page.getTotalElements());
        responseDto.pushData("totalPages", page.getTotalPages());
    }
}
