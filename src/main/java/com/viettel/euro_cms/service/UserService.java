package com.viettel.euro_cms.service;


import com.viettel.euro_cms.domain.dto.request.AccountDto;
import com.viettel.euro_cms.domain.dto.response.ResponseDto;

public interface UserService {

    void signIn(ResponseDto responseDTO, AccountDto accountDTO) throws Exception;
}
