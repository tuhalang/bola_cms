package com.viettel.euro_cms.service;

import com.viettel.euro_cms.domain.dto.response.ResponseDto;
import com.viettel.euro_cms.domain.model.Match;
import com.viettel.euro_cms.domain.model.WinnerPackage;
import org.springframework.data.domain.Pageable;

public interface MatchService {

    void create(ResponseDto responseDto, Match match) throws  Exception;
    void update(ResponseDto responseDto, Match match) throws  Exception;
    void find(ResponseDto responseDto, Pageable pageable, Integer status, String key, String stateMatch) throws  Exception;
}
