package com.viettel.euro_cms.service;

import com.viettel.euro_cms.domain.dto.response.ResponseDto;
import com.viettel.euro_cms.domain.model.PredictionSession;
import org.springframework.data.domain.Pageable;

public interface PredictionSessionService {

    void create(ResponseDto responseDto, PredictionSession predictionSession) throws  Exception;
    void update(ResponseDto responseDto, PredictionSession predictionSession) throws  Exception;
    void find(ResponseDto responseDto, Pageable pageable, Integer status, String key) throws  Exception;

}
