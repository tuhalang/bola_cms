package com.viettel.euro_cms.service;

import com.viettel.euro_cms.domain.dto.response.ResponseDto;
import com.viettel.euro_cms.domain.model.WinnerPackage;
import org.springframework.data.domain.Pageable;

public interface PackageService {

    void create(ResponseDto responseDto, WinnerPackage winnerPackage) throws  Exception;
    void update(ResponseDto responseDto, WinnerPackage winnerPackage) throws  Exception;
    void find(ResponseDto responseDto, Pageable pageable, Integer status, String key) throws  Exception;
}
