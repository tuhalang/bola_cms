package com.viettel.euro_cms.service;

import com.viettel.euro_cms.domain.dto.request.TeamDto;
import com.viettel.euro_cms.domain.dto.response.ResponseDto;
import com.viettel.euro_cms.domain.model.Team;
import org.springframework.data.domain.Pageable;


public interface TeamService {

    void create(ResponseDto responseDto, TeamDto teamDto) throws  Exception;
    void update(ResponseDto responseDto, Team team) throws  Exception;
    void find(ResponseDto responseDto, Pageable pageable, Integer status, String key) throws  Exception;
}
