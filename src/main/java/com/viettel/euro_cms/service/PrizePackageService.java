package com.viettel.euro_cms.service;

import com.viettel.euro_cms.domain.dto.request.PrizePackageDto;
import com.viettel.euro_cms.domain.dto.response.ResponseDto;
import org.springframework.data.domain.Pageable;

public interface PrizePackageService {

    void create(ResponseDto responseDto, PrizePackageDto prizePackageDto) throws  Exception;
    void update(ResponseDto responseDto, PrizePackageDto prizePackageDto) throws  Exception;
    void find(ResponseDto responseDto, Pageable pageable, String packageId, Integer status) throws  Exception;
}
