package com.viettel.euro_cms.controller;

import com.viettel.euro_cms.common.Constant;
import com.viettel.euro_cms.domain.dto.request.TeamDto;
import com.viettel.euro_cms.domain.dto.response.ResponseDto;
import com.viettel.euro_cms.domain.model.Team;
import com.viettel.euro_cms.service.TeamService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping("/api/secure")
public class TeamController {

    private final TeamService teamService;

    @RequestMapping(value = "/teams", method = RequestMethod.POST)
    public ResponseEntity create(@RequestBody TeamDto teamDto){
        ResponseDto responseDto = new ResponseDto();
        try{
            teamService.create(responseDto, teamDto);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage(e.getMessage());
        }
        return ResponseEntity.ok(responseDto);
    }

    @RequestMapping(value = "/teams", method = RequestMethod.PUT)
    public ResponseEntity update(@RequestBody Team team){
        ResponseDto responseDto = new ResponseDto();
        try{
            teamService.update(responseDto, team);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage(e.getMessage());
        }
        return ResponseEntity.ok(responseDto);
    }

    @RequestMapping(value = "/teams", method = RequestMethod.GET)
    public ResponseEntity fetch(@RequestParam Integer page,
                                @RequestParam Integer size,
                                @RequestParam(required = false) Integer status,
                                @RequestParam(required = false) String key){
        ResponseDto responseDto = new ResponseDto();
        try{
            Pageable pageable = PageRequest.of(page-1, size);
            teamService.find(responseDto, pageable, status, key);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage(e.getMessage());
        }
        return ResponseEntity.ok(responseDto);
    }
}
