package com.viettel.euro_cms.controller;

import com.viettel.euro_cms.common.Constant;
import com.viettel.euro_cms.domain.dto.request.PrizePackageDto;
import com.viettel.euro_cms.domain.dto.response.ResponseDto;
import com.viettel.euro_cms.domain.model.WinnerPackage;
import com.viettel.euro_cms.service.PrizePackageService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping("/api/secure")
public class PrizePackageController {

    private final PrizePackageService prizePackageService;

    @RequestMapping(value = "/prizePackages", method = RequestMethod.POST)
    public ResponseEntity create(@RequestBody PrizePackageDto prizePackageDto){
        ResponseDto responseDto = new ResponseDto();
        try{
            prizePackageService.create(responseDto, prizePackageDto);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage(e.getMessage());
        }
        return ResponseEntity.ok(responseDto);
    }

    @RequestMapping(value = "/prizePackages", method = RequestMethod.PUT)
    public ResponseEntity update(@RequestBody PrizePackageDto prizePackageDto){
        ResponseDto responseDto = new ResponseDto();
        try{
            prizePackageService.update(responseDto, prizePackageDto);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage(e.getMessage());
        }
        return ResponseEntity.ok(responseDto);
    }

    @RequestMapping(value = "/prizePackages", method = RequestMethod.GET)
    public ResponseEntity fetch(@RequestParam Integer page,
                                @RequestParam Integer size,
                                @RequestParam String packageId,
                                @RequestParam(required = false) Integer status){
        ResponseDto responseDto = new ResponseDto();
        try{
            Pageable pageable = PageRequest.of(page-1, size);
            prizePackageService.find(responseDto, pageable, packageId, status);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage(e.getMessage());
        }
        return ResponseEntity.ok(responseDto);
    }
}
