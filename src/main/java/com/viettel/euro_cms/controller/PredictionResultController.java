package com.viettel.euro_cms.controller;

import com.viettel.euro_cms.common.Constant;
import com.viettel.euro_cms.domain.dto.response.ResponseDto;
import com.viettel.euro_cms.service.PredictionResultService;
import com.viettel.euro_cms.service.PredictionTurnService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping("/api/secure")
public class PredictionResultController {

    private final PredictionResultService predictionResultService;

    @RequestMapping(value = "/results", method = RequestMethod.GET)
    public ResponseEntity fetch(@RequestParam Integer page,
                                @RequestParam Integer size,
                                @RequestParam String predictionSession){
        ResponseDto responseDto = new ResponseDto();
        try{
            Pageable pageable = PageRequest.of(page-1, size);
            predictionResultService.find(responseDto, pageable, predictionSession);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage(e.getMessage());
        }
        return ResponseEntity.ok(responseDto);
    }
}
