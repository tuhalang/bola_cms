package com.viettel.euro_cms.controller;

import com.viettel.euro_cms.common.Constant;
import com.viettel.euro_cms.domain.dto.request.PlayerDto;
import com.viettel.euro_cms.domain.dto.response.ResponseDto;
import com.viettel.euro_cms.domain.model.Player;
import com.viettel.euro_cms.domain.model.WinnerPackage;
import com.viettel.euro_cms.service.PackageService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping("/api/secure")
public class PackageController {

    private final PackageService packageService;

    @RequestMapping(value = "/packages", method = RequestMethod.POST)
    public ResponseEntity create(@RequestBody WinnerPackage winnerPackage){
        ResponseDto responseDto = new ResponseDto();
        try{
            packageService.create(responseDto, winnerPackage);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage(e.getMessage());
        }
        return ResponseEntity.ok(responseDto);
    }

    @RequestMapping(value = "/packages", method = RequestMethod.PUT)
    public ResponseEntity update(@RequestBody WinnerPackage winnerPackage){
        ResponseDto responseDto = new ResponseDto();
        try{
            packageService.update(responseDto, winnerPackage);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage(e.getMessage());
        }
        return ResponseEntity.ok(responseDto);
    }

    @RequestMapping(value = "/packages", method = RequestMethod.GET)
    public ResponseEntity fetch(@RequestParam Integer page,
                                @RequestParam Integer size,
                                @RequestParam(required = false) Integer status,
                                @RequestParam(required = false) String key){
        ResponseDto responseDto = new ResponseDto();
        try{
            Pageable pageable = PageRequest.of(page-1, size);
            packageService.find(responseDto, pageable, status, key);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage(e.getMessage());
        }
        return ResponseEntity.ok(responseDto);
    }

}
