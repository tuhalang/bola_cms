package com.viettel.euro_cms.controller;

import com.viettel.euro_cms.common.Constant;
import com.viettel.euro_cms.domain.dto.response.ResponseDto;
import com.viettel.euro_cms.domain.model.Match;
import com.viettel.euro_cms.domain.model.PredictionSession;
import com.viettel.euro_cms.service.MatchService;
import com.viettel.euro_cms.service.PredictionSessionService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping("/api/secure")
public class MatchController {

    private final MatchService matchService;

    @RequestMapping(value = "/matches", method = RequestMethod.POST)
    public ResponseEntity create(@RequestBody Match match){
        ResponseDto responseDto = new ResponseDto();
        try{
            matchService.create(responseDto, match);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage(e.getMessage());
        }
        return ResponseEntity.ok(responseDto);
    }

    @RequestMapping(value = "/matches", method = RequestMethod.PUT)
    public ResponseEntity update(@RequestBody Match match){
        ResponseDto responseDto = new ResponseDto();
        try{
            matchService.update(responseDto, match);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage(e.getMessage());
        }
        return ResponseEntity.ok(responseDto);
    }

    @RequestMapping(value = "/matches", method = RequestMethod.GET)
    public ResponseEntity fetch(@RequestParam Integer page,
                                @RequestParam Integer size,
                                @RequestParam(required = false) String stateMatch,
                                @RequestParam(required = false) Integer status,
                                @RequestParam(required = false) String key){
        ResponseDto responseDto = new ResponseDto();
        try{
            Pageable pageable = PageRequest.of(page-1, size);
            matchService.find(responseDto, pageable, status, key, stateMatch);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage(e.getMessage());
        }
        return ResponseEntity.ok(responseDto);
    }
}
