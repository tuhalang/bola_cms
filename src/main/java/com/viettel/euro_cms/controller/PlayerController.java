package com.viettel.euro_cms.controller;

import com.viettel.euro_cms.common.Constant;
import com.viettel.euro_cms.domain.dto.request.PlayerDto;
import com.viettel.euro_cms.domain.dto.response.ResponseDto;
import com.viettel.euro_cms.domain.model.Player;
import com.viettel.euro_cms.domain.model.Prize;
import com.viettel.euro_cms.service.PlayerService;
import com.viettel.euro_cms.service.PrizeService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping("/api/secure")
public class PlayerController {

    private final PlayerService playerService;

    @RequestMapping(value = "/players", method = RequestMethod.POST)
    public ResponseEntity create(@RequestBody PlayerDto playerDto){
        ResponseDto responseDto = new ResponseDto();
        try{
            playerService.create(responseDto, playerDto);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage(e.getMessage());
        }
        return ResponseEntity.ok(responseDto);
    }

    @RequestMapping(value = "/players", method = RequestMethod.PUT)
    public ResponseEntity update(@RequestBody Player player){
        ResponseDto responseDto = new ResponseDto();
        try{
            playerService.update(responseDto, player);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage(e.getMessage());
        }
        return ResponseEntity.ok(responseDto);
    }

    @RequestMapping(value = "/players", method = RequestMethod.GET)
    public ResponseEntity fetch(@RequestParam Integer page,
                                @RequestParam Integer size,
                                @RequestParam(required = false) String team,
                                @RequestParam(required = false) Integer status,
                                @RequestParam(required = false) String key){
        ResponseDto responseDto = new ResponseDto();
        try{
            Pageable pageable = PageRequest.of(page-1, size);
            playerService.find(responseDto, pageable, status, key, team);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage(e.getMessage());
        }
        return ResponseEntity.ok(responseDto);
    }
}
