package com.viettel.euro_cms.controller;

import com.viettel.euro_cms.common.Constant;
import com.viettel.euro_cms.domain.dto.request.AccountDto;
import com.viettel.euro_cms.domain.dto.response.ResponseDto;
import com.viettel.euro_cms.service.UserService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping("/api/auth")
@Slf4j
public class AuthController {

    private final UserService userService;

    @PostMapping("/signIn")
    public ResponseEntity signIn(@RequestBody AccountDto accountDTO){
        ResponseDto responseDTO = new ResponseDto();
        try{
            userService.signIn(responseDTO, accountDTO);
        } catch (BadCredentialsException e){
            log.error(e.getMessage());
            responseDTO.setMessage(Constant.MSG_SIGN_IN_NOK);
        } catch (DisabledException e){
            log.error(e.getMessage());
            responseDTO.setMessage(Constant.MSG_USER_NOT_ACTIVE);
        } catch (Exception e){
            log.error(e.getMessage());
            responseDTO.setMessage(Constant.MSG_SERVER_ERROR);
        }
        return ResponseEntity.ok(responseDTO);
    }
}
