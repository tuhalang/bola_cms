package com.viettel.euro_cms.controller;

import com.viettel.euro_cms.common.Constant;
import com.viettel.euro_cms.domain.dto.response.ResponseDto;
import com.viettel.euro_cms.domain.model.PredictionSession;
import com.viettel.euro_cms.domain.model.Prize;
import com.viettel.euro_cms.service.PredictionSessionService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping("/api/secure")
public class PredictionSessionController {

    private final PredictionSessionService predictionSessionService;

    @RequestMapping(value = "/sessions", method = RequestMethod.POST)
    public ResponseEntity create(@RequestBody PredictionSession predictionSession){
        ResponseDto responseDto = new ResponseDto();
        try{
            predictionSessionService.create(responseDto, predictionSession);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage(e.getMessage());
        }
        return ResponseEntity.ok(responseDto);
    }

    @RequestMapping(value = "/sessions", method = RequestMethod.PUT)
    public ResponseEntity update(@RequestBody PredictionSession predictionSession){
        ResponseDto responseDto = new ResponseDto();
        try{
            predictionSessionService.update(responseDto, predictionSession);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage(e.getMessage());
        }
        return ResponseEntity.ok(responseDto);
    }

    @RequestMapping(value = "/sessions", method = RequestMethod.GET)
    public ResponseEntity fetch(@RequestParam Integer page,
                                @RequestParam Integer size,
                                @RequestParam(required = false) Integer type,
                                @RequestParam(required = false) String key){
        ResponseDto responseDto = new ResponseDto();
        try{
            Pageable pageable = PageRequest.of(page-1, size);
            predictionSessionService.find(responseDto, pageable, type, key);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            responseDto.setErrorCode(Constant.ERROR_CODE_NOK);
            responseDto.setMessage(e.getMessage());
        }
        return ResponseEntity.ok(responseDto);
    }
}
